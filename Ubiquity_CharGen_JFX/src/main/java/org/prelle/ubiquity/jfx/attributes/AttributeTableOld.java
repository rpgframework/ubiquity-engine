/**
 *
 */
package org.prelle.ubiquity.jfx.attributes;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.AttributeValue;
import org.prelle.ubiquity.GenerationTemplate;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.charctrl.AttributeController;

import javafx.event.EventHandler;
import javafx.geometry.Insets;
import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.GridPane;

/**
 * @author prelle
 *
 */
public class AttributeTableOld extends GridPane {
	
	private static Logger logger = LogManager.getLogger("ubiquity.jfx");

	private Map<Attribute, CheckBox[]> fields;
	private Map<Attribute, Button> decButtons;
	private Map<Attribute, Button> incButtons;
	private Map<Attribute, Label>  secondary;

	private AttributeController attrGen;
	private UbiquityCharacter model;
	private EventHandler<MouseEvent> onMouse;

	//-------------------------------------------------------------------
	public AttributeTableOld(AttributeController gen) {
		this.attrGen = gen;
		if (gen==null)
			throw new NullPointerException();

		initCompontents();
		initLayout();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initCompontents() {
		setVgap(10);
//		getStyleClass().add("priority-table");

		secondary = new HashMap<Attribute, Label>();
		fields = new HashMap<Attribute, CheckBox[]>();
		fields.put(Attribute.BODY     , new CheckBox[9]);
		fields.put(Attribute.DEXTERITY, new CheckBox[9]);
		fields.put(Attribute.STRENGTH , new CheckBox[9]);
		fields.put(Attribute.CHARISMA , new CheckBox[9]);
		fields.put(Attribute.INTELLIGENCE, new CheckBox[9]);
		fields.put(Attribute.WILLPOWER, new CheckBox[9]);

		for (Attribute attrib : Attribute.primaryValues()) {
			for (int x=0; x<fields.get(attrib).length; x++) {
				fields.get(attrib)[x] = new CheckBox();
//				fields.get(attrib)[x].setMouseTransparent(true);
			}
		}

		/*
		 * Buttons
		 */
		incButtons = new HashMap<Attribute, Button>();
		decButtons = new HashMap<Attribute, Button>();
		for (Attribute key : Attribute.values()) {
			Button dec = new Button("<");
			Button inc = new Button(">");
			decButtons.put(key, dec);
			incButtons.put(key, inc);
			dec.setUserData(key);
			inc.setUserData(key);
		}
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		/*
		 * Create attribute labels
		 */
		Label[] lblAttrib = new Label[Attribute.primaryValues().length];
		int i=0;
		for (Attribute attrib : Attribute.primaryValues()) {
			lblAttrib[i] = new Label(attrib.getName());
			lblAttrib[i].setUserData(attrib);
			lblAttrib[i].setOnMouseEntered(event -> {if (onMouse!=null) onMouse.handle(event);});
			// Geht nur in Java 8
//			lblAttrib[i].setPadding(new Insets(0,10,0,0));;
			i++;
		}


		i=0;
		for (Attribute attrib : Attribute.primaryValues()) {
			GridPane.setMargin(decButtons.get(attrib), new Insets(0,10,0,0));

			add(lblAttrib[i], 0,i);
			add(decButtons.get(attrib), 1,i);
			CheckBox[] boxes = fields.get(attrib);
//			for (int x=0; x<boxes.length; x++) {
//				add(boxes[x], 2+x, i);
//				// Extra space for last attribute
//				if ((x+1)==boxes.length)
//					GridPane.setMargin(boxes[x], new Insets(0,0,0,20));
//			}
			add(incButtons.get(attrib), 3+boxes.length,i);

			i++;
		}

		/*
		 * Create derived attribute labels
		 */
		i=0;
		for (Attribute attrib : Attribute.secondaryValues()) {
			Label name  = new Label(attrib.getName());
			Label value = new Label("-");
			name.setUserData(attrib);
			value.setUserData(attrib);
			name.setOnMouseEntered(event -> {if (onMouse!=null) onMouse.handle(event);});
			value.setOnMouseEntered(event -> {if (onMouse!=null) onMouse.handle(event);});
			secondary.put(attrib, value);
			add(name , 12,i);
			add(value, 13,i);
			GridPane.setMargin(name, new Insets(0,10,0,40));
			i++;
		}
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		for (Entry<Attribute, Button> entry : decButtons.entrySet()) {
			entry.getValue().setOnAction(event -> attrGen.decrease(entry.getKey()));
		}
		for (Entry<Attribute, Button> entry : incButtons.entrySet()) {
			entry.getValue().setOnAction(event -> attrGen.increase(entry.getKey()));
		}

		// Mouse over
		for (Attribute attrib : Attribute.primaryValues()) {
			decButtons.get(attrib).setOnMouseEntered(event -> {if (onMouse!=null) onMouse.handle(event);});
			incButtons.get(attrib).setOnMouseEntered(event -> {if (onMouse!=null) onMouse.handle(event);});
		}

		for (Attribute attrib : Attribute.primaryValues()) {
			for (CheckBox box : fields.get(attrib))
				box.setOnAction(event -> {
//				box.selectedProperty().addListener((ov,o,n) -> {
					int index = Arrays.asList(fields.get(attrib)).indexOf(box);
					setAttributeTo(attrib, index+1);
				});
		}

	}

	//-------------------------------------------------------------------
	private void setAttributeTo(Attribute attrib, int val) {
		int current = model.getAttribute(attrib).getPoints();
		logger.debug("Set "+attrib+" to "+val);
		// Loop until requested value reached
		while (current!=val) {
			if (current>val) {
				// Must be decreased
				if (attrGen.canBeDecreased(attrib)) {
					attrGen.decrease(attrib);
				} else {
					logger.warn("Cannot decrease from "+current+" to "+val);
					refresh();
					break;
				}
			} else {
				// Must be increased
				if (attrGen.canBeIncreased(attrib)) {
					attrGen.increase(attrib);
				} else {
					logger.warn("Cannot increase from "+current+" to "+val);
					refresh();
					break;
				}
			}
			current = model.getAttribute(attrib).getPoints();
		}
		logger.debug("Done");
	}

	//-------------------------------------------------------------------
	public void setData(GenerationTemplate template, UbiquityCharacter model) {
		this.model = model;

		int max = template.getAttributeMax();
		int i=0;
		for (Attribute attrib : Attribute.primaryValues()) {
			CheckBox[] boxes = fields.get(attrib);
			for (int x=0; x<boxes.length; x++) {
				if (x<max) {
					if (!getChildren().contains(boxes[x]))
						add(boxes[x], 2+x, i);
				} else {
					getChildren().remove(boxes[x]);
				}
			}
			i++;
		}

		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
		for (Attribute key : Attribute.primaryValues()) {
			AttributeValue val = model.getAttribute(key);
			set(key, val.getPoints());
			decButtons.get(key).setDisable(!attrGen.canBeDecreased(key));
			incButtons.get(key).setDisable(!attrGen.canBeIncreased(key));
		}
		for (Attribute key : Attribute.secondaryValues()) {
			AttributeValue val = model.getAttribute(key);
			secondary.get(key).setText(String.valueOf(val.getValue()));
		}
	}

	//-------------------------------------------------------------------
	public void set(Attribute key, int val) {
		CheckBox[] boxes = fields.get(key);
		for (int i=0; i<boxes.length; i++)
			boxes[i].setSelected(i<val);
	}

	//-------------------------------------------------------------------
	public void setOnAttributeSelected(EventHandler<MouseEvent> handler) {
		onMouse = handler;
	}

}
