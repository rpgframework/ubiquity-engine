/**
 * 
 */
package org.prelle.ubiquity.jfx.wizard;

import java.io.InputStream;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.ubiquity.Archetype;
import org.prelle.ubiquity.Motivation;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.FXCollections;
import javafx.scene.control.Label;
import javafx.scene.control.Spinner;
import javafx.scene.control.SpinnerValueFactory.ListSpinnerValueFactory;
import javafx.scene.image.Image;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan
 *
 */
public class WizardPageMotivation extends WizardPage implements GenerationEventListener {
	
	protected static Logger logger = LogManager.getLogger("ubiquity.jfx");
	
	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	private UbiquityRuleset ruleset;
	private CharacterGenerator charGen;
	
	// Content
	private Label    explain;
	private Spinner<Motivation> spinMotivation;
	private Label    reference;
	private Label    description;
	
	private VBox content;

	//--------------------------------------------------------------------
	public WizardPageMotivation(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		this.ruleset = charGen.getRuleset();
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);
		
		// Select standard
		spinMotivation.increment();
		spinMotivation.decrement();
//		update(ruleset.getMotivations().get(0));
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectMotivation.title"));

		
		/*
		 * Content
		 */
		explain = new Label(UI.getString("wizard.selectMotivation.descr"));
		explain.setWrapText(true);

		spinMotivation = new Spinner<>();
		spinMotivation.getStyleClass().add(Spinner.STYLE_CLASS_SPLIT_ARROWS_HORIZONTAL);
		ListSpinnerValueFactory<Motivation> factory = new ListSpinnerValueFactory<Motivation>(FXCollections.observableArrayList(charGen.getRuleset().getMotivations()));
		factory.setWrapAround(true);
		factory.setConverter(new StringConverter<Motivation>() {
			public String toString(Motivation race) { return race.getName();}
			public Motivation fromString(String string) {return null;}
		});
		spinMotivation.setValueFactory(factory);
		
		description = new Label();
		description.setWrapText(true);
		
		reference = new Label();
		
		content = new VBox();
		content.setSpacing(5);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Content node
		explain.setPrefWidth(550);
		description.setPrefWidth(550);
		
		content.getChildren().addAll(explain,  spinMotivation);
		content.getChildren().addAll(reference, description);
		super.setContent(content);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		explain.getStyleClass().add("text-body");
		description.getStyleClass().add("text-body");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		spinMotivation.valueProperty().addListener(new ChangeListener<Motivation>() {
			public void changed(ObservableValue<? extends Motivation> observable,
					Motivation oldValue, Motivation newValue) {
				charGen.selectMotivation(newValue);
				update(newValue);
			}
		});
		
	}

	//-------------------------------------------------------------------
	private void update(Motivation value) {
		// Update page reference
		int page = value.getPage();
		String product = value.getProductName();
		String format = UI.getString("format.pagereference");
		if (page>0)
			reference.setText(String.format(format, product, page));
		else
			reference.setText(String.format(format, product, "?"));

		description.setText(value.getHelpText());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case ARCHETYPE_CHANGED:
			// Typical motivations changed
			logger.debug("handle "+event);
			Archetype value = (Archetype)event.getKey();
			if (!value.getMotivations().isEmpty()) {
				Motivation recom = value.getMotivations().get(0);
				spinMotivation.getValueFactory().setValue(recom);
			}

			// Image from archetype
			String fname = "data/space1889/archetype_"+value.getId()+".png";
			logger.trace("Load "+fname);
			InputStream in = getClass().getClassLoader().getResourceAsStream(fname);
			if (in!=null)
				setImage(new Image(in));
			else
				logger.error("Missing image "+fname);
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft(CloseType type) {
		logger.debug("pageLeft("+type+")");
		charGen.selectMotivation(spinMotivation.getValue());
	}

}

