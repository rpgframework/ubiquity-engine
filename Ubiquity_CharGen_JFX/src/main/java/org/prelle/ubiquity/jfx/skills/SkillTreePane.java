/**
 *
 */
package org.prelle.ubiquity.jfx.skills;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.AttributeValue;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.SkillValue;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.TalentValue;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.SkillController;
import org.prelle.ubiquity.charctrl.TalentController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import javafx.beans.property.ReadOnlyStringWrapper;
import javafx.beans.property.SimpleIntegerProperty;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Tooltip;
import javafx.scene.control.TreeItem;
import javafx.scene.control.TreeTableCell;
import javafx.scene.control.TreeTableColumn;
import javafx.scene.control.TreeTableColumn.CellDataFeatures;
import javafx.scene.control.TreeTableView;

/**
 * @author Stefan
 *
 */
public class SkillTreePane extends TreeTableView<Skill> implements GenerationEventListener {
	
	private static Logger logger = LogManager.getLogger("ubiquity.jfx");
	
	private static PropertyResourceBundle uiResources = UbiquityJFXConstants.UI;

	private SkillController control;
	private TalentController talentCtrl;
	private UbiquityCharacter model;

	private TreeTableColumn<Skill, String> colName;
	private TreeTableColumn<Skill, String> colAtt;
	private TreeTableColumn<Skill, Number> colPoints;
	private TreeTableColumn<Skill, Number> colMod;
	private TreeTableColumn<Skill, Number> colValue;

	private Map<Skill, SkillTreeItem> itemBySkill;

//	private MyTreeTableViewSkin<Skill> skin;

	//--------------------------------------------------------------------
	public SkillTreePane(SkillController ctrl, TalentController talCtrl, UbiquityCharacter model) {
		super(new SkillTreeItem());
		this.control = ctrl;
		this.talentCtrl = talCtrl;
		this.model = model;

		initComponents();
		initFactories();
		initLayout();
		initData();

//		skin = new MyTreeTableViewSkin<Skill>(this);
//		setSkin(skin);
		for (SkillValue sVal : model.getSkills()) {
			SkillTreeItem item = itemBySkill.get(sVal.getModifyable());
			item.setSkillValue(sVal);
		}

		getSelectionModel().setCellSelectionEnabled(false);
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void initComponents() {
		colName   = new TreeTableColumn<Skill, String>(uiResources.getString("label.skills"));
		colAtt    = new TreeTableColumn<Skill, String>(uiResources.getString("label.attribute.short"));
		colPoints = new TreeTableColumn<Skill, Number>(uiResources.getString("label.points"));
		colMod    = new TreeTableColumn<Skill, Number>(uiResources.getString("label.modifier.short"));
		colValue  = new TreeTableColumn<Skill, Number>(uiResources.getString("label.value"));
		getColumns().addAll(colName, colAtt, colPoints, colMod, colValue);
		getRoot().setExpanded(true);
		setShowRoot(false);

		colName.setMinWidth(250);

		itemBySkill = new HashMap<>();
	}

	//--------------------------------------------------------------------
	private void initFactories() {
		colName.setCellValueFactory((TreeTableColumn.CellDataFeatures<Skill, String> p) -> {
			if (p.getValue()==getRoot())
				return null;
			Skill skill = p.getValue().getValue();
			if (skill.isPartOfGroup())
				return new ReadOnlyStringWrapper(skill.getNameWithoutGroup());
			return new ReadOnlyStringWrapper(skill.getName());
		});

		colAtt.setCellValueFactory((CellDataFeatures<Skill,String> p) -> {
			if (p.getValue()==getRoot())
				return null;
			Skill skill = p.getValue().getValue();
			return new ReadOnlyStringWrapper(skill.getAttribute().getShortName());
		});
		colPoints.setCellValueFactory((CellDataFeatures<Skill,Number> p) -> {
			Skill skill = p.getValue().getValue();
			if (!control.getAvailableSkills().contains(skill))
				return null;
			SkillValue sVal = ((SkillTreeItem)p.getValue()).getSkillValue();
			if (sVal==null)
				return new SimpleIntegerProperty(0);
			return new SimpleIntegerProperty(sVal.getPoints())
		;});
		colMod.setCellValueFactory((CellDataFeatures<Skill,Number> p) -> {
			Skill skill = p.getValue().getValue();
			if (!control.getAvailableSkills().contains(skill))
				return null;
			SkillValue sVal = ((SkillTreeItem)p.getValue()).getSkillValue();
			if (sVal==null)
				return new SimpleIntegerProperty(0);
			return new SimpleIntegerProperty(sVal.getModifier())
		;});
		colValue.setCellValueFactory((CellDataFeatures<Skill,Number> p) -> {
			Skill skill = p.getValue().getValue();
			if (!control.getAvailableSkills().contains(skill))
				return null;
			SkillValue sVal = ((SkillTreeItem)p.getValue()).getSkillValue();
			int finVal = (sVal==null)?0:sVal.getModifiedValue();
			AttributeValue aVal = model.getAttribute(skill.getAttribute());
			int attVal = aVal.getValue();
			return new SimpleIntegerProperty(finVal	+ attVal);
		});

		// Cell Factories
		colName.setCellFactory((TreeTableColumn<Skill, String> param) -> {
			return new SkillNameCell(talentCtrl, model.getRuleset());});
		colPoints.setCellFactory((TreeTableColumn<Skill, Number> param) -> {
			return new PointsFieldCell(control, talentCtrl, model.getRuleset(), this);});
	}

	//--------------------------------------------------------------------
	private void initLayout() {
		setColumnResizePolicy(UNCONSTRAINED_RESIZE_POLICY);

//		colName.setStyle("-fx-pref-width: 20em");
	}

	//--------------------------------------------------------------------
	private void initData() {
		TreeItem<Skill> root = getRoot();
		for (Skill skill : control.getRuleset().getSkills()) {
			SkillTreeItem item = new SkillTreeItem(skill);
			itemBySkill.put(skill, item);

			if (skill.isPartOfGroup() && control.getAvailableSkills().contains(skill)) {
				SkillTreeItem parent = itemBySkill.get(skill.getParentSkill());
				parent.getChildren().add(item);
			} else if (!skill.isPartOfGroup()){
				root.getChildren().add(item);
			}
		}
	}

	//--------------------------------------------------------------------
	UbiquityCharacter getModel() {
		return model;
	}

	//--------------------------------------------------------------------
	public void setData(UbiquityCharacter model) {
		this.model = model;

		for (SkillValue sVal : model.getSkills()) {
			SkillTreeItem item = itemBySkill.get(sVal.getModifyable());
			item.setSkillValue(sVal);
		}
	}

	//--------------------------------------------------------------------
	public void replaceSkillValue(Skill skill, SkillValue val) {
		logger.debug("replaceSkillValue "+val);
		SkillTreeItem item = itemBySkill.get(skill);
		item.setSkillValue(val);
	}


	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		SkillTreeItem item;
		SkillValue sVal;
		Skill skill;
		switch (event.getType()) {
		case SKILL_CHANGED:
			logger.debug("RCV "+event);
			skill = (Skill) event.getKey();
			item = itemBySkill.get(skill);
			sVal = model.getSkill(skill);
			logger.debug("Changed Item = "+item+" to "+sVal.getPoints());

			if (sVal.getPoints()==0)
				item.setSkillValue(null);
			else
				item.setSkillValue(sVal);
//			if (skill.isPartOfGroup() && control.getAvailableSkills().contains(skill)) {
//				SkillTreeItem parent = itemBySkill.get(skill.getParentSkill());
//				int idx = parent.getChildren().indexOf(item);
//				parent.getChildren().remove(item);
//				parent.getChildren().add(idx, item);
//			} else if (!skill.isPartOfGroup()){
//				int idx = getRoot().getChildren().indexOf(item);
//				getRoot().getChildren().remove(item);
//				getRoot().getChildren().add(idx, item);
//			}
//			skin.refresh();
			break;
		case SKILL_ADDED:
			logger.debug("RCV "+event);
			skill = (Skill) event.getKey();
			sVal  = (SkillValue) event.getValue();
			item = itemBySkill.get(skill);
			logger.debug("Enabled Item = "+item);
			item.setSkillValue(sVal);
//			skin.refresh();
			break;
		case SKILL_REMOVED:
			logger.debug("RCV "+event);
			skill = (Skill) event.getKey();
			item = itemBySkill.get(skill);
			logger.debug("Disabled Item = "+item);
			item.setSkillValue(null);
//			logger.fatal("Stop here");
//			System.exit(0);
//			skin.refresh();
			break;
		case SKILL_AVAILABLE_CHANGED:
			logger.debug("RCV "+event);
			List<Skill>[] changes = (List[])event.getKey();
			List<Skill> added   = (List<Skill>)changes[1];
			List<Skill> removed = (List<Skill>)changes[2];
			/*
			 * Remove corresponding skill values from items
			 * Remove sub-skills-items from tree, but always keep
			 * grouop items
			 */
			for (Skill removedSkill : removed) {
				item = itemBySkill.get(removedSkill);
//				item.setSkillValue(null);
				if (removedSkill.isPartOfGroup()) {
					Skill group = removedSkill.getParentSkill();
					SkillTreeItem parentItem = itemBySkill.get(group);
					parentItem.getChildren().remove(item);
					logger.debug("Removed item for "+removedSkill+" from "+group);
				}
			}
			for (Skill addedSkill : added) {
				item = itemBySkill.get(addedSkill);
//				item.setSkillValue(null);
				if (addedSkill.isPartOfGroup()) {
					Skill group = addedSkill.getParentSkill();
					SkillTreeItem parentItem = itemBySkill.get(group);
					parentItem.getChildren().add(item);
//					logger.trace("Added item for "+addedSkill+" from "+group);
				}
			}
			break;
		default:
			break;
		}
	}

	
	//--------------------------------------------------------------------
	public void setController(SkillController ctrl, TalentController talCtrl) {
		this.control = ctrl;
		this.talentCtrl = talCtrl;
		refresh();
	}

}

class SkillTreeItem extends TreeItem<Skill> {

	private SkillValue sVal;

	public SkillTreeItem() {}
	public SkillTreeItem(Skill data) {
		super(data);
	}
	public void setSkillValue(SkillValue sVal) {
		this.sVal = sVal;
	}
	public SkillValue getSkillValue() { return sVal; }

//	//--------------------------------------------------------------------
//	public static Callback<SkillValueTreeItem, Observable[]> extractor() {
//        return new Callback<SkillValueTreeItem, Observable[]>() {
//            @Override
//            public Observable[] call(SkillValueTreeItem param) {
//                return new Observable[]{param..durationSeconds, param.lss};
//            }
//        };
//    }
}

class PointsFieldCell extends TreeTableCell<Skill, Number> {

	private static Logger logger = LogManager.getLogger("ubiquity.jfx");

	private Talent SKILLMASTER;

	private SkillField spinner;
	private SkillController control;
	private TalentController talentCtrl;
	private CheckBox cbSkillMaster;
	private SkillTreePane parent;

	//--------------------------------------------------------------------
	public PointsFieldCell(SkillController ctrl, TalentController talent, UbiquityRuleset ruleset, SkillTreePane parent) {
		this.control = ctrl;
		this.talentCtrl = talent;
		this.parent = parent;
		spinner = new SkillField();
		setGraphic(spinner);
		SKILLMASTER = ruleset.getTalent("skillmaster");
		cbSkillMaster = new CheckBox(SKILLMASTER.getName());

		spinner.inc.setOnAction(event -> { ctrl.increase(getTreeTableRow().getItem()); refresh(getTreeTableRow().getItem());});
		spinner.dec.setOnAction(event -> { ctrl.decrease(getTreeTableRow().getItem()); refresh(getTreeTableRow().getItem());});

		cbSkillMaster.selectedProperty().addListener((ov,o,n) -> {
			if (n)
				talentCtrl.select(SKILLMASTER, getTreeTableRow().getItem());
			else {
				logger.debug("Search SKILLMASTER for "+getTreeTableRow().getItem());
				for (TalentValue tVal : parent.getModel().getTalents()) {
					if (tVal.getTalent()==SKILLMASTER && tVal.getSkillOption()==getTreeTableRow().getItem()) {
						logger.debug("Found "+tVal);
						talentCtrl.deselect(tVal);
						break;
					}
				}
			}
		});
	}

	private void refresh(Skill skill) {
		spinner.dec.setDisable(!control.canBeDecreased(skill));
		spinner.inc.setDisable(!control.canBeIncreased(skill));
		SkillTreeItem myItem = (SkillTreeItem) getTreeTableRow().getTreeItem();
		SkillValue sVal = myItem.getSkillValue();
		if (sVal!=null)
			spinner.setText(String.valueOf(sVal.getPoints()));
		else {
			spinner.setText("0");
		}

	}

	//--------------------------------------------------------------------
	@Override
	public void updateItem(Number item, boolean empty) {
		super.updateItem(item, empty);
		Skill skill = getTreeTableRow().getItem();
		if (empty || skill==null) {
			setText(null);
			setGraphic(null);
			return;
		}

		SkillTreeItem myItem = (SkillTreeItem) getTreeTableRow().getTreeItem();
		SkillValue sVal = myItem.getSkillValue();
		// Don't show anything if skill may not be used
		// - should be the case for skillgroups without skillmaster talent
		if (!control.getAvailableSkills().contains(skill)) {
			setText(null);
			if (talentCtrl.canBeSelected(SKILLMASTER))
				setGraphic(cbSkillMaster);
			else
				setGraphic(null);
			return;
		}
		spinner.dec.setDisable(!control.canBeDecreased(skill));
		spinner.inc.setDisable(!control.canBeIncreased(skill));

		setGraphic(spinner);
		if (sVal!=null)
			spinner.setText(String.valueOf(sVal.getPoints()));
		else {
			spinner.setText("0");
		}
	}
}

class SkillNameCell extends TreeTableCell<Skill, String> {

	private Talent SKILLMASTER;

	private TalentController talentCtrl;

	//--------------------------------------------------------------------
	public SkillNameCell(TalentController talent, UbiquityRuleset ruleset) {
		this.talentCtrl = talent;
		SKILLMASTER = ruleset.getTalent("skillmaster");
	}

	//--------------------------------------------------------------------
	@Override
	public void updateItem(String item, boolean empty) {
		super.updateItem(item, empty);
		Skill skill = getTreeTableRow().getItem();

		if (empty || skill==null) {
			setText(null);
			setGraphic(null);
			return;
		}

		setText(item);
		if (talentCtrl.hasSkillMaster(skill)) {
			setStyle("-fx-font-weight: bold");
			setTooltip(new Tooltip(SKILLMASTER.getName()));
		} else {
			setStyle("-fx-font-weight: normal");
			setTooltip(null);
		}
	}

}