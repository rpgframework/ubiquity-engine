package org.prelle.ubiquity.jfx.sections;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;
import java.util.stream.Collectors;

import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DescriptionPane;
import org.prelle.rpgframework.jfx.OptionalDescriptionPane;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.items.CarriedItem;
import org.prelle.ubiquity.items.ItemLocationType;
import org.prelle.ubiquity.items.ItemTemplate;
import org.prelle.ubiquity.items.ItemType;
import org.prelle.ubiquity.jfx.cells.CarriedItemCell;
import org.prelle.ubiquity.jfx.cells.ItemTemplateCell;

import javafx.application.Platform;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListView;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.util.StringConverter;

/**
 * @author Stefan Prelle
 *
 */
public class EquipmentSection extends GenericListSection<CarriedItem> {

	private static PropertyResourceBundle RES = (PropertyResourceBundle) ResourceBundle.getBundle(EquipmentSection.class.getName());
	
	private ItemLocationType loc;

	//-------------------------------------------------------------------
	public EquipmentSection(String title, CharacterController ctrl, ScreenManagerProvider provider, ItemLocationType loc) {
		super(title, ctrl, provider);
		list.setCellFactory( lv -> new CarriedItemCell(ctrl.getModel()));
		this.loc = loc;

//		setData(ctrl.getModel().getItems().stream().filter(item -> item.getLocation()==loc).collect(Collectors.toList()));
		refresh();
		list.setStyle("-fx-pref-height: 50em; -fx-pref-width: 32em");
		GridPane.setVgrow(list, Priority.ALWAYS);
		list.setMaxHeight(Double.MAX_VALUE);

		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> getDeleteButton().setDisable(n==null));
		initInteractivity();
	}

	//-------------------------------------------------------------------
	protected void initInteractivity() {
		super.initInteractivity();
		list.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			Platform.runLater( () -> {
				getDeleteButton().setDisable( !control.getItemController().canBeDeselected(n));
			});
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.jfx.sections.GenericListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		logger.trace("onAdd");
		Label question = new Label(RES.getString("section.item.dialog.add.question"));
		
		ChoiceBox<ItemType> cbType = new ChoiceBox<>();
		cbType.getItems().addAll(ItemType.values());
		cbType.setConverter(new StringConverter<ItemType>() {
			public String toString(ItemType val) { return val.getName();}
			public ItemType fromString(String string) { return null; }
		});
		ListView<ItemTemplate> myList = new ListView<>();
		myList.setCellFactory(lv -> new ItemTemplateCell());
		myList.getItems().addAll(control.getItemController().getAvailable());
		myList.setPlaceholder(new Label(RES.getString("section.item.dialog.add.placeholder")));
		VBox innerLayout = new VBox(10, question, cbType, myList);

		cbType.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			myList.getItems().setAll(
					control.getItemController().getAvailable().stream().filter(item -> item.isType(n)).collect(Collectors.toList())
					);
		});
		
		final DescriptionPane descr = new DescriptionPane();
		OptionalDescriptionPane layout = new OptionalDescriptionPane(innerLayout, descr);

		myList.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n!=null) {
			descr.setText(n.getName(), n.getProductName()+" "+n.getPage(), n.getHelpText());
			} else {
				descr.setText(null, null, null);
			}
		});


		CloseType result = provider.getScreenManager().showAlertAndCall(AlertType.QUESTION, RES.getString("section.item.dialog.add.title"), layout);
		if (result==CloseType.OK) {
			ItemTemplate value = myList.getSelectionModel().getSelectedItem();
			if (value!=null) {
				logger.info("Try add item: "+value);
				myList.getItems().add(value);
				control.getItemController().select(value);
			}
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.jfx.sections.GenericListSection#onDelete()
	 */
	@Override
	protected void onDelete() {
		logger.trace("onDelete");
		CarriedItem toDelete = list.getSelectionModel().getSelectedItem();
		if (toDelete!=null) {
			logger.info("Try remove item: "+toDelete);
			if (control.getItemController().deselect(toDelete)) {
				list.getItems().remove(toDelete);
				list.getSelectionModel().clearSelection();
			} else 
				logger.warn("GUI allowed removing a item which cannot be deselected: "+toDelete);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		setData(control.getModel().getItems().stream().filter(item -> (item.getLocation()==loc || (loc==ItemLocationType.BODY && item.getLocation()==null))).collect(Collectors.toList()));
//		setData(control.getModel().getItems());
	}

}
