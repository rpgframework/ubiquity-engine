package org.prelle.ubiquity.jfx.cells;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.charctrl.SkillController;

import javafx.scene.control.Button;
import javafx.scene.control.CheckBox;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.layout.HBox;

public class SkillSpecListCell extends  ListCell<ListElemSpecialization> {

	private final static Logger logger = LogManager.getLogger("ubiquity.jfx");

	private SkillController control;
	private Button btnDec, btnInc;
	private CheckBox check1;
	private Label label;
	private HBox box;

	//--------------------------------------------------------------------
	public SkillSpecListCell(CharacterController ctrl) {
		control = ctrl.getSkillController();
		check1 = new CheckBox();
		btnInc = new Button("\uE0C5");
		btnInc.setStyle("-fx-font-family: 'Segoe UI Symbol'");
		btnDec = new Button("\uE0C6");
		btnDec.setStyle("-fx-font-family: 'Segoe UI Symbol'");
		label  = new Label();
		box = new HBox(5);
		box.getChildren().addAll(check1, btnDec, btnInc, label);
		this.setPrefWidth(0);
		box.prefWidthProperty().bind(this.widthProperty());

		check1.setOnAction(event -> changed(check1.selectedProperty().get()));

		btnDec.setOnAction(ev -> control.decreaseSpecialization(getItem().val));
		btnInc.setOnAction(ev -> control.increaseSpecialization(getItem().val));
	}

	//--------------------------------------------------------------------
	private void changed(boolean selected) {
		logger.debug("Cell changes to "+selected+"  "+getItem());
		btnDec.setDisable(!control.canDecreaseSpecialization(getItem().val));
		btnInc.setDisable(!control.canIncreaseSpecialization(getItem().val));
		if (selected)
			control.selectSpecialization(getItem().data);
		else
			control.deselectSpecialization(getItem().val);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(ListElemSpecialization item, boolean empty) {
		super.updateItem(item, empty);

		if (empty) {
			setGraphic(null);
			setText(null);
		} else {


			label.setText(item.data.getName()+" "+((item.val!=null)?item.val.getValue():""));
			setGraphic(box);
//			boolean selected = getListView().getSelectionModel().isSelected(this.getIndex());
			boolean selected = (item.val!=null)?(item.val.getValue()>0):false;
			check1.setSelected(selected);
//			logger.debug("updateItem("+item.data+") for "+control+" as selected="+selected);
			btnDec.setDisable(!control.canDecreaseSpecialization(getItem().val));
			btnInc.setDisable(!control.canIncreaseSpecialization(getItem().val));

			if (selected)
				check1.setDisable(!control.canDeselectSpecialization(item.val));
			else
				check1.setDisable(!control.canSelectSpecialization(item.data));
		}

	}

}