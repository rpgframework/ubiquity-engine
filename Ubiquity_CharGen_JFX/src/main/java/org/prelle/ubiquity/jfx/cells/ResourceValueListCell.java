package org.prelle.ubiquity.jfx.cells;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.AlertType;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.ManagedScreen;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.ubiquity.ResourceValue;
import org.prelle.ubiquity.charctrl.ResourceController;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;
import org.prelle.ubiquity.levelling.ResourceLeveller;

import javafx.geometry.Orientation;
import javafx.geometry.Pos;
import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.TextField;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.TilePane;
import javafx.scene.layout.VBox;

public class ResourceValueListCell extends ListCell<ResourceValue> {
	
	private static Logger logger = LogManager.getLogger("ubiquity.jfx");
	
	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;
	
	private transient ResourceValue data;
	
	private ResourceController charGen;
	private ScreenManagerProvider provider;

	private HBox layout;
	private Label name;
	private Button btnEdit;
	private Button btnDec;
	private Label  lblVal;
	private Button btnInc;
	
	private Label tfDescr;
	
	//-------------------------------------------------------------------
	public ResourceValueListCell(ResourceController charGen, ScreenManagerProvider prov) {
		this.charGen = charGen;
		this.provider= prov;
		if (provider==null)
			throw new NullPointerException();
		
		layout  = new HBox(5);
		name    = new Label();
		tfDescr = new Label();
		tfDescr.setWrapText(true);
		btnEdit = new Button("\uE1C2");
		btnDec  = new Button("-");
		lblVal  = new Label("?");
		btnInc  = new Button("+");
		
		initStyle();
		initLayout();
		initInteractivity();
	}
	
	//-------------------------------------------------------------------
	private void initStyle() {	
		btnDec.setStyle("-fx-background-color: transparent");
		btnInc.setStyle("-fx-background-color: transparent");
		btnDec.getStyleClass().add("bordered");
		btnInc.getStyleClass().add("bordered");
		name.getStyleClass().add("text-small-subheader");
		lblVal.getStyleClass().add("text-subheader");
		
		btnEdit.setStyle("-fx-background-color: transparent");
		
		layout.getStyleClass().add("content");	
		
		getStyleClass().add("resourcevalue-list-cell");
		btnEdit.setStyle("-fx-min-width: 3em");
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {		
		HBox line2 = new HBox(5);
		line2.getChildren().addAll(btnEdit, tfDescr);
		
		VBox bxCenter = new VBox(2);
		bxCenter.getChildren().addAll(name, line2);

		btnDec.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnInc.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		TilePane tiles = new TilePane(Orientation.HORIZONTAL);
		tiles.setPrefColumns(3);
		tiles.setHgap(4);
		tiles.getChildren().addAll(btnDec, lblVal, btnInc);
		tiles.setAlignment(Pos.CENTER_LEFT);
		tiles.setStyle("-fx-min-width: 8em");
		layout.getChildren().addAll(bxCenter, tiles);
		
		bxCenter.setMaxWidth(Double.MAX_VALUE);
		HBox.setHgrow(bxCenter, Priority.ALWAYS);
		
		
		setAlignment(Pos.CENTER);
	}
	
	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnInc .setOnAction(event -> {charGen.increase(data); updateItem(data, false); });
		btnDec .setOnAction(event -> {charGen.decrease(data); updateItem(data, false); });
		btnEdit.setOnAction(event -> editClicked(data));

		this.setOnDragDetected(event -> dragStarted(event));
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.debug("drag started for "+data);
		if (data==null)
			return;
		logger.debug("canBeDeselected = "+charGen.canBeDeSelected(data));
		logger.debug("canBeTrashed    = "+((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true));
		if (!charGen.canBeDeSelected(data) && !((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true))
			return;
		
		Node source = (Node) event.getSource();
		logger.debug("drag src = "+source);

		/* drag was detected, start a drag-and-drop gesture*/
        /* allow any transfer mode */
        Dragboard db = source.startDragAndDrop(TransferMode.ANY);
        
        /* Put a string on a dragboard */
        ClipboardContent content = new ClipboardContent();
        String id = "resource:"+data.getModifyable().getId()+"|desc="+data.getComment()+"|idref="+data.getIdReference();
        content.putString(id);
        db.setContent(content);
        
        /* Drag image */
        WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
        db.setDragView(snapshot);
        
        event.consume();	
    }


	//-------------------------------------------------------------------
	private void editClicked(ResourceValue ref) {
		String res = ref.getModifyable().getId();
//		if (res.equals("creature")) {
//			editClickedCreature(ref);
//		} else
//		if (res.equals("relic")) {
//			editClickedRelic(ref);
//		} else {
			TextField tf = new TextField(ref.getComment());
			tf.setOnAction(event -> {
				ManagedScreen screen = (ManagedScreen) tf.getParent().getParent().getParent().getParent();
				logger.debug("Action on "+screen);
//				screen.impl_navigClicked(CloseType.OK, event);
				});
			CloseType close = provider.getScreenManager().showAlertAndCall(
					AlertType.QUESTION, 
					UI.getString("resourcelistview.namedialog.title"), 
					tf);
			if (close==CloseType.OK) {
				ref.setComment(tf.getText());
				tfDescr.setText(tf.getText());
			}
//		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	protected void updateItem(ResourceValue resRef, boolean empty) {
		super.updateItem(resRef, empty);
		
		if (empty) {
			setGraphic(null);
			name.setText(null);
//			setStyle("-fx-border: 0px; -fx-border-color: transparent; -fx-padding: 2px; -fx-background-color: transparent");
			return;
		} else {
			data = resRef;
			
			setGraphic(layout);
			name.setText(resRef.getModifyable().getName());
			tfDescr.setText(resRef.getComment());
			lblVal.setText(" "+String.valueOf(resRef.getPoints())+" ");
			btnDec.setDisable(!charGen.canBeDecreased(resRef));
			btnInc.setDisable(!charGen.canBeIncreased(resRef));
//			if (resRef.getSkillSpecialization()!=null) 
//				name.setText(resRef.getSkillSpecialization().getSkill().getName()+"/"+resRef.getSkillSpecialization().getName()+" +1");
//			if (charGen.canBeDeselected(item)) {
//				layout.getStyleClass().clear();
//				layout.getStyleClass().add("selectable-list-item");
//			} else {
//				layout.getStyleClass().clear();
//				layout.getStyleClass().add("unselectable-list-item");
//			}
		}

	}

}