/**
 *
 */
package org.prelle.ubiquity.jfx.sections;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.ubiquity.Archetype;
import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.BasePluginData;
import org.prelle.ubiquity.Motivation;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityCharacter.Gender;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;
import org.prelle.ubiquity.jfx.ViewMode;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.geometry.Pos;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;
import javafx.util.StringConverter;

/**
 * @author prelle
 *
 */
public class BasicDataSection extends SingleSection{

	private final static Logger logger = LogManager.getLogger(UbiquityJFXConstants.BASE_LOGGER_NAME);
	
	private final static String INVALID = "invalid-data";

	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	private CharacterController control;
	private UbiquityCharacter model;
	private ViewMode mode;

	private TextField tfName;
	private ChoiceBox<Archetype> cbArchetype;
	private ChoiceBox<Motivation> cbMotivation;
	private ChoiceBox<Gender> cbGender;
	private TextField tfRace;

	
	private TextField tfSize;
	private TextField tfAge;
	private TextField tfWeight;
	private TextField tfHair;
	private TextField tfEyes;
	private TextField tfSkin;

	private ObjectProperty<BasePluginData> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public BasicDataSection(String title, CharacterController ctrl, ViewMode mode, ScreenManagerProvider provider) {
		super(provider, title, null);
		control = ctrl;
		model = ctrl.getModel();
		this.mode = mode;

		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		tfName = new TextField();	
		cbArchetype = new ChoiceBox<>();
		cbArchetype.getItems().addAll(control.getRuleset().getArchetypes());
		cbArchetype.setConverter(new StringConverter<Archetype>() {
			public String toString(Archetype val) { return val.getName(); }
			public Archetype fromString(String val) {return null;}
		});
		cbMotivation = new ChoiceBox<>();
		cbMotivation.getItems().addAll(control.getRuleset().getMotivations());
		cbMotivation.setConverter(new StringConverter<Motivation>() {
			public String toString(Motivation val) { return val.getName(); }
			public Motivation fromString(String val) {return null;}
		});
		cbGender = new ChoiceBox<>();
		cbGender.getItems().addAll(Gender.values());
		tfRace = new TextField();	
		
		tfSize = new TextField();
		tfAge  = new TextField();
		tfWeight = new TextField();
		tfHair = new TextField();
		tfEyes = new TextField();
		tfSkin = new TextField();
		
		tfSize  .setStyle("-fx-pref-width: 4em");
		tfAge   .setStyle("-fx-pref-width: 3em");
		tfWeight.setStyle("-fx-pref-width: 3em");
		tfHair  .setStyle("-fx-pref-width: 8em");
		tfEyes  .setStyle("-fx-pref-width: 8em");
		tfSkin  .setStyle("-fx-pref-width: 8em");
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		Label heaName      = new Label(UI.getString("label.name"));
		Label heaArchtype  = new Label(UI.getString("label.archetype"));
		Label heaMotiv     = new Label(UI.getString("label.motivation"));
		Label heaGender    = new Label(UI.getString("label.gender"));
		Label heaRace      = new Label(UI.getString("label.race"));
		
		Label heaSize      = new Label(UI.getString("label.size"));
		Label heaAge       = new Label(UI.getString("label.age"));
		Label heaWeight    = new Label(UI.getString("label.weight"));
		Label heaHair      = new Label(UI.getString("label.hair"));
		Label heaEyes      = new Label(UI.getString("label.eyes"));
		Label heaSkin      = new Label(UI.getString("label.skin"));
		
		HBox sizeLine = new HBox(5);
		sizeLine.getChildren().addAll(tfSize, new Label(UI.getString("label.size.unit")));
		HBox wghtLine = new HBox(5);
		wghtLine.getChildren().addAll(tfWeight, new Label(UI.getString("label.weight.unit")));
		sizeLine.setAlignment(Pos.CENTER_LEFT);
		wghtLine.setAlignment(Pos.CENTER_LEFT);
		
		GridPane grid = new GridPane();
		grid.add(heaName    , 0, 0);
		grid.add( tfName    , 1, 0);
		grid.add(heaArchtype, 0, 1);
		grid.add(cbArchetype, 1, 1);
		grid.add(heaMotiv   , 0, 2);
		grid.add(cbMotivation, 1, 2);
		grid.add(heaGender  , 0, 3);
		grid.add( cbGender  , 1, 3);
		grid.add(heaRace    , 0, 4);
		grid.add( tfRace    , 1, 4);
		
		grid.add(heaSize  , 3, 0);
		grid.add( sizeLine, 4, 0);
		grid.add(heaAge   , 3, 1);
		grid.add( tfAge   , 4, 1);
		grid.add(heaWeight, 3, 2);
		grid.add( wghtLine, 4, 2);
		grid.add(heaHair  , 3, 3);
		grid.add( tfHair  , 4, 3);
		grid.add(heaEyes  , 3, 4);
		grid.add( tfEyes  , 4, 4);
		grid.add(heaSkin  , 3, 5);
		grid.add( tfSkin  , 4, 5);

		heaName.getStyleClass().add("base");
		heaArchtype.getStyleClass().add("base");
		heaMotiv.getStyleClass().add("base");
		heaGender.getStyleClass().add("base");
		heaRace.getStyleClass().add("base");
		heaSize.getStyleClass().add("base");
		heaAge.getStyleClass().add("base");
		heaWeight.getStyleClass().add("base");
		heaHair.getStyleClass().add("base");
		heaEyes.getStyleClass().add("base");
		heaSkin.getStyleClass().add("base");
		grid.setStyle("-fx-vgap: 0.5em; -fx-padding: 0.3em; -fx-hgap: 0.5em");

		setContent(grid);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		tfName.textProperty().addListener( (ov,o,n) -> {
			control.getModel().setName(n);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, control.getModel()));
			});
		cbArchetype.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			showHelpFor.set(n);
			if (mode==ViewMode.GENERATION) {
				((CharacterGenerator)control).selectArchetype(n);
			} else {
				control.getModel().setArchetype(n);
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ARCHETYPE_CHANGED, n));
			}
		});
		cbMotivation.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			showHelpFor.set(n);
			if (mode==ViewMode.GENERATION) {
				((CharacterGenerator)control).selectMotivation(n);
			} else {
				control.getModel().setMotivation(n);
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.MOTIVATION_CHANGED, n));
			}
		});
		cbGender.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			control.getModel().setGender(n);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, n));
		});
		tfRace.textProperty().addListener( (ov,o,n) -> {
			control.getModel().setRace(n);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, control.getModel()));
			});
		tfHair.textProperty().addListener( (ov,o,n) -> {
			control.getModel().setHairColor(n);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, control.getModel()));
			});
		tfEyes.textProperty().addListener( (ov,o,n) -> {
			control.getModel().setEyeColor(n);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, control.getModel()));
			});
		tfSkin.textProperty().addListener( (ov,o,n) -> {
			control.getModel().setSkinColor(n);
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, control.getModel()));
			});
		tfAge.textProperty().addListener( (ov,o,n) -> {
			try {
				model.setAge(Integer.parseInt(tfAge.getText()));
				tfAge.getStyleClass().remove(INVALID);
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, control.getModel()));
			} catch (NumberFormatException e) {
				tfAge.getStyleClass().add(INVALID);
			}
			});
		tfSize.textProperty().addListener( (ov,o,n) -> {
			try {
				try {
					model.setSize(Integer.parseInt(tfSize.getText()));
					tfSize.getStyleClass().remove(INVALID);
					GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, control.getModel()));
				} catch (NumberFormatException e) {
					tfSize.getStyleClass().add(INVALID);
				}
			} catch (Exception e) {
			}
			});
		tfWeight.textProperty().addListener( (ov,o,n) -> {
			try {
				control.getModel().setWeight(Integer.parseInt(n));
				tfWeight.getStyleClass().remove(INVALID);
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, control.getModel()));
			} catch (Exception e) {
				tfWeight.getStyleClass().add(INVALID);
			}
			});
	}

	//-------------------------------------------------------------------
	public void refresh() {
		logger.debug("refresh");
		tfName.setText(model.getName());
		cbArchetype.setValue(model.getArchetype());
		cbMotivation.setValue(model.getMotivation());
		cbGender.setValue(model.getGender());
		tfRace.setText(model.getRace());
		
		tfSize.setText(String.valueOf(model.getSize()));
		tfAge.setText(String.valueOf(model.getAge()));
		tfWeight.setText(String.valueOf(model.getWeight()));
		tfHair.setText(model.getHairColor());
		tfEyes.setText(model.getEyeColor());
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<BasePluginData> showHelpForProperty() {
		return showHelpFor;
	}

}
