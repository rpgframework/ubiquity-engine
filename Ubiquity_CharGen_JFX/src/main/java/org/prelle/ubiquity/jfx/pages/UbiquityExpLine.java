/**
 *
 */
package org.prelle.ubiquity.jfx.pages;

import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class UbiquityExpLine extends HBox {

	private Label lbExpTotal;
	private Label lbExpInvested;

	//-------------------------------------------------------------------
	public UbiquityExpLine() {
		super(5);
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		lbExpTotal    = new Label("?");
		lbExpInvested = new Label("?");
		lbExpTotal.getStyleClass().add("base");
		lbExpInvested.getStyleClass().add("base");
	}

	//-------------------------------------------------------------------
	protected void initLayout() {
		Label hdExpTotal    = new Label(UbiquityJFXConstants.UI.getString("label.ep.total")+": ");
		Label hdExpInvested = new Label(UbiquityJFXConstants.UI.getString("label.ep.used")+": ");

		getChildren().addAll(hdExpTotal, lbExpTotal, hdExpInvested, lbExpInvested);
		HBox.setMargin(hdExpInvested, new Insets(0,0,0,20));
	}

	//-------------------------------------------------------------------
	public void setData(UbiquityCharacter model) {
		lbExpTotal.setText((model.getExperienceInvested()+model.getExperienceFree())+"");
		lbExpInvested.setText(model.getExperienceInvested()+"");
	}

}
