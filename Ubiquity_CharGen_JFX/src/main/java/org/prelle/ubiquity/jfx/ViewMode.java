/**
 * 
 */
package org.prelle.ubiquity.jfx;

/**
 * @author Stefan
 *
 */
public enum ViewMode {

	GENERATION,
	MODIFICATION,
	VIEW_ONLY
	
}
