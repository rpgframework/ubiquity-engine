package org.prelle.ubiquity.jfx.sections;

import java.text.Collator;
import java.util.Collections;
import java.util.Comparator;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.ubiquity.SkillSpecialization;
import org.prelle.ubiquity.SkillValue;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.cells.ListElemSpecialization;
import org.prelle.ubiquity.jfx.cells.SkillSpecListCell;

import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.util.Callback;

/**
 * @author Stefan Prelle
 *
 */
public class SkillSpecializationSection extends GenericListSection<ListElemSpecialization> {
	
	private SkillValue data;

	//-------------------------------------------------------------------
	public SkillSpecializationSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(title, ctrl, provider);
		list.setCellFactory(new Callback<ListView<ListElemSpecialization>, ListCell<ListElemSpecialization>>() {
			public ListCell<ListElemSpecialization> call(ListView<ListElemSpecialization> param) {
				return new SkillSpecListCell(control);
			}
		});

		setStyle("-fx-pref-width: 25em;");
		setDeleteButton(null);
		setAddButton(null );
		
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		list.getItems().clear();
		
		if (data!=null) {
			for (SkillSpecialization spec : data.getModifyable().getSpecializations()) {
				ListElemSpecialization item = new ListElemSpecialization();
				item.skill = data.getModifyable();
				item.data  = spec;
				item.val   = control.getModel().getSkill(item.skill).getSpecialization(spec);
				list.getItems().add(item);
			}

			final Collator collat = Collator.getInstance();
			Collections.sort(list.getItems(), new Comparator<ListElemSpecialization>() {
				@Override
				public int compare(ListElemSpecialization o1, ListElemSpecialization o2) {
					return collat.compare(o1.data.getName(), o2.data.getName());
				}
			});
		}

	}

	//-------------------------------------------------------------------
	public void setSkillData(SkillValue value) {
		logger.info("Set skill "+value);
		this.data = value;
		refresh();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.jfx.sections.GenericListSection#onAdd()
	 */
	@Override
	protected void onAdd() {
		// TODO Auto-generated method stub
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.jfx.sections.GenericListSection#onDelete()
	 */
	@Override
	protected void onDelete() {
		// TODO Auto-generated method stub
		
	}

}
