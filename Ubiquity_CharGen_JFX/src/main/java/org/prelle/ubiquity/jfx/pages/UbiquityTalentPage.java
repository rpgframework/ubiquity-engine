package org.prelle.ubiquity.jfx.pages;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.ubiquity.BasePluginData;
import org.prelle.ubiquity.FlawValue;
import org.prelle.ubiquity.TalentValue;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.ViewMode;
import org.prelle.ubiquity.jfx.sections.FlawSection;
import org.prelle.ubiquity.jfx.sections.TalentSection;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;

/**
 * @author Stefan Prelle
 *
 */
public class UbiquityTalentPage extends UbiquityManagedScreenPage {

	private ViewMode mode;
	private ScreenManagerProvider provider;

	private TalentSection talents;
	private FlawSection flaws;

	private Section secLine1;

	//-------------------------------------------------------------------
	public UbiquityTalentPage(CharacterController control, ViewMode mode, CharacterHandle handle, ScreenManagerProvider provider) {
		super(control, handle);
		this.setId("ubiquity-talents");
		this.setTitle(control.getModel().getName());
		this.provider = provider;
		this.handle   = handle;
		this.mode = mode;
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;
		
		initComponents();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initBasicData() {
		talents = new TalentSection(UI.getString("section.talents"), charGen, provider);
		flaws   = new FlawSection(UI.getString("section.flaws"), charGen, provider);

		secLine1 = new DoubleSection(talents, flaws);
		getSectionList().add(secLine1);

		// Interactivity
		talents.showHelpForProperty().addListener( (ov,o,n) -> updateHelp( (n!=null)?((TalentValue)n).getTalent():null));
		flaws.showHelpForProperty().addListener( (ov,o,n) -> updateHelp( (n!=null)?((FlawValue)n).getFlaw():null));
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		setPointsNameProperty(UI.getString("label.experience.short"));

		initBasicData();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, charGen.getModel());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, charGen.getModel()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductName()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

}
