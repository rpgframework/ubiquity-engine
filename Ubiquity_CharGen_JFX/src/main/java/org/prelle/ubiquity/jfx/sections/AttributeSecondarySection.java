package org.prelle.ubiquity.jfx.sections;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.attributes.AttributeTableReadOnly;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Stefan Prelle
 *
 */
public class AttributeSecondarySection extends SingleSection {
	
	private AttributeTableReadOnly table;

	private ObjectProperty<Attribute> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public AttributeSecondarySection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title, null);
		table = new AttributeTableReadOnly(ctrl, Attribute.secondaryValues());
		setContent(table);
		
		table.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			showHelpFor.set( (n!=null)?n.getAttribute():null);
		});
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		table.refresh();
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<Attribute> showHelpForProperty() {
		return showHelpFor;
	}

}
