/**
 *
 */
package org.prelle.ubiquity.jfx.wizard;

import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.GenerationTemplate;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;
import org.prelle.ubiquity.jfx.attributes.AttributeTableOld;

import de.rpgframework.RPGFrameworkLoader;
import javafx.event.EventTarget;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;

/**
 * @author Stefan
 *
 */
public class WizardPageAttributes extends WizardPage implements GenerationEventListener {

	protected static Logger logger = LogManager.getLogger("ubiquity.jfx");

	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	private CharacterGenerator charGen;

	// Content
	private Label    explain;
	private AttributeTableOld table;
	private Label focusAttributeName;
	private Label reference;
	private Label focusAttributeDescription;

	private VBox content;
	private Label pointsLeft;
	private Attribute focussed;

	//--------------------------------------------------------------------
	public WizardPageAttributes(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;

		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);

		// Select standard
		update(Attribute.BODY);
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectAttributes.title"));

		pointsLeft = new Label();
		/*
		 * Content
		 */
		explain = new Label(UI.getString("wizard.selectAttributes.descr"));
		explain.setWrapText(true);

		table = new AttributeTableOld(charGen.getAttributeController());
		table.setData(charGen.getTemplate(), charGen.getModel());

		focusAttributeName = new Label();

		reference = new Label();

		focusAttributeDescription = new Label();
		focusAttributeDescription.setWrapText(true);
		focusAttributeDescription.setMaxHeight(Double.MAX_VALUE);
		focusAttributeDescription.setTextAlignment(TextAlignment.JUSTIFY);
		focusAttributeDescription.setAlignment(Pos.TOP_LEFT);
		focusAttributeDescription.setPrefHeight(90);

		content = new VBox();
		content.setSpacing(5);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Content node
		explain.setPrefWidth(650);
		focusAttributeDescription.setPrefWidth(650);

		content.getChildren().addAll(explain,  table);
		content.getChildren().addAll(focusAttributeName, reference, focusAttributeDescription);
		super.setContent(content);

		// Side node
		VBox side = new VBox();
		Label lblPointsLeft = new Label(UI.getString("wizard.selectAttributes.pointsLeft"));
		lblPointsLeft.getStyleClass().add("text-body");
		side.getChildren().addAll(lblPointsLeft, pointsLeft);
		side.setAlignment(Pos.TOP_CENTER);
		setSide(side);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		explain.getStyleClass().add("text-body");
		focusAttributeName.getStyleClass().add("section-head");
		focusAttributeName.setWrapText(true);
		focusAttributeDescription.getStyleClass().add("text-body");
		pointsLeft.getStyleClass().add("text-subheader");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		table.setOnAttributeSelected(event -> mouseFocusChanged(event));
	}

	//-------------------------------------------------------------------
	private void mouseFocusChanged(MouseEvent event) {
		EventTarget target = event.getTarget();
		Attribute newAttr = null;
		if ((target instanceof Button) && (((Button)target).getUserData() instanceof Attribute)) {
			newAttr = (Attribute) ((Button)target).getUserData();
		}
		if ((target instanceof Label) && (((Label)target).getUserData() instanceof Attribute)) {
			newAttr = (Attribute) ((Label)target).getUserData();
		}

		if (newAttr==null || newAttr==focussed)
			return;
		logger.debug("Focus changes to "+newAttr);
		update(newAttr);
		focussed = newAttr;
	}

	//-------------------------------------------------------------------
	private void update(Attribute value) {
		// Update page reference
		String key = "attribute."+value.name().toLowerCase()+".page";
		int page = 0;
//		if (CORE.containsKey(key)) {
//			try {
//				page = Integer.parseInt(CORE.getString(key));
//			} catch (NumberFormatException e) {
//				logger.error("Property value of '"+key+"' in "+CORE.getBaseBundleName()+" is not an integer");
//			}
//		} else
//			logger.error("Missing key '"+key+"' in "+CORE.getBaseBundleName());
//		String product = CORE.getString("plugin.CORE.productname.full");
//		String format = UI.getString("format.pagereference");
//		if (page>0)
//			reference.setText(String.format(format, product, page));
//		else
//			reference.setText(String.format(format, product, "?"));

		// Detect the required license
		String license = "CORE";
		try {
			if (RPGFrameworkLoader.getInstance().getLicenseManager().hasLicense(charGen.getRuleset().getRoleplayingSystem(), license)) {
				focusAttributeName.setText(value.getName());
//				focusAttributeDescription.setText( HELP.getString("attribute."+value.name().toLowerCase()+".desc") );
			} else {
				focusAttributeName.setText(null);
				focusAttributeDescription.setText(null);
			}
		} catch (MissingResourceException e) {
			logger.error("Missing "+e.getKey());
			focusAttributeName.setText(null);
			focusAttributeDescription.setText(e.getMessage());
		}
	}

	//-------------------------------------------------------------------
	private void refresh() {
		table.refresh();
		pointsLeft.setText(String.valueOf(charGen.getAttributeController().getPointsLeft()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case POINTS_LEFT_ATTRIBUTES:
			pointsLeft.setText(String.valueOf(charGen.getAttributeController().getPointsLeft()));
			break;
		case ATTRIBUTE_CHANGED:
			logger.debug("handle "+event);
			refresh();
			break;
		case TEMPLATE_CHANGED:
			table.setData(
					(GenerationTemplate) event.getKey(),
					charGen.getModel());
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft(CloseType type) {
		logger.debug("pageLeft("+type+")");
//		charGen.selectMotivation(spinMotivation.getValue());
	}

}

