/**
 * 
 */
package org.prelle.ubiquity.jfx.wizard;

import java.io.InputStream;
import java.util.List;
import java.util.MissingResourceException;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.CloseType;
import org.prelle.javafx.Wizard;
import org.prelle.javafx.WizardPage;
import org.prelle.ubiquity.Resource;
import org.prelle.ubiquity.ResourceValue;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.ResourceController;
import org.prelle.ubiquity.chargen.CharacterGenerator;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import de.rpgframework.RPGFrameworkLoader;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.control.ListView;
import javafx.scene.image.Image;
import javafx.scene.input.MouseEvent;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.TextAlignment;
import javafx.util.Callback;

/**
 * @author Stefan
 *
 */
public class WizardPageResources extends WizardPage implements GenerationEventListener {
	
	protected static Logger logger = LogManager.getLogger("ubiquity.jfx");
	
	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	private UbiquityRuleset ruleset;
	private CharacterGenerator charGen;
	
	// Content
	private Label    explain;
	private ListView<Resource> listPossible;
	private ListView<ResourceValue> listSelected;
	
	private VBox boxSpecial;
	private Label descName;
	private Label reference;
	private Label descText;
	
	private VBox content;
	private Label pointsLeft;

	//--------------------------------------------------------------------
	public WizardPageResources(Wizard wizard, CharacterGenerator charGen) {
		super(wizard);
		this.charGen = charGen;
		this.ruleset = charGen.getRuleset();
		
		initComponents();
		initLayout();
		initStyle();
		initInteractivity();
		GenerationEventDispatcher.addListener(this);
		
		// Select standard
		refresh();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("wizard.selectResource.title"));

		pointsLeft = new Label();
		/*
		 * Content
		 */
		explain = new Label(UI.getString("wizard.selectResource.descr"));
		explain.setWrapText(true);

		listPossible = new ListView<Resource>();
		listPossible.setCellFactory(new Callback<ListView<Resource>, ListCell<Resource>>() {
			public ListCell<Resource> call(ListView<Resource> param) {
				ResourceListCell cell = new ResourceListCell(WizardPageResources.this);
				return cell;
			}
		});
		listPossible.setStyle("-fx-border-width: 1px; -fx-border-color: black;");
		Label placeholderPossible = new Label(UI.getString("wizard.selectResource.available.placeholder"));
		placeholderPossible.setWrapText(true);
		listPossible.setPlaceholder(placeholderPossible);
		
		listSelected = new ListView<ResourceValue>();
		listSelected.setCellFactory(new Callback<ListView<ResourceValue>, ListCell<ResourceValue>>() {
			public ListCell<ResourceValue> call(ListView<ResourceValue> param) {
				ResourceValueListCell lc = new ResourceValueListCell(charGen.getResourceController());
				lc.prefWidthProperty().bind(listSelected.widthProperty().subtract(10));
				return lc;
			}
		});
		listSelected.setStyle("-fx-border-width: 1px; -fx-border-color: black;");
		listSelected.getItems().addAll(charGen.getModel().getResources());
		
		/*
		 * Column showing per skill information
		 */
		boxSpecial = new VBox(5);
		descName = new Label();
		
		reference = new Label();
		
		descText = new Label();
		descText.setWrapText(true);
		descText.setMaxHeight(Double.MAX_VALUE);
		descText.setTextAlignment(TextAlignment.JUSTIFY);
		descText.setAlignment(Pos.TOP_LEFT);
//		focusSkillDescription.setPrefHeight(90);
		
		content = new VBox();
		content.setSpacing(5);
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		// Content node
		explain.setMaxWidth(720);
//		focusSkillDescription.setPrefWidth(650);
		
		/*
		 * Info column
		 */
		VBox boxInfo = new VBox(5);
		boxInfo.getChildren().addAll(descName, boxSpecial, reference, descText);
		
		HBox sideByside = new HBox(20);
		sideByside.getChildren().addAll(listPossible, listSelected, boxInfo);
		listPossible.setStyle("-fx-pref-width: 18em");
		listSelected.setStyle("-fx-pref-width: 18em");
		boxInfo.setStyle("-fx-pref-width: 18em");
		HBox.setHgrow(listPossible, Priority.NEVER);
		HBox.setHgrow(boxInfo, Priority.NEVER);
		
		content.getChildren().addAll(explain,  sideByside);
		VBox.setVgrow(sideByside, Priority.ALWAYS);
		sideByside.setMaxHeight(Double.MAX_VALUE);
		super.setContent(content);
		
		// Side node
		VBox side = new VBox();
		Label lblPointsLeft = new Label(UI.getString("wizard.selectAttributes.pointsLeft"));
		lblPointsLeft.getStyleClass().add("text-body");
		side.getChildren().addAll(lblPointsLeft, pointsLeft);
		side.setAlignment(Pos.TOP_CENTER);
		setSide(side);
		
//			String fname = "data/space1889/archetype_soldier.png";
//			InputStream in = getClass().getClassLoader().getResourceAsStream(fname);
//			if (in!=null) {
//				setImage(new Image(in));
//			} else
//				logger.warn("Missing image at "+fname);
	}

	//-------------------------------------------------------------------
	private void initStyle() {
		explain.getStyleClass().add("text-body");
		descName.getStyleClass().add("section-head");
		descName.setWrapText(true);
		descText.getStyleClass().add("text-body");
		descText.setWrapText(true);
		pointsLeft.getStyleClass().add("text-subheader");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		listPossible.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			selectionChanged(n);
		});
		listSelected.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			if (n==null)
				selectionChanged(null);
			else
				selectionChanged(n.getModifyable());
		});
	}
	
	//-------------------------------------------------------------------
	private void selectionChanged(Resource selected) {
		logger.debug("Selected "+selected);
		boxSpecial.getChildren().clear();

		if (selected==null) {
			descName.setText(null);
			reference.setText(null);
			descText.setText(null);
		} else {
			descName.setText(selected.getName());

			// Update page reference
			int page = selected.getPage();
			String product = selected.getProductName();
			String format = UI.getString("format.pagereference");
			if (page>0)
				reference.setText(String.format(format, product, page));
			else
				reference.setText(String.format(format, product, "?"));

			// Detect the required license
			String license = selected.getPlugin().getID();
			logger.debug("License to show "+selected+" is "+license);
			try {
				if (RPGFrameworkLoader.getInstance().getLicenseManager().hasLicense(ruleset.getRoleplayingSystem(), license))
					if (selected.getHelpResourceBundle()!=null)
						descText.setText( selected.getHelpResourceBundle().getString("resource."+selected.getId()+".desc") );
				else
					descText.setText(null);
			} catch (MissingResourceException e) {
				logger.error("Missing "+e.getKey()+" in "+selected.getHelpResourceBundle().getBaseBundleName());
				descText.setText(e.getMessage());
			}
		}
	}

	//-------------------------------------------------------------------
	private void refresh() {
		listPossible.getItems().clear();
		listSelected.getItems().clear();
		
		listPossible.getItems().addAll(charGen.getResourceController().getAvailableResources());
		listSelected.getItems().addAll(charGen.getModel().getResources());
		pointsLeft.setText(String.valueOf(charGen.getResourceController().getPointsLeft()));
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case POINTS_LEFT_TALENTS_RESOURCES:
			logger.debug("RCV "+event);
			pointsLeft.setText(String.valueOf(charGen.getResourceController().getPointsLeft()));
			break;
		case RESOURCE_AVAILABLE_CHANGED:
			logger.debug("RCV "+event);
			listPossible.getItems().clear();
			listPossible.getItems().addAll((List<Resource>) event.getKey());
			break;
		case RESOURCE_ADDED:
			logger.debug("RCV "+event);
			listSelected.getItems().clear();
			listSelected.getItems().addAll(charGen.getModel().getResources());
			break;
		case RESOURCE_REMOVED:
			logger.debug("RCV "+event);
			listSelected.getItems().remove( (ResourceValue)event.getKey() );
			break;
		case RESOURCE_CHANGED:
			logger.debug("RCV "+event);
			listSelected.getItems().clear();
			listSelected.getItems().addAll(charGen.getModel().getResources());
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Called from Wizard when page is left
	 */
	public void pageLeft(CloseType type) {
		logger.debug("pageLeft("+type+")");
//		charGen.selectMotivation(spinMotivation.getValue());
	}

	//--------------------------------------------------------------------
	void onResourceDoubleClicked(Resource current) {
		// TODO Auto-generated method stub
		logger.debug("double click on "+current);
		charGen.getResourceController().select(current);
	}

}

class ResourceListCell extends ListCell<Resource> implements EventHandler<MouseEvent> {
	
	private WizardPageResources parent;
	private Label lblName;
	private Label lblRequire;
	private VBox  layout;
	
	private Resource current;
	
	//--------------------------------------------------------------------
	public ResourceListCell(WizardPageResources parent) {
		this.parent = parent;
		lblName = new Label();
		lblName.getStyleClass().add("text-body");
		lblRequire = new Label();
		lblRequire.getStyleClass().add("text-tertiary-info");
		layout = new VBox();
		layout.getChildren().addAll(lblName, lblRequire);
		layout.setStyle("-fx-padding: 2px; -fx-border-color: black; -fx-border-width: 1px;");
		
		setOnMouseClicked(this);
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(Resource item, boolean empty) {
		super.updateItem(item, empty);
		current = item;
		
		if (empty) {
			setGraphic(null);
			current = null;
			return;
		}
		
		lblName.setText(item.getName());
		lblRequire.setText("");
		
		setGraphic(layout);
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.event.EventHandler#handle(javafx.event.Event)
	 */
	@Override
	public void handle(MouseEvent event) {
		if (current!=null && event.getClickCount()==2)
			parent.onResourceDoubleClicked(current);
	}
}

//--------------------------------------------------------------------
//--------------------------------------------------------------------
class ResourceValueListCell extends ListCell<ResourceValue> implements EventHandler<MouseEvent> {
	
	private ResourceController ctrl;
	private Label lblName;
	private Button btnDec;
	private Button btnInc;
	private HBox  layout;
	
	private ResourceValue current;
	
	//--------------------------------------------------------------------
	public ResourceValueListCell(ResourceController control) {
		this.ctrl = control;
		lblName = new Label();
		lblName.getStyleClass().add("text-body");
		lblName.setAlignment(Pos.CENTER_LEFT);
		lblName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		btnDec = new Button("-");
		btnDec.getStyleClass().add("text-small-heading");
		btnDec.setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-border-radius: 5px;");
		btnDec.setOnAction(event -> control.decrease(current));
		btnInc = new Button("+");
		btnInc.getStyleClass().add("text-small-heading");
		btnInc.setStyle("-fx-border-color: black; -fx-border-width: 1px; -fx-border-radius: 5px;");
		btnInc.setOnAction(event -> control.increase(current));
		layout = new HBox(5);
		layout.getChildren().addAll(lblName, btnDec, btnInc);
//		layout.setStyle("-fx-padding: 2px; -fx-border-color: black; -fx-border-width: 1px;");
		HBox.setHgrow(lblName, Priority.ALWAYS);

		setOnMouseClicked(this);
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(ResourceValue item, boolean empty) {
		super.updateItem(item, empty);
		current = item;
		
		if (empty) {
			setGraphic(null);
			current = null;
			return;
		}
		
		setGraphic(layout);
		lblName.setText(String.valueOf(item));
		btnInc.setDisable(!ctrl.canBeIncreased(current));
		btnDec.setDisable(!ctrl.canBeDecreased(current));
	}

	//--------------------------------------------------------------------
	/**
	 * @see javafx.event.EventHandler#handle(javafx.event.Event)
	 */
	@Override
	public void handle(MouseEvent event) {
		if (current!=null && event.getClickCount()==2)
			ctrl.deselect(current);
	}
	
}