package org.prelle.ubiquity.jfx.sections;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.skills.SkillTreePane;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.ReadOnlyObjectProperty;
import javafx.beans.property.SimpleObjectProperty;

/**
 * @author Stefan Prelle
 *
 */
public class SkillSection extends SingleSection {
	
	private SkillTreePane table;

	private ObjectProperty<Skill> showHelpFor = new SimpleObjectProperty<>();

	//-------------------------------------------------------------------
	public SkillSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title, null);
		table = new SkillTreePane(ctrl.getSkillController(), ctrl.getTalentController(), ctrl.getModel());
		setContent(table);
		
		table.getSelectionModel().selectedItemProperty().addListener( (ov,o,n) -> {
			showHelpFor.set( (n!=null)?n.getValue():null);
		});
		
		setStyle("-fx-pref-height: 60em;");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		table.refresh();
	}

	//-------------------------------------------------------------------
	public ReadOnlyObjectProperty<Skill> showHelpForProperty() {
		return showHelpFor;
	}

}
