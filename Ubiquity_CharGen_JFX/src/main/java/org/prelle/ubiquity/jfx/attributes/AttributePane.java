/**
 * 
 */
package org.prelle.ubiquity.jfx.attributes;

import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.AttributeValue;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.charctrl.AttributeController;
import org.prelle.ubiquity.charctrl.CharGenMode;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.ColumnConstraints;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.HBox;

/**
 * @author prelle
 *
 */
public class AttributePane extends GridPane implements GenerationEventListener {
	
	private static Logger logger = LogManager.getLogger("chargen.ui.attr");
	
	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	class AttributeField extends HBox {
		private Button dec, inc;
		private TextField value;
		
		public AttributeField() {
			dec  = new Button("<");
			inc  = new Button(">");
			value = new TextField();
			value.setPrefColumnCount(1);
			this.getChildren().addAll(dec, value, inc);
		}
		public void setText(String val) {
			this.value.setText(val);
		}
	}

	private AttributeController control;
	private UbiquityCharacter     model;
	private CharGenMode         mode;

	private Label headAttr, headPoints, headMod, headStart, headValue, headTemp;
	private Label headDeri, headDPoints, headDMod, headDValue;
	private Map<Attribute, Label> modification;
	private Map<Attribute, Label> eqModification;
	private Map<Attribute, AttributeField> distributed;
	private Map<Attribute, Label> derived;
	private Map<Attribute, Label> finalValue;
	private Map<Attribute, Label> startValue;
	private Map<Attribute, Label> costValue;

	//--------------------------------------------------------------------
	public AttributePane(AttributeController cntrl, CharGenMode mode) {
		this.control = cntrl;
		this.mode    = mode;
		assert control!=null;
		assert mode   !=null;

		modification = new HashMap<Attribute, Label>();
		eqModification = new HashMap<Attribute, Label>();
		distributed  = new HashMap<Attribute, AttributeField>();
		finalValue   = new HashMap<Attribute, Label>();
		startValue   = new HashMap<Attribute, Label>();
		derived      = new HashMap<Attribute, Label>();
		costValue    = new HashMap<Attribute, Label>();
		
		doInit();
		doLayout();
	}

	//-------------------------------------------------------------------
	private void doInit() {
		super.setVgap(0);
		super.setHgap(0);
		
		headAttr   = new Label(UI.getString("label.attributes"));
		headPoints = new Label(UI.getString("label.points"));
		headMod    = new Label(UI.getString("label.modified.short"));
		headStart  = new Label(UI.getString("label.start"));
		headValue  = new Label(UI.getString("label.value"));
		headTemp   = new Label(UI.getString("label.cost"));
		headDeri   = new Label(UI.getString("label.derived_values"));
		headDPoints= new Label(UI.getString("label.points"));
		headDMod   = new Label(UI.getString("label.modified.short"));
		headDValue = new Label(UI.getString("label.value"));
		headAttr.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headPoints.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headMod.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headStart.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headValue.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headTemp.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headDeri.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headDPoints.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headDMod.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		headDValue.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
		
		headAttr.getStyleClass().add("table-head");
		headPoints.getStyleClass().add("table-head");
		headMod.getStyleClass().add("table-head");
		headStart.getStyleClass().add("table-head");
		headValue.getStyleClass().add("table-head");
		headTemp.getStyleClass().add("table-head");
		headDeri.getStyleClass().add("table-head");
		headDPoints.getStyleClass().add("table-head");
		headDMod.getStyleClass().add("table-head");
		headDValue.getStyleClass().add("table-head");
		
		headPoints.setAlignment(Pos.CENTER);
		headValue.setAlignment(Pos.CENTER);
		
		for (final Attribute attr : Attribute.primaryValues()) {
			AttributeField value = new AttributeField();
			Label modVal= new Label();
			Label finVal= new Label();
			Label startVal= new Label();
			
			finalValue  .put(attr, finVal);
			distributed .put(attr, value);
			modification.put(attr, modVal);
			startValue  .put(attr, startVal);
			costValue   .put(attr, new Label());
		}
		
		for (final Attribute attr : Attribute.secondaryValues()) {
			Label value = new Label();
			Label modVal= new Label();
			Label eqModVal= new Label();
			Label finVal= new Label();
			Label startVal= new Label();
			
			finalValue  .put(attr, finVal);
			derived     .put(attr, value);
			modification.put(attr, modVal);
			eqModification.put(attr, eqModVal);
			startValue  .put(attr, startVal);
		}
		
	}

	//-------------------------------------------------------------------
	private void doLayout() {
		this.add(headAttr  , 0,0, 2,1);
		this.add(headPoints, 2,0);
		this.add(headMod   , 3,0);
		this.add(headValue , 4,0);
		this.add(headDeri  , 6,0, 2,1);
		this.add(headDPoints, 8,0);
		this.add(headDMod   , 9,0);
		this.add(headDValue ,10,0);

		int y=0;
		for (final Attribute attr : Attribute.primaryValues()) {
			y++;
			Label longName  = new Label(attr.getName());
			Label shortName = new Label(attr.getShortName());
			Label modVal    = modification.get(attr);
			AttributeField points = distributed.get(attr);
			Label finVal    = finalValue.get(attr);
			Label startVal  = startValue.get(attr);
			Label temp      = costValue.get(attr);
			
			String lineStyle = ((y%2)==0)?"even":"odd";
			longName.getStyleClass().addAll(lineStyle, "border-left");
			shortName.getStyleClass().add(lineStyle);
			modVal.getStyleClass().add(lineStyle);
			points.getStyleClass().add(lineStyle);
			finVal.getStyleClass().add(lineStyle);
			startVal.getStyleClass().add(lineStyle);
			
			temp.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			temp.getStyleClass().add(lineStyle);
			if (y==8) {
				longName.getStyleClass().add("border-bottom-left");
				shortName.getStyleClass().add("border-bottom");
				modVal.getStyleClass().add("border-bottom");
				points.getStyleClass().add("border-bottom");
				finVal.getStyleClass().add("border-bottom");
				startVal.getStyleClass().add("border-bottom");
				temp.getStyleClass().add("border-bottom");
			}
			

			longName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			shortName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			modVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			points.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			finVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			startVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			this.add(longName , 0, y);
			this.add(shortName, 1, y);
			switch (mode) {
			case CREATING:
				this.add( distributed.get(attr), 2, y);
				this.add(modification.get(attr), 3, y);
				this.add(  finalValue.get(attr), 4, y);
				break;
			case LEVELING:
				this.add( startValue.get(attr), 2, y);
				this.add(distributed.get(attr), 3, y);
				this.add(  costValue.get(attr), 4, y);
				break;
			}
		}
		
		/*
		 * Separator
		 */
		for (int i=0; i<=y; i++) {
			Label toAdd = new Label("  ");
			toAdd.getStyleClass().add("gridsep");
			toAdd.setMaxHeight(Double.MAX_VALUE);
			this.add(toAdd, 5,i);
		}
		
		/*
		 * Derived attributes
		 */
		y=0;
		for (final Attribute attr : Attribute.secondaryValues()) {
			y++;
			Label longName  = new Label(attr.getName());
			Label shortName = new Label(attr.getShortName());
			Label modVal    = modification.get(attr);
			Label eqModVal  = eqModification.get(attr);
			Label points    = derived.get(attr);
			Label finVal    = finalValue.get(attr);
			
			String lineStyle = ((y%2)==0)?"even":"odd";
			longName.getStyleClass().add(lineStyle);
			shortName.getStyleClass().add(lineStyle);
			modVal.getStyleClass().add(lineStyle);
			eqModVal.getStyleClass().add(lineStyle);
			points.getStyleClass().add(lineStyle);
			finVal.getStyleClass().addAll(lineStyle, "border-right");
			
			if (y==8) {
				longName.getStyleClass().add("border-bottom");
				shortName.getStyleClass().add("border-bottom");
				points.getStyleClass().add("border-bottom");
				modVal.getStyleClass().add("border-bottom");
				eqModVal.getStyleClass().add("border-bottom");
				finVal.getStyleClass().add("border-bottom-right");
			}

			longName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			shortName.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			points.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			modVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			eqModVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			finVal.setMaxSize(Double.MAX_VALUE, Double.MAX_VALUE);
			
			this.add(longName , 6, y);
			this.add(shortName, 7, y);
			this.add(points   , 8, y);
			this.add(modVal   , 9, y);
			this.add(eqModVal , 10, y);
			this.add(finVal   , 11, y);
		}
		
		
		// Column alignment
		ObservableList<ColumnConstraints> constraints = getColumnConstraints();
		ColumnConstraints rightAlign = new ColumnConstraints();
		constraints.add(new ColumnConstraints());
		constraints.add(new ColumnConstraints());
		switch (mode) {
		case CREATING:
			constraints.add(new ColumnConstraints(100));
			constraints.add(rightAlign);
			constraints.add(rightAlign);
			break;
		case LEVELING:
			constraints.add(rightAlign);
			constraints.add(new ColumnConstraints());
			constraints.add(rightAlign);
			break;
		}
	}

	//-------------------------------------------------------------------
	private void doInteractivity() {
		for (final Attribute attr : Attribute.primaryValues()) {
			AttributeField field = distributed.get(attr);
			field.inc.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					control.increase(attr);
				}
			});
			field.dec.setOnAction(new EventHandler<ActionEvent>() {
				public void handle(ActionEvent event) {
					control.decrease(attr);
				}
			});
		}
	}

	//-------------------------------------------------------------------
	private void updateContent() {
		for (Attribute attr : Attribute.primaryValues()) {
			Label start_l = startValue.get(attr);
			Label modif_l = modification.get(attr);
			AttributeField field = distributed.get(attr);
			Label cost_l  = costValue.get(attr);
			Label final_l = finalValue.get(attr);
			
			AttributeValue data = model.getAttribute(attr);
			
			String modString = (data.getModifier()==0)?"":String.valueOf(data.getModifier());
					
			switch (mode) {
			case CREATING:
				field.setText(Integer.toString(data.getPoints()));
				final_l.setText(String.valueOf(data.getValue()));
				break;
			case LEVELING:
				field.setText(Integer.toString(data.getValue()));
				break;
			}
			modif_l.setText(modString);
			cost_l .setText(Integer.toString(control.getIncreaseCost(attr)));
			field.inc.setDisable(!control.canBeIncreased(attr));
			field.dec.setDisable(!control.canBeDecreased(attr));
		}
		for (final Attribute attr : Attribute.secondaryValues()) {
			Label modif_l = modification.get(attr);
			Label deriv_l = derived.get(attr);
			Label final_l = finalValue.get(attr);
			Label eqModif_l = eqModification.get(attr);

			AttributeValue data = model.getAttribute(attr);
			deriv_l.setText(Integer.toString(data.getPoints()));
			if (data.getModifier()!=0)
				modif_l.setText(Integer.toString(data.getModifier()));
			else
				modif_l.setText(null);
			final_l.setText(Integer.toString(data.getValue()));
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		if (event.getType()==GenerationEventType.ATTRIBUTE_CHANGED) {
			logger.debug("handle "+event);
			Attribute      attribute = (Attribute) event.getKey();
			AttributeValue set       = (AttributeValue) event.getValue();
			Label modVal = this.modification.get(attribute);
			Label finV_l = finalValue.get(attribute);
			
			if (attribute.isPrimary()) {
				// Update increase and decrease buttons
				AttributeField field     = distributed.get(attribute);
				field.inc.setDisable(!control.canBeIncreased(attribute));
				field.dec.setDisable(!control.canBeDecreased(attribute));
				if (field !=null) field .setText(Integer.toString(set.getValue()));
			} else {
				Label derive = derived.get(attribute);
				derive.setText(Integer.toString(set.getPoints()));
			}
			// Update values
			if (modVal!=null) { 
				modVal.setText(Integer.toString(set.getModifier()));
			} else {
				logger.warn("No label for modifier "+attribute+" in "+modification.keySet());
			}
			if (finV_l!=null) finV_l.setText(Integer.toString(set.getValue()));
			updateContent();
		} else if (event.getType()==GenerationEventType.POINTS_LEFT_ATTRIBUTES) {
			logger.debug("handle "+event);
			updateContent();
		} else if (event.getType()==GenerationEventType.EXPERIENCE_CHANGED) {
			logger.debug("handle "+event);
			updateContent();
		}
	}

	//-------------------------------------------------------------------
	public void setData(UbiquityCharacter model) {
		this.model = model;
		updateContent();
		doInteractivity();
		
		GenerationEventDispatcher.addListener(this);	
	}

}
