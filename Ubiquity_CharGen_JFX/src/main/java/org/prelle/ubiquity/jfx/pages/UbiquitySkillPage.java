package org.prelle.ubiquity.jfx.pages;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.ViewMode;
import org.prelle.ubiquity.jfx.sections.SkillSection;
import org.prelle.ubiquity.jfx.sections.SkillSpecializationSection;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;

/**
 * @author Stefan Prelle
 *
 */
public class UbiquitySkillPage extends UbiquityManagedScreenPage {

	private ViewMode mode;
	private ScreenManagerProvider provider;

	private SkillSection skills;
	private SkillSpecializationSection special;

	//-------------------------------------------------------------------
	public UbiquitySkillPage(CharacterController control, ViewMode mode, CharacterHandle handle, ScreenManagerProvider provider) {
		super(control, handle);
		this.setId("ubiquity-skills");
		this.setTitle(control.getModel().getName());
		this.provider = provider;
		this.handle   = handle;
		this.mode = mode;
		if (this.mode==null)
			this.mode = ViewMode.MODIFICATION;
		
		initComponents();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initLine1() {
		skills  = new SkillSection(UI.getString("section.skills"), charGen, provider);
		special = new SkillSpecializationSection(UI.getString("section.specializations"), charGen, provider);

		Section line1 = new DoubleSection(skills, special);
		getSectionList().add(line1);

		// Interactivity
		skills.showHelpForProperty().addListener( (ov,o,n) -> updateHelp( n));
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		setPointsNameProperty(UI.getString("label.experience.short"));

		initLine1();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, charGen.getModel());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, charGen.getModel()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(Skill data) {
		if (data!=null) {
			special.setSkillData(charGen.getModel().getSkill(data));
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductName()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			special.setSkillData(null);
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

}
