package org.prelle.ubiquity.jfx.sections;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.PropertyResourceBundle;
import java.util.prefs.Preferences;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.javafx.SymbolIcon;
import org.prelle.rpgframework.jfx.SingleSection;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import de.rpgframework.RPGFramework;
import javafx.scene.control.Button;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.stage.FileChooser;
import javafx.stage.Stage;

/**
 * @author prelle
 *
 */
public class PortraitSection extends SingleSection {

	private final static Logger logger = LogManager.getLogger(UbiquityJFXConstants.BASE_LOGGER_NAME);

	private static Preferences CONFIG = Preferences.userRoot().node(RPGFramework.LAST_OPEN_DIR);

	private static PropertyResourceBundle UI = UbiquityJFXConstants.UI;

	private CharacterController control;
	private UbiquityCharacter model;

	private ImageView ivPortrait;
	private Button btnAdd;
	private Button btnDel;

	//-------------------------------------------------------------------
	public PortraitSection(String title, CharacterController ctrl, ScreenManagerProvider provider) {
		super(provider, title, null);
		this.control = ctrl;
		model = ctrl.getModel();

		initComponents();
		initLayout();
		refresh();
		initInteractivity();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		ivPortrait = new ImageView();
		ivPortrait.setFitHeight(150);
		ivPortrait.setFitWidth(150);
		ivPortrait.setPreserveRatio(true);
		btnAdd = new Button(null, new SymbolIcon("add"));
		btnDel = new Button(null, new SymbolIcon("delete"));

		setDeleteButton( btnDel );
		setAddButton( btnAdd );
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		setContent(ivPortrait);
		ivPortrait.setStyle("-fx-fx-padding: 0.3em; -fx-min-width: 10em");
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		btnAdd.setOnAction(ev -> openImage());
		btnDel.setOnAction(ev -> removeImage());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.rpgframework.jfx.Section#refresh()
	 */
	@Override
	public void refresh() {
		model = control.getModel();

		if (model.getImage()!=null) {
			ivPortrait.setImage(new Image(new ByteArrayInputStream(model.getImage())));
		} else {
			ivPortrait.setImage(null);
		}
	}

	//-------------------------------------------------------------------
	private void openImage() {
		logger.debug("openImage");
		FileChooser chooser = new FileChooser();
		chooser.setTitle(UI.getString("appearance.filechooser.title"));
		String lastDir = CONFIG.get(RPGFramework.PROP_LAST_OPEN_IMAGE_DIR, System.getProperty("user.home"));
		chooser.setInitialDirectory(new File(lastDir));
		chooser.getExtensionFilters().addAll(
				new FileChooser.ExtensionFilter("All", "*.*"),
				new FileChooser.ExtensionFilter("JPG", "*.jpg"),
				new FileChooser.ExtensionFilter("PNG", "*.png")
				);
		File selection = chooser.showOpenDialog(new Stage());
		if (selection!=null) {
			CONFIG.put(RPGFramework.PROP_LAST_OPEN_IMAGE_DIR, selection.getParentFile().getAbsolutePath().toString());
			try {
				byte[] imgBytes = Files.readAllBytes(selection.toPath());
				ivPortrait.setImage(new Image(new ByteArrayInputStream(imgBytes)));
				control.getModel().setImage(imgBytes);
				GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, null));
			} catch (IOException e) {
				logger.warn("Failed loading image from "+selection+": "+e);
			}
		}
	}

	//-------------------------------------------------------------------
	private void removeImage() {
		ivPortrait.setImage(null);
		control.getModel().setImage(null);
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.BASE_DATA_CHANGED, null));
	}

}
