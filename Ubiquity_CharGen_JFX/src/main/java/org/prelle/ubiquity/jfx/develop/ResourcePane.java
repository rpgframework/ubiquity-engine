package org.prelle.ubiquity.jfx.develop;

import java.util.PropertyResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Resource;
import org.prelle.ubiquity.ResourceValue;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.charctrl.ResourceController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;

import javafx.beans.property.SimpleIntegerProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.control.Button;
import javafx.scene.control.ChoiceBox;
import javafx.scene.control.ScrollPane;
import javafx.scene.control.SelectionMode;
import javafx.scene.control.Spinner;
import javafx.scene.control.TableCell;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableColumn.CellDataFeatures;
import javafx.scene.control.TableColumn.CellEditEvent;
import javafx.scene.control.TableView;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.control.cell.TextFieldTableCell;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;
import javafx.scene.text.Text;
import javafx.util.Callback;
import javafx.util.StringConverter;

public class ResourcePane extends VBox implements GenerationEventListener, EventHandler<ActionEvent> {

	private static Logger logger = LogManager.getLogger(UbiquityJFXConstants.BASE_LOGGER_NAME);

	private static PropertyResourceBundle uiResources = UbiquityJFXConstants.UI;

	private ResourceController control;
	private UbiquityCharacter model;

	private ChoiceBox<Resource> addChoice;
	private Button add;
	private TableView<ResourceValue> table;

	private TableColumn<ResourceValue, String> nameCol;
	private TableColumn<ResourceValue, Number> valueCol;
	private TableColumn<ResourceValue, String> notesCol;

	//--------------------------------------------------------------------
	/**
	 * @param withNotes Display a 'Notes' column
	 */
	public ResourcePane(ResourceController ctrl, boolean withNotes) {
		this.control = ctrl;

		GenerationEventDispatcher.addListener(this);
		doInit();
		doValueFactories();
		if (!withNotes)
			table.getColumns().remove(notesCol);

		initInteractivity();
	}

	//--------------------------------------------------------------------
	public void setData(UbiquityCharacter model) {
		this.model = model;

		updateContent();
	}

	//--------------------------------------------------------------------
	@SuppressWarnings("unchecked")
	private void doInit() {
		addChoice = new ChoiceBox<>();
		addChoice.setPrefWidth(200);
		addChoice.setConverter(new StringConverter<Resource>() {
			public String toString(Resource object) {return object.getName();}
			public Resource fromString(String string) {return null;}
		});
		addChoice.getItems().addAll(control.getAvailableResources());
		add = new Button(uiResources.getString("button.add"));
		HBox addLine = new HBox(5);
		addLine.getChildren().addAll(addChoice,add);
		HBox.setHgrow(addChoice, Priority.ALWAYS);

		table = new TableView<ResourceValue>();
		table.setColumnResizePolicy(TableView.CONSTRAINED_RESIZE_POLICY);
		table.getSelectionModel().setSelectionMode(SelectionMode.MULTIPLE);
        table.setPlaceholder(new Text(uiResources.getString("placeholder.resource")));
        table.setEditable(true);

		nameCol = new TableColumn<ResourceValue, String>(uiResources.getString("label.resource"));
		valueCol = new TableColumn<ResourceValue, Number>(uiResources.getString("label.value"));
		notesCol = new TableColumn<ResourceValue, String>(uiResources.getString("label.notes"));

		nameCol.setMinWidth(200);
		nameCol.setMaxWidth(300);
		valueCol.setMinWidth(80);
		valueCol.setMaxWidth(160);
		notesCol.setMinWidth(100);
		notesCol.setMaxWidth(Double.MAX_VALUE);
		table.getColumns().addAll(nameCol, valueCol, notesCol);

		notesCol.setMaxWidth(Double.MAX_VALUE);


		// Let table scroll
		ScrollPane scroll = new ScrollPane(table);
		scroll.setFitToWidth(true);
		scroll.setFitToHeight(true);

		// Add to layout
		super.getChildren().addAll(addLine, scroll);
		super.setSpacing(5);
		VBox.setVgrow(table, Priority.SOMETIMES);

	}

	//-------------------------------------------------------------------
	private void doValueFactories() {
		nameCol.setCellValueFactory(new Callback<CellDataFeatures<ResourceValue, String>, ObservableValue<String>>() {
			public ObservableValue<String> call(CellDataFeatures<ResourceValue, String> p) {
				ResourceValue item = p.getValue();
				return new SimpleStringProperty(item.getModifyable().getName());
			}
		});
		valueCol.setCellValueFactory(new Callback<CellDataFeatures<ResourceValue, Number>, ObservableValue<Number>>() {
			public ObservableValue<Number> call(CellDataFeatures<ResourceValue, Number> p) {
				return new SimpleIntegerProperty( p.getValue().getPoints() );
			}
		});

		valueCol.setCellFactory(new Callback<TableColumn<ResourceValue,Number>, TableCell<ResourceValue,Number>>() {
			public TableCell<ResourceValue,Number> call(TableColumn<ResourceValue,Number> p) {
				return new ResourceEditingCell(control);
			}
		});
		notesCol.setCellValueFactory(
	            new PropertyValueFactory<ResourceValue, String>("description"));
		notesCol.setCellFactory(TextFieldTableCell.<ResourceValue>forTableColumn());
		notesCol.setEditable(true);
		notesCol.setOnEditCommit(
			    new EventHandler<CellEditEvent<ResourceValue, String>>() {
			        @Override
			        public void handle(CellEditEvent<ResourceValue, String> t) {
			        	logger.warn("editCommit");
			            ((ResourceValue) t.getTableView().getItems().get(
			                t.getTablePosition().getRow())
			                ).setComment(t.getNewValue());
			            GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.RESOURCE_CHANGED, null));
			        }
			    }
			);
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		add.setOnAction(this);
		add.setDisable(true);

		addChoice.getSelectionModel().selectedItemProperty().addListener(new ChangeListener<Resource>() {
			public void changed(ObservableValue<? extends Resource> item,
					Resource arg1, Resource newVal) {
				add.setDisable(newVal==null);
			}
		});
	}

	//-------------------------------------------------------------------
	public TableView<ResourceValue> getTable() {
		return table;
	}

	//-------------------------------------------------------------------
	private void updateContent() {
		if (model!=null) {
			table.getItems().clear();
			table.getItems().addAll(model.getResources());
			addChoice.getItems().clear();
			addChoice.getItems().addAll(control.getAvailableResources());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.splimo.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.splimo.chargen.event.GenerationEvent)
	 */
	@SuppressWarnings("incomplete-switch")
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case RESOURCE_ADDED:
		case RESOURCE_REMOVED:
		case RESOURCE_CHANGED:
			logger.debug("Resources changed - refresh view");
			updateContent();
			addChoice.requestFocus();
			add.setDisable(addChoice.getSelectionModel().getSelectedIndex()==-1);
			break;
		case EXPERIENCE_CHANGED:
			updateContent();
			break;
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.event.EventHandler#handle(javafx.event.Event)
	 */
	@Override
	public void handle(ActionEvent event) {
		if (event.getSource()==add) {
			Resource res = addChoice.getSelectionModel().getSelectedItem();
			ResourceValue ref = control.select(res);
			logger.debug("  resource added: "+ref);
			table.getItems().add(ref);
		}
	}

	//-------------------------------------------------------------------
	public ObservableList<ResourceValue> getResources() {
		return table.getItems();
	}

}

class ResourceEditingCell extends TableCell<ResourceValue, Number> implements ChangeListener<Integer>{

	private Spinner<Integer> box;
	private ResourceController charGen;
	private ResourceValue resource;

	//-------------------------------------------------------------------
	public ResourceEditingCell(ResourceController charGen) {
		this.charGen = charGen;
		box = new Spinner<>(-2, 6, 1);
		box.valueProperty().addListener(this);
//		box.setCyclic(false);
		//		ListSpinnerSkin<Integer> skin = new ListSpinnerSkin<Integer>(box);
		//		skin.setArrowPosition(ArrowPosition.SPLIT);
		//		box.setSkin(skin);
		////		((ListSpinnerSkin<Race>)box.getSkin()).setArrowPosition(ArrowPosition.SPLIT);
		setAlignment(Pos.CENTER);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	protected void updateItem(Number item, boolean empty) {
		super.updateItem(item, empty);

		if (item==null || empty || getTableRow()==null) {
			this.setGraphic(null);
			return;
		}

		resource = (ResourceValue) getTableRow().getItem();
		if (resource==null)
			return;
		//		parent.setMapping(resource, this);
		box.getValueFactory().setValue(resource.getPoints());

		this.setGraphic(box);
	}

	//-------------------------------------------------------------------
	/**
	 * @see javafx.beans.value.ChangeListener#changed(javafx.beans.value.ObservableValue, java.lang.Object, java.lang.Object)
	 */
	@Override
	public void changed(ObservableValue<? extends Integer> item, Integer old, Integer val) {
		if (val>resource.getPoints() && charGen.canBeIncreased(resource)) {
			charGen.increase(resource);
		} else if (val<resource.getPoints() && charGen.canBeDecreased(resource)) {
			charGen.decrease(resource);
		} else {
			// Revert back
			box.getValueFactory().setValue(resource.getPoints());
		}
	}

}