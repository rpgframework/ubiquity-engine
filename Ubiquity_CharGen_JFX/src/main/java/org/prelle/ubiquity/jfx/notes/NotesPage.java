/**
 * 
 */
package org.prelle.ubiquity.jfx.notes;

import org.prelle.rpgframework.jfx.FreePointsNode;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.jfx.pages.UbiquityExpLine;
import org.prelle.ubiquity.jfx.pages.UbiquityManagedScreenPage;

import de.rpgframework.character.CharacterHandle;
import javafx.geometry.Insets;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TextArea;
import javafx.scene.layout.HBox;
import javafx.scene.layout.Priority;
import javafx.scene.layout.VBox;

/**
 * @author Stefan
 *
 */
public class NotesPage extends UbiquityManagedScreenPage {

	private TextArea tfNotes;

	private UbiquityExpLine firstLine;
	
	//--------------------------------------------------------------------
	public NotesPage(CharacterController control, CharacterHandle handle) {
		super(control, handle);
		
		initComponents();
		initCommandBar();
		initLayout();
		initInteractivity();
		
		setData(control.getModel());
 	}
	//-------------------------------------------------------------------
	private void initCommandBar() {
		/*
		 * Command bar
		 */
		firstLine = new UbiquityExpLine();
		getCommandBar().setContent(firstLine);
		cmdPrint = new MenuItem(UI.getString("command.primary.print"), new Label("\uE749"));
		cmdDelete = new MenuItem(UI.getString("command.primary.delete"), new Label("\uE74D"));
		if (handle!=null)
			getCommandBar().getPrimaryCommands().addAll(cmdPrint);
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		setTitle(UI.getString("screen.notes.title"));
		
		tfNotes = new TextArea();
	}

	//-------------------------------------------------------------------
	private void initLayout() {
		getStyleClass().add("text-body");
		
		tfNotes.setStyle("-fx-pref-height: 27em; -fx-max-width: 50em");
		tfNotes.setWrapText(true);

		Label lblExplain = new Label(UI.getString("notes.explanation"));
		lblExplain.setStyle("-fx-text-fill: white; -fx-font-size: 120%");
		lblExplain.setWrapText(true);
		VBox bxNotesExplain = new VBox(20, lblExplain, tfNotes);
		

		VBox content = new VBox();
		content.setSpacing(20);
		content.getChildren().addAll(firstLine, bxNotesExplain);
		VBox.setVgrow(bxNotesExplain, Priority.ALWAYS);
		VBox.setMargin(bxNotesExplain  , new Insets(0,0,20,0));

		setContent(content);
}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		tfNotes.textProperty().addListener( (ov,o,n) -> charGen.getModel().setNotes(n));
	}

	//-------------------------------------------------------------------
	public void setData(UbiquityCharacter model) {
		logger.debug("refresh");
		setTitle(UI.getString("screen.notes.title")+" / "+model.getName());
		tfNotes.setText(charGen.getModel().getNotes());
		
		setPointsFree(charGen.getModel().getExperienceFree());
		firstLine.setData(charGen.getModel());

	}

}
