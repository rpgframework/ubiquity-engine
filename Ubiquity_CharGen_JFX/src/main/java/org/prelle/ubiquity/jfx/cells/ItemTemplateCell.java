package org.prelle.ubiquity.jfx.cells;

import org.prelle.ubiquity.items.ItemTemplate;

import javafx.scene.control.ListCell;

public class ItemTemplateCell extends ListCell<ItemTemplate> {

	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(ItemTemplate item, boolean empty) {
		super.updateItem(item, empty);
		
		if (empty) {
			setText(null);
		} else {
			setText(item.getName());
		}
	}
}