package org.prelle.ubiquity.jfx.cells;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.items.CarriedItem;
import org.prelle.ubiquity.items.Damage;
import org.prelle.ubiquity.items.ItemAttribute;
import org.prelle.ubiquity.items.ItemTypeData;
import org.prelle.ubiquity.items.RangeWeapon;
import org.prelle.ubiquity.items.Weapon;
import org.prelle.ubiquity.jfx.UbiquityJFXConstants;
import org.prelle.ubiquity.requirements.AttributeRequirement;
import org.prelle.ubiquity.requirements.Requirement;
import org.prelle.ubiquity.items.Damage.Type;

import javafx.scene.Node;
import javafx.scene.SnapshotParameters;
import javafx.scene.control.Label;
import javafx.scene.control.ListCell;
import javafx.scene.image.ImageView;
import javafx.scene.image.WritableImage;
import javafx.scene.input.ClipboardContent;
import javafx.scene.input.Dragboard;
import javafx.scene.input.MouseEvent;
import javafx.scene.input.TransferMode;
import javafx.scene.layout.FlowPane;
import javafx.scene.layout.GridPane;

//-------------------------------------------------------------------
//-------------------------------------------------------------------
public class CarriedItemCell extends ListCell<CarriedItem> {

	private final static Logger logger = LogManager.getLogger(UbiquityJFXConstants.BASE_LOGGER_NAME);
	
	private UbiquityCharacter model;
	private GridPane grid;
	private ImageView image;
	private Label name;
	private FlowPane statsFlow;
	private Label statsCommon;
	
	private CarriedItem data;
	
	//-------------------------------------------------------------------
	public CarriedItemCell(UbiquityCharacter model) {
		this.model = model;
		image = new ImageView();
		image.setFitHeight(48);
		image.setFitWidth(48);
		name = new Label();
		name.getStyleClass().add("text-small-subheader");
		statsFlow = new FlowPane();
		statsFlow.setStyle("-fx-hgap: 0.7em; -fx-pref-width: 17em;");
		statsCommon = new Label();
		statsCommon.getStyleClass().add("text-secondary-info");
		
		grid = new GridPane();
		grid.setHgap(2);
		grid.add(image    , 0, 0, 1, 3);
		grid.add(name     , 1, 0);
		grid.add(statsFlow, 1, 1);
		grid.add(statsCommon, 1, 2);
		grid.getStyleClass().add("content");
		grid.setStyle("-fx-pref-width: 18em");

		this.setOnDragDetected(event -> dragStarted(event));
	}
	
	//-------------------------------------------------------------------
	/**
	 * @see javafx.scene.control.Cell#updateItem(java.lang.Object, boolean)
	 */
	@Override
	public void updateItem(CarriedItem item, boolean empty) {
		super.updateItem(item, empty);
		this.data = item;
		
		setGraphicTextGap(0);
		if (empty || item==null) {
			setText(null);
			setGraphic(null);
			return;
		} else {
			setGraphic(grid);
			fillGrid(item);
		}
	}

	//-------------------------------------------------------------------
	private String write(Damage value) {
		if (value.getValue()<0)
			return "*";
		
		if (value.getType()==Type.LETHAL)
			return value.getValue()+UbiquityCore.getI18nResources().getString("damage.lethal.short");
		else
			return value.getValue()+UbiquityCore.getI18nResources().getString("damage.nonlethal.short");
	}
	
	//-------------------------------------------------------------------
	private void fillGrid(CarriedItem item) {
		name.setText(item.getName());
		image.setImage(null);
		
		statsCommon.setText((item.getItem().getWeight()/1000.0)+" kg");
		
		// Weapon
		statsFlow.getChildren().clear();
		for (ItemTypeData tData : item.getItem().getTypeData()) {
			if (tData instanceof Weapon) {
				Weapon weapon = (Weapon)tData;
				// Damage
				try {
					Label lblDmg = new Label(
							ItemAttribute.DAMAGE.getName()+
							": "+write(weapon.getDamage()));
					lblDmg.getStyleClass().add("text-secondary-info");
					statsFlow.getChildren().add(lblDmg);
				} catch (Exception e) {
					logger.error("Error building damage label",e);
				}
				// Speed
				Label lblSpd = new Label(
						ItemAttribute.SPEED.getName()
						+": "+weapon.getSpeed().getShortName());
				lblSpd.getStyleClass().add("text-secondary-info");
				statsFlow.getChildren().add(lblSpd);
				// Requirements
				for (Requirement req : weapon.getRequirements()) {
					if (req instanceof AttributeRequirement) {
						AttributeRequirement aReq = (AttributeRequirement) req;
						// Only print, if not fulfilled
						if (model.getAttribute(aReq.getAttr()).getValue()<aReq.getVal()) {
							Label lblReq = new Label(aReq.getAttr().getShortName()+" "+aReq.getVal());
							lblReq.getStyleClass().add("text-secondary-info");
							lblReq.setStyle("-fx-text-fill: red;");							
							statsFlow.getChildren().add(lblReq);
						}
					}
				}
			} 
			if (tData instanceof RangeWeapon) {
				RangeWeapon weapon = (RangeWeapon)tData;
				Label lblRate = new Label(
						ItemAttribute.RATE.getName()
						+": "+weapon.getRate().getShortName());
				lblRate.getStyleClass().add("text-secondary-info");
				statsFlow.getChildren().add(lblRate);
			}
		}
		
	}

	//-------------------------------------------------------------------
	private void dragStarted(MouseEvent event) {
		logger.debug("drag started for "+data);
		if (data==null)
			return;
//		logger.debug("canBeDeselected = "+charGen.canBeDeselected(data));
//		logger.debug("canBeTrashed    = "+((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true));
//		if (!charGen.canBeDeselected(data) && !((charGen instanceof ResourceLeveller)?((ResourceLeveller)charGen).canBeTrashed(data):true))
//			return;
		
		Node source = (Node) event.getSource();

		/* drag was detected, start a drag-and-drop gesture*/
        /* allow any transfer mode */
        Dragboard db = source.startDragAndDrop(TransferMode.ANY);
        
        /* Put a string on a dragboard */
        ClipboardContent content = new ClipboardContent();
        String id = "item|"+data.getItem().getId()+"|"+data.getCount();
		logger.debug("drag "+id);
        content.putString(id);
        db.setContent(content);
        
        /* Drag image */
        WritableImage snapshot = source.snapshot(new SnapshotParameters(), null);
        db.setDragView(snapshot);
        
        event.consume();	
    }

}