package org.prelle.ubiquity.jfx.pages;

import org.prelle.javafx.ScreenManagerProvider;
import org.prelle.rpgframework.jfx.DoubleSection;
import org.prelle.rpgframework.jfx.Section;
import org.prelle.ubiquity.BasePluginData;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.items.ItemLocationType;
import org.prelle.ubiquity.jfx.sections.EquipmentSection;

import de.rpgframework.character.CharacterHandle;
import de.rpgframework.core.BabylonEventBus;
import de.rpgframework.core.BabylonEventType;

/**
 * @author Stefan Prelle
 *
 */
public class UbiquityEquipmentPage extends UbiquityManagedScreenPage {

	private ScreenManagerProvider provider;

	private EquipmentSection secBody;
	private EquipmentSection secOther;

	//-------------------------------------------------------------------
	public UbiquityEquipmentPage(CharacterController control, CharacterHandle handle, ScreenManagerProvider provider) {
		super(control, handle);
		this.setId("ubiquity-equipment");
		this.setTitle(control.getModel().getName());
		this.provider = provider;
		this.handle   = handle;
		
		initComponents();
		initInteractivity();

		refresh();
	}

	//-------------------------------------------------------------------
	private void initLine1() {
		secBody  = new EquipmentSection(UI.getString("section.items.body"), charGen, provider, ItemLocationType.BODY);
		secOther = new EquipmentSection(UI.getString("section.items.other"), charGen, provider, ItemLocationType.SOMEWHEREELSE);

		Section line1 = new DoubleSection(secBody, secOther);
		getSectionList().add(line1);

		// Interactivity
		secBody.showHelpForProperty().addListener( (ov,o,n) -> updateHelp( (n!=null)?n.getItem():null ));
		secOther.showHelpForProperty().addListener( (ov,o,n) -> updateHelp( (n!=null)?n.getItem():null ));
	}

	//-------------------------------------------------------------------
	protected void initComponents() {
		setPointsNameProperty(UI.getString("label.experience.short"));

		initLine1();
	}

	//-------------------------------------------------------------------
	private void initInteractivity() {
		cmdPrint.setOnAction( ev -> {BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, charGen.getModel());});
		cmdDelete.setOnAction( ev -> BabylonEventBus.fireEvent(BabylonEventType.PRINT_REQUESTED, handle, charGen.getModel()));
	}

	//-------------------------------------------------------------------
	private void updateHelp(BasePluginData data) {
		if (data!=null) {
			this.setDescriptionHeading(data.getName());
			this.setDescriptionPageRef(data.getProductName()+" "+data.getPage());
			this.setDescriptionText(data.getHelpText());
		} else {
			this.setDescriptionHeading(null);
			this.setDescriptionPageRef(null);
			this.setDescriptionText(null);
		}
	}

}
