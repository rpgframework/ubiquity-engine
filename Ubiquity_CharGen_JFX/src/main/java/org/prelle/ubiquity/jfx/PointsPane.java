/**
 * 
 */
package org.prelle.ubiquity.jfx;

import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import javafx.geometry.HPos;
import javafx.geometry.Insets;
import javafx.geometry.Pos;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;

import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.charctrl.CharGenMode;
import org.prelle.ubiquity.charctrl.CharacterController;

/**
 * @author prelle
 *
 */
public class PointsPane extends VBox {

	private static PropertyResourceBundle UI = (PropertyResourceBundle) ResourceBundle.getBundle("i18n/ubiquity/chargenui");

	private Label lblGPLeft;
	private Label lblExpLeft;
	private Label lblExpInvested;
	
	private UbiquityCharacter model;
	private CharacterController ctrl;
	
	//-------------------------------------------------------------------
	/**
	 */
	public PointsPane(CharacterController ctrl) {
		this.ctrl = ctrl;
		initComponents();
		initLayout();
	}

	//-------------------------------------------------------------------
	private void initComponents() {
		lblGPLeft      = new Label("?");
		lblExpLeft     = new Label("?");
		lblExpInvested = new Label("?");
		
		lblGPLeft     .getStyleClass().add("text-header");
		lblExpLeft    .getStyleClass().add("text-header");
		lblExpInvested.getStyleClass().add("text-subheader");
	}
	
	//-------------------------------------------------------------------
	private void initLayout() {
		setAlignment(Pos.TOP_CENTER);
		getStyleClass().add("section-bar");
//		setPrefWidth(300);
		setMinWidth(200);
		setMinHeight(300);

		Label heaGPLeft     = new Label(UI.getString("label.gp.free"));
		Label heaExPLeft    = new Label(UI.getString("label.ep.free"));
		Label heaEPInvested = new Label(UI.getString("label.ep.used"));
		
		GridPane grid = new GridPane();
		grid.setMaxWidth(Double.MAX_VALUE);
		grid.setHgap(10);
		grid.setVgap(10);
		grid.add(lblGPLeft     , 0, 0, 2,1);
		grid.add(heaGPLeft     , 0, 1, 2,1);
		grid.add(lblExpLeft    , 0, 2, 2,1);
		grid.add(heaExPLeft    , 0, 3, 2,1);
		grid.add(heaEPInvested , 0, 4);
		grid.add(lblExpInvested, 1, 4);
		GridPane.setFillWidth(heaGPLeft, true);
		GridPane.setFillWidth(lblGPLeft, true);
		GridPane.setFillWidth(heaExPLeft, true);
		GridPane.setFillWidth(lblExpLeft, true);
		GridPane.setHalignment(lblExpLeft, HPos.CENTER);
		GridPane.setHalignment(heaExPLeft, HPos.CENTER);
		GridPane.setHalignment(lblGPLeft, HPos.CENTER);
		GridPane.setHalignment(heaGPLeft, HPos.CENTER);
		GridPane.setMargin(heaExPLeft, new Insets(-10,0,0,0));
		
		getChildren().add(grid);

		
		if (ctrl.getMode()!=CharGenMode.CREATING) {
			lblGPLeft.setVisible(false);
			heaGPLeft.setVisible(false);
			grid.getChildren().remove(lblGPLeft);
			grid.getChildren().remove(heaGPLeft);
		}
			
	}

	//-------------------------------------------------------------------
	public void setData(UbiquityCharacter model) {
		this.model = model;
		refresh();
	}

	//-------------------------------------------------------------------
	public void refresh() {
//		if (generator!=null) {
//			lblGPLeft.setText(String.valueOf(generator.getPointsLeft()));
//		}
		lblExpLeft.setText(String.valueOf(model.getExperienceFree()));
		lblExpInvested.setText(String.valueOf(model.getExperienceInvested()));
	}

}
