/**
 * 
 */
package org.prelle.ubiquity;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.ElementList;
import org.prelle.ubiquity.persist.MotivationElementConverter;

/**
 * @author prelle
 *
 */
public class Archetype extends BasePluginData implements Comparable<Archetype> {
	

	@org.prelle.simplepersist.Attribute
	private String    id;
	@ElementList(type=Motivation.class,entry="motivref",convert=MotivationElementConverter.class)
	private List<Motivation> motivations;

	//-------------------------------------------------------------------
	/**
	 */
	public Archetype() {
		motivations = new ArrayList<Motivation>();
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "archetype."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "archetype."+id+".desc";
	}

	//-------------------------------------------------------------------
	public String toString() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Archetype o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		return i18n.getString("archetype."+id);
	}

	//-------------------------------------------------------------------
	/**
	 * Return the typical motivations
	 * @return the motivations
	 */
	public List<Motivation> getMotivations() {
		return motivations;
	}

}
