/**
 * 
 */
package org.prelle.ubiquity;

import org.prelle.simplepersist.Attribute;

/**
 * @author Stefan
 *
 */
public class GenerationTemplate extends BasePluginData {

	@Attribute
	private String id;
	private transient boolean defaultChoice;
	@Attribute(name="attr")
	private int attributePoints;
	@Attribute(name="attrmax")
	private int attributeMax;
	@Attribute(name="talents")
	private double talentAndResourcePoints;
	@Attribute(name="skill")
	private int skillPoints;
	@Attribute(name="skillmax")
	private int skillPointMax;
	@Attribute(name="exp")
	private int startExp;
	
	//--------------------------------------------------------------------
	public GenerationTemplate() {
	}

	//--------------------------------------------------------------------
	public String getName() {
		return i18n.getString("template."+id);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "template."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "template."+id+".desc";
	}

	//--------------------------------------------------------------------
	/**
	 * @return the defaultChoice
	 */
	public boolean isDefaultChoice() {
		return defaultChoice;
	}

	//--------------------------------------------------------------------
	/**
	 * @param defaultChoice the defaultChoice to set
	 */
	public void setDefaultChoice(boolean defaultChoice) {
		this.defaultChoice = defaultChoice;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the attributePoints
	 */
	public int getAttributePoints() {
		return attributePoints;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the attributeMax
	 */
	public int getAttributeMax() {
		return attributeMax;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the talentAndResourcePoints
	 */
	public double getTalentAndResourcePoints() {
		return talentAndResourcePoints;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the skillPoints
	 */
	public int getSkillPoints() {
		return skillPoints;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the skillPointMax
	 */
	public int getSkillPointMax() {
		return skillPointMax;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the startExp
	 */
	public int getStartExp() {
		return startExp;
	}

}
