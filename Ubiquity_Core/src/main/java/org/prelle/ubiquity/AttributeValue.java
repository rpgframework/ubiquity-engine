/**
 *
 */
package org.prelle.ubiquity;

import java.util.ArrayList;
import java.util.List;

import org.prelle.simplepersist.Root;
import org.prelle.ubiquity.modifications.AttributeModification;

import de.rpgframework.genericrpg.NumericalValue;

/**
 * @author prelle
 *
 */
@Root(name = "attr")
public class AttributeValue implements Comparable<AttributeValue>, NumericalValue<Attribute> {

	@org.prelle.simplepersist.Attribute(name="id")
	private Attribute id;
	@org.prelle.simplepersist.Attribute(name="value")
	private int points;

	// Modifications that influence this attribute
	private transient List<AttributeModification> modifications;

	//-------------------------------------------------------------------
	public AttributeValue() {
		this.points = 0;
		modifications = new ArrayList<AttributeModification>();
	}

	//-------------------------------------------------------------------
	public AttributeValue(Attribute attr, int val) {
		this();
		this.id = attr;
		this.points = val;
	}

	//-------------------------------------------------------------------
	public AttributeValue(AttributeValue copy) {
		this.id = copy.getAttribute();
		this.points = copy.getPoints();
		this.modifications = new ArrayList<AttributeModification>(copy.getModifications());
	}

	//-------------------------------------------------------------------
	public String toString() {
		return String.format("%s(%d+=%d=%d)", id, points, getModifier(), getValue());
	}

	//-------------------------------------------------------------------
	/**
	 * The invested points
	 */
	public int getPoints() {
		return points;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setPoints(int value) {
		this.points = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public Attribute getAttribute() {
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(AttributeValue other) {
		return id.compareTo(other.getAttribute());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the modifier
	 */
	public int getModifier() {
		int modifier = 0;
		for (AttributeModification mod : modifications)
			modifier += mod.getValue();
		return modifier;
	}

	//-------------------------------------------------------------------
	/**
	 * Value including all modifiers
	 */
	public int getValue() {
		return points + getModifier();
	}

	//--------------------------------------------------------------------
	/**
	 * @return the modifications
	 */
	public List<AttributeModification> getModifications() {
		return modifications;
	}

	//--------------------------------------------------------------------
	public void addModification(AttributeModification mod) {
		if (!modifications.contains(mod))
			modifications.add(mod);
	}

	//--------------------------------------------------------------------
	public void removeModification(AttributeModification mod) {
		modifications.remove(mod);
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValue#getModifyable()
	 */
	@Override
	public Attribute getModifyable() {
		return id;
	}
}
