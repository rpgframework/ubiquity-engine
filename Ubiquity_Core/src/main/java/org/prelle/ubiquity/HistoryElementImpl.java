/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Date;
import java.util.List;

import org.prelle.ubiquity.modifications.ModificationImpl;

import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.Reward;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.products.Adventure;

/**
 * @author prelle
 *
 */
public class HistoryElementImpl implements HistoryElement {
	
	/** Optional */
	private Adventure adventure;
	private String name;
	private List<Reward> gained;
	private List<Modification> spent;

	//-------------------------------------------------------------------
	public HistoryElementImpl() {
		gained = new ArrayList<Reward>();
		spent   = new ArrayList<Modification>();
	}

	//-------------------------------------------------------------------
	public void addGained(RewardImpl mod) {
		gained.add(mod);
		Collections.sort(gained, new Comparator<Reward>() {
			public int compare(Reward o1, Reward o2) {
				if (o1.getDate()!=null && o2.getDate()!=null)
					return o1.getDate().compareTo(o2.getDate());
				return 0;
			}
		});
	}

	//-------------------------------------------------------------------
	public void addSpent(ModificationImpl mod) {
		spent.add(mod);
		Collections.sort(spent, new Comparator<Modification>() {
			public int compare(Modification o1, Modification o2) {
				if (o1.getDate()!=null && o2.getDate()!=null)
					return o1.getDate().compareTo(o2.getDate());
				return 0;
			}
		});
	}

	//-------------------------------------------------------------------
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.HistoryElement#getStart()
	 */
	@Override
	public Date getStart() {
		if (gained.isEmpty())
			return spent.get(0).getDate();
		return gained.get(0).getDate();
	}

	//-------------------------------------------------------------------
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.HistoryElement#getEnd()
	 */
	@Override
	public Date getEnd() {
		if (gained.isEmpty())
			return spent.get(spent.size()-1).getDate();
		return gained.get(gained.size()-1).getDate();
	}

	//-------------------------------------------------------------------
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.HistoryElement#getAdventure()
	 */
	@Override
	public Adventure getAdventure() {
		return adventure;
	}

	//-------------------------------------------------------------------
	/**
	 * @param adventure the adventure to set
	 */
	public void setAdventure(Adventure adventure) {
		this.adventure = adventure;
	}

	//-------------------------------------------------------------------
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.HistoryElement#getName()
	 */
	@Override
	public String getName() {
		return name;
	}

	//-------------------------------------------------------------------
	/**
	 * @param name the name to set
	 */
	public void setName(String name) {
		this.name = name;
	}

	//-------------------------------------------------------------------
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.HistoryElement#getGained()
	 */
	@Override
	public List<Reward> getGained() {
		return gained;
	}

	//-------------------------------------------------------------------
	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.HistoryElement#getSpent()
	 */
	@Override
	public List<Modification> getSpent() {
		return spent;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.HistoryElement#getTotalExperience()
	 */
	@Override
	public int getTotalExperience() {
		int sum = 0;
		for (Reward reward : gained) sum+=reward.getExperiencePoints();
		return sum;
	}

}
