/**
 * 
 */
package org.prelle.ubiquity;

import java.text.Collator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.rpgframework.PluginData;
import de.rpgframework.genericrpg.SelectableItem;

/**
 * @author prelle
 *
 */
public class Flaw extends BasePluginData implements PluginData, Comparable<Flaw>, SelectableItem {
	
	private static Logger logger = LogManager.getLogger("ubiquity");
	
	public enum Type {
		PHYSICAL,
		MENTAL,
		SOCIAL,
		MISC
	}

	@org.prelle.simplepersist.Attribute
	private String    id;
	@org.prelle.simplepersist.Attribute
	private Type      type;

	//-------------------------------------------------------------------
	/**
	 */
	public Flaw() {
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		return getName()+" ("+type+")";
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		if (i18n==null)
			return "flaw."+id;
		try {
			return i18n.getString("flaw."+id);
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Flaw o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "flaw."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "flaw."+id+".desc";
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectableItem#getTypeId()
	 */
	@Override
	public String getTypeId() {
		return "flaw";
	}

}
