/**
 * 
 */
package org.prelle.ubiquity;

import java.text.Collator;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import de.rpgframework.genericrpg.SelectableItem;

/**
 * @author prelle
 *
 */
public class Resource extends BasePluginData implements Comparable<Resource>, SelectableItem {
	
	private static Logger logger = LogManager.getLogger("ubiquity");
	
	public enum Type {
		BASE,
		NORMAL,
	}

	@org.prelle.simplepersist.Attribute
	private String    id;
	@org.prelle.simplepersist.Attribute(required=false)
	private Type   type;

	//-------------------------------------------------------------------
	/**
	 */
	public Resource() {
		type = Type.NORMAL;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "resource."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "resource."+id+".desc";
	}

	//-------------------------------------------------------------------
	@Override
	public String toString() {
		if (i18n==null)
			return id;
		return getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @return the name
	 */
	public String getName() {
		if (i18n==null)
			return "resource."+id;
		try {
			return i18n.getString("resource."+id);
		} catch (Exception e) {
			logger.error(e.toString());
		}
		return id;
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Resource o) {
		return Collator.getInstance().compare(getName(), o.getName());
	}

	//-------------------------------------------------------------------
	/**
	 * @return the id
	 */
	public String getId() {
		return id;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public Type getType() {
		return type;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectableItem#getTypeId()
	 */
	@Override
	public String getTypeId() {
		return "resource";
	}

}
