/**
 * 
 */
package org.prelle.ubiquity;

import org.prelle.ubiquity.persist.SkillSpecializationConverter;

/**
 * @author prelle
 *
 */
public class SkillSpecializationValue {

	
	@org.prelle.simplepersist.Attribute(name="ref")
	@org.prelle.simplepersist.AttribConvert(SkillSpecializationConverter.class)
	private SkillSpecialization ref;
	@org.prelle.simplepersist.Attribute(name="value")
	private int value;

	//-------------------------------------------------------------------
	public SkillSpecializationValue() {
		value = 1;
	}

	//-------------------------------------------------------------------
	public SkillSpecializationValue(SkillSpecialization spec, int val) {
		this.ref = spec;
		value = val;
	}

	//-------------------------------------------------------------------
	public int getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	public void setValue(int value) {
		this.value = value;
	}

	//-------------------------------------------------------------------
	public SkillSpecialization getSpecialization() {
		return ref;
	}

}
