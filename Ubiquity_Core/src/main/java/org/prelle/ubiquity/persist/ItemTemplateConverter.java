/**
 * 
 */
package org.prelle.ubiquity.persist;

import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.simplepersist.SerializationException;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.items.ItemTemplate;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@ConstructorParams({UbiquityCore.KEY_RULES})
public class ItemTemplateConverter implements StringValueConverter<ItemTemplate> {

	private UbiquityRuleset ruleset;
	
	//-------------------------------------------------------------------
	public ItemTemplateConverter(RoleplayingSystem rules) {
		ruleset = UbiquityCore.getRuleset(rules);
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in UbiquityCore");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(ItemTemplate value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public ItemTemplate read(String idref) throws Exception {
		ItemTemplate item = ruleset.getItemTemplate(idref);
		if (item==null)
			throw new SerializationException("Unknown item ID '"+idref);

		return item;
	}

}
