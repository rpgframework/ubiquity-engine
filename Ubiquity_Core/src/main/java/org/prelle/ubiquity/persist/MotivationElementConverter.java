/**
 * 
 */
package org.prelle.ubiquity.persist;

import javax.xml.namespace.QName;
import javax.xml.stream.XMLEventReader;
import javax.xml.stream.events.Attribute;
import javax.xml.stream.events.StartElement;

import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.XMLElementConverter;
import org.prelle.simplepersist.marshaller.XmlNode;
import org.prelle.simplepersist.unmarshal.XMLTreeItem;
import org.prelle.ubiquity.Motivation;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.UbiquityRuleset;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@ConstructorParams({UbiquityCore.KEY_RULES})
public class MotivationElementConverter implements XMLElementConverter<Motivation> {

	private UbiquityRuleset ruleset;
	
	//-------------------------------------------------------------------
	public MotivationElementConverter(RoleplayingSystem rules) {
		ruleset = UbiquityCore.getRuleset(rules);
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in UbiquityCore");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.marshaller.XmlNode, java.lang.Object)
	 */
	@Override
	public void write(XmlNode node, Motivation value) throws Exception {
		if (value!=null)
			node.setAttribute("motive", value.getId());
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Motivation read(XMLTreeItem node, StartElement ev, XMLEventReader evRd) throws Exception {
		Attribute attr = ev.getAttributeByName(new QName("motive"));
		if (attr==null)
			throw new SerializationException("Missing attribute 'motive' in "+ev);
		String idref = attr.getValue();
		
		Motivation skill = ruleset.getMotivation(idref);
		if (skill==null)
			throw new SerializationException("Unknown motivation ID '"+idref+"' in "+ev);

		return skill;
	}

}
