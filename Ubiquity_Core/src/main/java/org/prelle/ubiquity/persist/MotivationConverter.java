/**
 * 
 */
package org.prelle.ubiquity.persist;

import org.prelle.simplepersist.ConstructorParams;
import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.ubiquity.Motivation;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.UbiquityRuleset;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
@ConstructorParams({UbiquityCore.KEY_RULES})
public class MotivationConverter implements StringValueConverter<Motivation> {

	private UbiquityRuleset ruleset;
	
	//-------------------------------------------------------------------
	public MotivationConverter(RoleplayingSystem rules) {
		ruleset = UbiquityCore.getRuleset(rules);
		if (ruleset==null)
			throw new NullPointerException("Missing "+rules+" ruleset in UbiquityCore");
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Converter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Motivation value) throws Exception {
		return value.getId();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.Converter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Motivation read(String idref) throws Exception {
		Motivation skill = ruleset.getMotivation(idref);
		if (skill==null)
			throw new SerializationException("Unknown motivation ID '"+idref);

		return skill;
	}

}
