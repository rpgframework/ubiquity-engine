/**
 * 
 */
package org.prelle.ubiquity.persist;

import org.prelle.simplepersist.SerializationException;
import org.prelle.simplepersist.StringValueConverter;
import org.prelle.ubiquity.items.Damage;
import org.prelle.ubiquity.items.Damage.Type;

/**
 * @author prelle
 *
 */
public class WeaponDamageConverter implements StringValueConverter<Damage> {

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#write(org.prelle.simplepersist.XmlNode, java.lang.Object)
	 */
	@Override
	public String write(Damage value) throws Exception {
		if (value.getValue()<0)
			return "*";
		
		if (value.getType()==Type.LETHAL)
			return value.getValue()+"L";
		else
			return value.getValue()+"N";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.simplepersist.StringValueConverter#read(org.prelle.simplepersist.Persister.ParseNode, javax.xml.stream.events.StartElement)
	 */
	@Override
	public Damage read(String toParse) throws Exception {
		toParse = toParse.trim();
		if (toParse.endsWith("*"))
			toParse = toParse.substring(0, toParse.length()-1);
		String num_s = toParse.substring(0, toParse.length()-1); 
		char type = toParse.toUpperCase().charAt(toParse.length()-1);
		int val = Integer.parseInt(num_s);
		if (val<0)
			val = -1; //Integer.MAX_VALUE;
		if (type=='L' || type=='D' || type=='T')
			return new Damage(val, Type.LETHAL);
		if (type=='N')
			return new Damage(val, Type.NON_LETHAL);

		throw new SerializationException("Don't know how to parse damage: "+toParse);
	}

}
