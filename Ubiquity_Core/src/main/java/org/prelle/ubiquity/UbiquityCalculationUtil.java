/**
 * 
 */
package org.prelle.ubiquity;

import java.util.List;

import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author Stefan
 *
 */
public interface UbiquityCalculationUtil {

	//--------------------------------------------------------------------
	public void applyAfterLoadModifications(UbiquityCharacter model);
	
	//--------------------------------------------------------------------
	/**
	 * Calculate changes in secondary attributes.
	 * 
	 * @return List of changes attributes values
	 */
	public List<AttributeValue> calculateSecondaryAttributes(UbiquityCharacter model);

	//--------------------------------------------------------------------
	/**
	 * Convert characters rewards and modifications into displayable
	 * history elements
	 */
	public List<HistoryElement> convertToHistoryElementList(UbiquityCharacter charac, RoleplayingSystem rules);

	//--------------------------------------------------------------------
	public void applyToCharacter(UbiquityCharacter model, List<Modification> modifications);

	//--------------------------------------------------------------------
	public void unapplyToCharacter(UbiquityCharacter model, List<Modification> modifications);

}
