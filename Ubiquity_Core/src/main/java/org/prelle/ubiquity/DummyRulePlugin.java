/**
 * 
 */
package org.prelle.ubiquity;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import de.rpgframework.ConfigContainer;
import de.rpgframework.ConfigOption;
import de.rpgframework.RulePlugin;
import de.rpgframework.RulePluginFeatures;
import de.rpgframework.character.RuleSpecificCharacterObject;
import de.rpgframework.core.CommandResult;
import de.rpgframework.core.CommandType;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author Stefan
 *
 */
public class DummyRulePlugin<C extends RuleSpecificCharacterObject> implements RulePlugin<C> {

	private RoleplayingSystem rules;
	private String id;
	
	//--------------------------------------------------------------------
	/**
	 */
	public DummyRulePlugin(RoleplayingSystem rules, String id) {
		this.rules = rules;
		this.id    = id;
	}

	@Override
	public boolean willProcessCommand(Object src, CommandType type,
			Object... values) {
		return false;
	}

	@Override
	public CommandResult handleCommand(Object src, CommandType type,
			Object... values) {
		return new CommandResult(type, false, null, false);
	}

	@Override
	public String getID() {
		return id;
	}

	@Override
	public String getReadableName() {
		return id;
	}

	@Override
	public RoleplayingSystem getRules() {
		return rules;
	}

	@Override
	public Collection<RulePluginFeatures> getSupportedFeatures() {
		return new ArrayList<RulePluginFeatures>();
	}

	@Override
	public void attachConfigurationTree(ConfigContainer addBelow) {
	}

	@Override
	public List<ConfigOption<?>> getConfiguration() {
		return new ArrayList<>();
	}

	@Override
	public void init() {
		// TODO Auto-generated method stub
		
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.RulePlugin#getAboutHTML()
	 */
	@Override
	public InputStream getAboutHTML() {
		return null;
	}

	@Override
	public Collection<String> getRequiredPlugins() {
		return new ArrayList<>();
	}

	//-------------------------------------------------------------------
	@Override
	public List<String> getLanguages() {
		return new ArrayList<>();
	}

}
