/**
 *
 */
package org.prelle.ubiquity;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.PropertyResourceBundle;
import java.util.ResourceBundle;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.items.ItemTemplate;
import org.prelle.ubiquity.items.ItemTemplateList;

import de.rpgframework.RulePlugin;
import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class UbiquityCore {

	private static Logger logger = LogManager.getLogger("ubiquity");

	public final static String KEY_RULES = "rulesUbiquity";

	private static PropertyResourceBundle i18NResources;
	protected static PropertyResourceBundle i18NHelpResources;

	private static Map<RoleplayingSystem, UbiquityRuleset> rulesets;

	//-------------------------------------------------------------------
	static {
		i18NResources = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/ubiquity/i18n/ubiquity_core");
		i18NHelpResources = (PropertyResourceBundle) ResourceBundle.getBundle("org/prelle/ubiquity/i18n/core-help");
		rulesets = new HashMap<RoleplayingSystem, UbiquityRuleset>();
	}

	//-------------------------------------------------------------------
	public static PropertyResourceBundle getI18nResources() {
		return i18NResources;
	}

	//-------------------------------------------------------------------
	public static PropertyResourceBundle getI18nHelpResources() {
		return i18NHelpResources;
	}

	//-------------------------------------------------------------------
	public static UbiquityRuleset getRuleset(RoleplayingSystem rules) {
		return rulesets.get(rules);
	}

	//-------------------------------------------------------------------
	public static UbiquityRuleset createRuleset(RoleplayingSystem rules) {
		if (rulesets.containsKey(rules))
			return rulesets.get(rules);
//			throw new IllegalStateException("Already have a ruleset for "+rules);

		UbiquityRuleset ruleset = new UbiquityRuleset(rules);
		rulesets.put(rules, ruleset);

		return ruleset;
	}

	//-------------------------------------------------------------------
	public static void loadTemplates(UbiquityRuleset ruleset, RulePlugin<? extends UbiquityCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load templates (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		try {
			GenerationTemplateList list = ruleset.getSerializer().read(GenerationTemplateList.class, in);
			logger.info("Successfully loaded "+list.size()+" templates");

			// Set translation
			for (GenerationTemplate tmp : list) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
			}

			if (logger.isDebugEnabled()) {
				for (GenerationTemplate tmp : list)
					logger.debug("* "+tmp.getName());
			}

			ruleset.addTemplates(list);
		} catch (Exception e) {
			logger.fatal("Failed deserializing templates",e);
			System.exit(0);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static void loadSkills(UbiquityRuleset ruleset, RulePlugin<? extends UbiquityCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load skills (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		assert resources!=null;
		try {
			SkillList skills = ruleset.getSerializer().read(SkillList.class, in);
			logger.info("Successfully loaded "+skills.size()+" skills");

			// Set translation
			for (Skill tmp : new ArrayList<>(skills)) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
				if (tmp.isGroup())
					logger.debug("Unfold "+tmp+" specializations to extra skills");
				// Attach skill to specializations
				// All skill groups are also added as skill
				for (SkillSpecialization spec : tmp.getSpecializations()) {
					spec.setSkill(tmp);
					if (tmp.isGroup()) {
						Skill virt = new Skill(tmp.getId()+"."+spec.getId());
						virt.setHelpResourceBundle(helpResources);
						virt.setAttribute(tmp.getAttribute());
						virt.setPlugin(plugin);
						virt.setResourceBundle(resources);
						virt.setParentSkill(tmp);
						skills.add(virt);
						tmp.addChildSkill(virt);
					}
				}

			}

			Collections.sort(skills);

			if (logger.isDebugEnabled()) {
				for (Skill tmp : skills)
					logger.debug("* "+tmp.getName());
			}

			ruleset.addSkills(skills);
		} catch (Exception e) {
			logger.fatal("Failed deserializing skills",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static void loadMotivations(UbiquityRuleset ruleset, RulePlugin<? extends UbiquityCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load motivations (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		try {
			MotivationList motivations = ruleset.getSerializer().read(MotivationList.class, in);
			logger.info("Successfully loaded "+motivations.size()+" motivations");

			// Set translation
			for (Motivation tmp : motivations) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
			}
			Collections.sort(motivations);

			if (logger.isDebugEnabled()) {
				for (Motivation tmp : motivations) {
					logger.debug("* "+tmp.getName());
					tmp.getHelpText();
				}
			}

			ruleset.addMotivations(motivations);
		} catch (Exception e) {
			logger.fatal("Failed deserializing motivations",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static void loadArchetypes(UbiquityRuleset ruleset, RulePlugin<? extends UbiquityCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load archetypes (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		try {
			ArchetypeList archetypes = ruleset.getSerializer().read(ArchetypeList.class, in);
			logger.info("Successfully loaded "+archetypes.size()+" archetypes");

			// Set translation
			for (Archetype tmp : archetypes) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
			}
			Collections.sort(archetypes);

			if (logger.isDebugEnabled()) {
				for (Archetype tmp : archetypes)
					logger.debug("* "+tmp.getName()+" = "+tmp.getMotivations());
			}

			ruleset.addArchetypes(archetypes);
		} catch (Exception e) {
			logger.fatal("Failed deserializing archetypes",e);
			System.exit(0);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static void loadFlaws(UbiquityRuleset ruleset, RulePlugin<? extends UbiquityCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load flaws (Rules="+ruleset.getRoleplayingSystem()+", Plugin="+plugin.getID()+")");
		try {
			FlawList flaws = ruleset.getSerializer().read(FlawList.class, in);
			logger.info("Successfully loaded "+flaws.size()+" flaws");

			// Set translation
			for (Flaw tmp : flaws) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
			}
			Collections.sort(flaws);

			if (logger.isDebugEnabled()) {
				for (Flaw tmp : flaws) {
					logger.debug("* "+tmp);
					tmp.getHelpText();
					tmp.getPage();
				}
			}

			ruleset.addFlaws(flaws);
		} catch (Exception e) {
			logger.fatal("Failed deserializing flaws",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static void loadResources(UbiquityRuleset ruleset, RulePlugin<? extends UbiquityCharacter> plugin, InputStream in, ResourceBundle resrcs, ResourceBundle helpResources) {
		logger.debug("Load resources");
		try {
			ResourceList resources = ruleset.getSerializer().read(ResourceList.class, in);
			logger.info("Successfully loaded "+resources.size()+" resources");

			// Set translation
			for (Resource tmp : resources) {
				tmp.setResourceBundle(resrcs);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
			}
			Collections.sort(resources);

			if (logger.isDebugEnabled()) {
				for (Resource tmp : resources)
					logger.debug("* "+tmp);
			}

			ruleset.addResources(resources);
		} catch (Exception e) {
			logger.fatal("Failed deserializing resources",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static void loadTalents(UbiquityRuleset ruleset, RulePlugin<? extends UbiquityCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load talents");
		try {
			TalentList talents = ruleset.getSerializer().read(TalentList.class, in);
			logger.info("Successfully loaded "+talents.size()+" talents");

			// Set translation
			for (Talent tmp : talents) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
			}
			Collections.sort(talents);

			if (logger.isDebugEnabled()) {
				for (Talent tmp : talents) {
					logger.debug("* "+tmp);
					tmp.getHelpText();
					tmp.getPage();
				}
			}

			ruleset.addTalent(talents);
		} catch (Exception e) {
			logger.fatal("Failed deserializing talents",e);
			return;
		}
	}

	//-------------------------------------------------------------------
	public static void loadItems(UbiquityRuleset ruleset, RulePlugin<? extends UbiquityCharacter> plugin, InputStream in, ResourceBundle resources, ResourceBundle helpResources) {
		logger.debug("Load items");
		try {
			ItemTemplateList items = ruleset.getSerializer().read(ItemTemplateList.class, in);
			logger.info("Successfully loaded "+items.size()+" items");

			// Set translation
			for (ItemTemplate tmp : items) {
				tmp.setResourceBundle(resources);
				tmp.setHelpResourceBundle(helpResources);
				tmp.setPlugin(plugin);
			}
			Collections.sort(items);

			if (logger.isDebugEnabled()) {
				for (ItemTemplate tmp : items)
					logger.debug("* "+tmp);
			}

			ruleset.addItems(items);
		} catch (Exception e) {
			logger.fatal("Failed deserializing items",e);
			return;
		}
	}

}

