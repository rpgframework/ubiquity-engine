package org.prelle.ubiquity.modifications;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.persist.SkillConverter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class SkillModification extends ModificationImpl {

	@Attribute(name="idref")
	@AttribConvert(SkillConverter.class)
    private Skill skill;
	@Attribute(required=false)
    private int val;
    
    //-----------------------------------------------------------------------
    public SkillModification() {
    	val = 1;
    }
    
    //-----------------------------------------------------------------------
    public SkillModification(Skill skill, int val) {
        this.skill = skill;
        this.val  = val;
    }
    
    //-----------------------------------------------------------------------
    public String toString() {
        if (skill==null)
        	return "SKILL_NOT_SET";
        return skill.getName()+" = "+val;
    }
    
    //-----------------------------------------------------------------------
    public int getValue() {
        return val;
    }
    
    //-----------------------------------------------------------------------
    public void setValue(int val) {
        this.val = val;
    }
    
//    //-----------------------------------------------------------------------
//    public Object clone() {
//        return new SkillModification(skill, val);
//    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof SkillModification) {
            SkillModification other = (SkillModification)o;
            if (other.getSkill()!=skill) return false;
            if (other.getValue()!=val) return false;
            return super.equals(o);
        } else
            return false;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof SkillModification) {
            SkillModification other = (SkillModification)o;
            if (other.getSkill()!=skill) return false;
            return true;
        } else
            return false;
    }
    
    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof SkillModification))
            return toString().compareTo(obj.toString());
        SkillModification other = (SkillModification)obj;
       return skill.getName().compareTo(other.getSkill().getName());
     }

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public Skill getSkill() {
		return skill;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(Skill skill) {
		this.skill = skill;
	}
    
}// AttributeModification
