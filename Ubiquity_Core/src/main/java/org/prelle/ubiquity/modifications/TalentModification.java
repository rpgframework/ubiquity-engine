/**
 * 
 */
package org.prelle.ubiquity.modifications;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.persist.TalentConverter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class TalentModification extends ModificationImpl {
	
	@Attribute(required=false)
	private ModificationValueType type;
	@Attribute(name="idref")
	@AttribConvert(value=TalentConverter.class)
	private Talent ref;
	@Attribute(name="val")
	private int value;

	//-------------------------------------------------------------------
	public TalentModification() {
		value = 1;
		type = ModificationValueType.RELATIVE;
	}

	//-------------------------------------------------------------------
	public TalentModification(Talent race, int val) {
		this.ref = race;
		this.value = val;
	}

	//-------------------------------------------------------------------
	public TalentModification(ModificationValueType type, Talent race, int val) {
		this.type = type;
		this.ref = race;
		this.value = val;
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (ref==null)
			return "NO_TALENT";
		return ref.getName()+" "+value;
	}

	//-------------------------------------------------------------------
	public String getTalentName() {
		return ref.getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification other) {
		if (other instanceof TalentModification)
			return ref.compareTo(((TalentModification)other).getTalent());
		return 0;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.ModificationImpl.modifications.Modification#clone()
//	 */
//	@Override
//	public Object clone() {
//		return new TalentModification(ref, value);
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the ref
	 */
	public Talent getTalent() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

}
