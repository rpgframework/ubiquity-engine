package org.prelle.ubiquity.modifications;

import org.prelle.ubiquity.Attribute;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class AttributeModification extends ModificationImpl {

	private transient ModificationValueType type;
	@org.prelle.simplepersist.Attribute
	private Attribute attr;
	@org.prelle.simplepersist.Attribute
	private int val;
	@org.prelle.simplepersist.Attribute(required=false)
    private boolean conditional = false;

    //-----------------------------------------------------------------------
    public AttributeModification() {
        type = ModificationValueType.RELATIVE;
    }

    //-----------------------------------------------------------------------
    public AttributeModification(Attribute attr, int val) {
        type = ModificationValueType.RELATIVE;
        this.attr = attr;
        this.val  = val;
    }

    //-----------------------------------------------------------------------
    public AttributeModification(ModificationValueType type, Attribute attr, int val) {
        this.type = type;
        this.attr = attr;
        this.val  = val;
    }

    //-----------------------------------------------------------------------
    public String toString() {
    	if (attr==null)
    		return "Unknown Attribute = "+val;

    	StringBuffer buf = new StringBuffer();
        if (type==ModificationValueType.RELATIVE)
            buf.append( attr.getName()+((val<0)?(" "+val):(" +"+val)) );
        else
        	buf.append( attr.getName()+" = "+val );

        if (conditional)
        	buf.append(" cond.");
        return buf.toString();
    }

    //-----------------------------------------------------------------------
    public ModificationValueType getType() {
        return type;
    }

    //-----------------------------------------------------------------------
    public void setType(ModificationValueType type) {
        this.type = type;
    }

    //-----------------------------------------------------------------------
    public Attribute getAttribute() {
        return attr;
    }

    //-----------------------------------------------------------------------
    public void setAttribute(Attribute attr) {
        this.attr = attr;
    }

    //-----------------------------------------------------------------------
    public int getValue() {
        return val;
    }

    //-----------------------------------------------------------------------
    public void setValue(int val) {
        this.val = val;
    }

//    //-----------------------------------------------------------------------
//    public Object clone() {
//        return new AttributeModification(type, attr, val);
//    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof AttributeModification) {
            AttributeModification amod = (AttributeModification)o;
            if (amod.getType()     !=type) return false;
            if (amod.getAttribute()!=attr) return false;
            if (amod.getValue()    !=val) return false;
            return true;
        } else
            return false;
    }

    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof AttributeModification) {
            AttributeModification amod = (AttributeModification)o;
            if (amod.getType()     !=type) return false;
            if (amod.getAttribute()!=attr) return false;
            return true;
        } else
            return false;
    }

    //-------------------------------------------------------
    /* (non-Javadoc)
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    public int compareTo(Modification obj) {
        if (!(obj instanceof AttributeModification))
            return toString().compareTo(obj.toString());
        AttributeModification other = (AttributeModification)obj;
        if (attr!=other.getAttribute())
            return (Integer.valueOf(attr.ordinal())).compareTo(Integer.valueOf(other.getAttribute().ordinal()));
        return (Integer.valueOf(type.ordinal()).compareTo(Integer.valueOf(other.getType().ordinal())));
    }

	//--------------------------------------------------------------------
	/**
	 * @return the conditional
	 */
	public boolean isConditional() {
		return conditional;
	}

	//--------------------------------------------------------------------
	/**
	 * @param conditional the conditional to set
	 */
	public void setConditional(boolean conditional) {
		this.conditional = conditional;
	}

}// AttributeModification
