/**
 * 
 */
package org.prelle.ubiquity.modifications;

import org.prelle.simplepersist.Attribute;
import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Element;
import org.prelle.ubiquity.Resource;
import org.prelle.ubiquity.persist.ResourceConverter;

import de.rpgframework.genericrpg.modification.Modification;

/**
 * @author prelle
 *
 */
public class ResourceModification extends ModificationImpl {
	
	@Attribute(required=false)
	private ModificationValueType type;
	@Attribute(name="idref")
	@AttribConvert(value=ResourceConverter.class)
	private Resource ref;
	@Attribute(name="val")
	private int value;
	@Element(name="comment",required=false)
	private String comment;

	//-------------------------------------------------------------------
	public ResourceModification() {
		value = 1;
		type = ModificationValueType.RELATIVE;
	}

	//-------------------------------------------------------------------
	public ResourceModification(Resource race, int val) {
		this.ref = race;
		this.value = val;
	}

	//-------------------------------------------------------------------
	public ResourceModification(ModificationValueType type, Resource race, int val) {
		this.type = type;
		this.ref = race;
		this.value = val;
	}

	//-------------------------------------------------------------------
	public String toString() {
		if (comment!=null)
			return ref+" "+value+" '"+comment+"'";
			
		return ref+" "+value;
	}

	//-------------------------------------------------------------------
	public String getResourceName() {
		return ref.getName();
	}

	//-------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(Modification other) {
		if (other instanceof ResourceModification)
			return ref.compareTo(((ResourceModification)other).getResource());
		return 0;
	}

//	//-------------------------------------------------------------------
//	/**
//	 * @see org.prelle.ModificationImpl.modifications.Modification#clone()
//	 */
//	@Override
//	public Object clone() {
//		return new ResourceModification(ref, value);
//	}

	//-------------------------------------------------------------------
	/**
	 * @return the ref
	 */
	public Resource getResource() {
		return ref;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the value
	 */
	public int getValue() {
		return value;
	}

	//-------------------------------------------------------------------
	/**
	 * @param value the value to set
	 */
	public void setValue(int value) {
		this.value = value;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the comment
	 */
	public String getComment() {
		return comment;
	}

	//-------------------------------------------------------------------
	/**
	 * @param comment the comment to set
	 */
	public void setComment(String comment) {
		this.comment = comment;
	}

}
