/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Collections;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.Persister;
import org.prelle.simplepersist.Serializer;
import org.prelle.ubiquity.items.ItemTemplate;
import org.prelle.ubiquity.items.ItemTemplateList;
import org.prelle.ubiquity.items.ItemType;

import de.rpgframework.core.RoleplayingSystem;

/**
 * @author prelle
 *
 */
public class UbiquityRuleset {
	
	private Logger logger;

	private RoleplayingSystem ruleset;
	
	private Persister serializer;
	private GenerationTemplateList templates;
	private MotivationList motivations;
	private ArchetypeList archetypes;
	private SkillList skills;
	private TalentList talents;
	private ResourceList resources;
	private FlawList flaws;
	private ItemTemplateList items;
	private UbiquityCalculationUtil utility;
	
	//-------------------------------------------------------------------
	public UbiquityRuleset(RoleplayingSystem rules) {
		this.ruleset = rules;
		logger = LogManager.getLogger("ubiquity."+rules.name().toLowerCase());
		
		serializer = new Persister();
		Persister.putContext(UbiquityCore.KEY_RULES, rules);
		
		templates   = new GenerationTemplateList();
		skills      = new SkillList();
		motivations = new MotivationList();
		archetypes  = new ArchetypeList();
		talents     = new TalentList();
		resources   = new ResourceList();
		flaws	    = new FlawList();
		items       = new ItemTemplateList();

		utility = new UbiquityUtils();
	}

	//-------------------------------------------------------------------
	public RoleplayingSystem getRoleplayingSystem() {
		return ruleset;
	}
	
	//-------------------------------------------------------------------
	public Serializer getSerializer() {
		return serializer;
	}
	
	//-------------------------------------------------------------------
	public void addTemplates(Collection<GenerationTemplate> toAdd) {
		logger.debug("Add "+toAdd.size()+" templates to "+ruleset+" ruleset");
		templates.addAll(toAdd);
	}
	
	//-------------------------------------------------------------------
	public List<GenerationTemplate> getTemplates() {
		List<GenerationTemplate> ret = new ArrayList<GenerationTemplate>(templates);
//		Collections.sort(ret);
		return ret;
	}
	
	//-------------------------------------------------------------------
	public GenerationTemplate getDefaultTemplates() {
		for (GenerationTemplate temp : templates) {
			if (temp.isDefaultChoice())
				return temp;
		}
		
		// If nothing is marked as default, choose the first		
		if (!templates.isEmpty())
			return templates.get(0);
		
		return null;
	}
	
	//-------------------------------------------------------------------
	public GenerationTemplate getTemplate(String key) {
		for (GenerationTemplate tmp : templates) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}
	
	//-------------------------------------------------------------------
	public void addSkills(Collection<Skill> toAdd) {
		logger.debug("Add "+toAdd.size()+" skills to "+ruleset+" ruleset");
		skills.addAll(toAdd);
		Collections.sort(skills);
	}
	
	//-------------------------------------------------------------------
	public List<Skill> getSkills() {
		return skills;
	}
	
	//-------------------------------------------------------------------
	public Skill getSkill(String key) {
		for (Skill tmp : skills) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}
	
	//-------------------------------------------------------------------
	public void addMotivations(Collection<Motivation> toAdd) {
		logger.debug("Add "+toAdd.size()+" motivation to "+ruleset+" ruleset");
		motivations.addAll(toAdd);
		Collections.sort(motivations);
	}
	
	//-------------------------------------------------------------------
	public List<Motivation> getMotivations() {
		return motivations;
	}
	
	//-------------------------------------------------------------------
	public Motivation getMotivation(String key) {
		for (Motivation tmp : motivations) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}
	
	//-------------------------------------------------------------------
	public void addArchetypes(Collection<Archetype> toAdd) {
		logger.debug("Add "+toAdd.size()+" archetypes to "+ruleset+" ruleset");
		archetypes.addAll(toAdd);
		Collections.sort(archetypes);
	}
	
	//-------------------------------------------------------------------
	public List<Archetype> getArchetypes() {
		return archetypes;
	}
	
	//-------------------------------------------------------------------
	public Archetype getArchetype(String key) {
		for (Archetype tmp : archetypes) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}
	
	//-------------------------------------------------------------------
	public void addFlaws(Collection<Flaw> toAdd) {
		logger.debug("Add "+toAdd.size()+" flaws to "+ruleset+" ruleset");
		flaws.addAll(toAdd);
	}
	
	//-------------------------------------------------------------------
	public List<Flaw> getFlaws() {
		List<Flaw> ret = new ArrayList<Flaw>(flaws);
		Collections.sort(ret);
		return ret;
	}
	
	//-------------------------------------------------------------------
	public Flaw getFlaw(String key) {
		for (Flaw tmp : flaws) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}
	
	//-------------------------------------------------------------------
	public void addResources(Collection<Resource> toAdd) {
		logger.debug("Add "+toAdd.size()+" resources to "+ruleset+" ruleset");
		resources.addAll(toAdd);
		Collections.sort(resources);
	}
	
	//-------------------------------------------------------------------
	public List<Resource> getResources() {
		List<Resource> ret = new ArrayList<Resource>(resources);
		Collections.sort(ret);
		return ret;
	}
	
	//-------------------------------------------------------------------
	public Resource getResource(String key) {
		for (Resource tmp : resources) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}
	
	//-------------------------------------------------------------------
	public void addTalent(Collection<Talent> toAdd) {
		logger.debug("Add "+toAdd.size()+" talents to "+ruleset+" ruleset");
		talents.addAll(toAdd);
	}
	
	//-------------------------------------------------------------------
	public List<Talent> getTalents() {
		List<Talent> ret = new ArrayList<Talent>(talents);
		Collections.sort(ret);
		return ret;
	}
	
	//-------------------------------------------------------------------
	public Talent getTalent(String key) {
		for (Talent tmp : talents) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}
	
	//-------------------------------------------------------------------
	public void addItems(Collection<ItemTemplate> toAdd) {
		logger.debug("Add "+toAdd.size()+" items to "+ruleset+" ruleset");
		items.addAll(toAdd);
	}
	
	//-------------------------------------------------------------------
	public List<ItemTemplate> getItemTemplates() {
		List<ItemTemplate> ret = new ArrayList<ItemTemplate>(items);
		Collections.sort(ret);
		return ret;
	}
	
	//-------------------------------------------------------------------
	public List<ItemTemplate> getItemTemplates(ItemType type) {
		List<ItemTemplate> ret = new ArrayList<ItemTemplate>();
		for (ItemTemplate tmp : items) {
			if (tmp.isType(type))
				ret.add(tmp);
		}
		
		Collections.sort(ret);
		return ret;
	}
	
	//-------------------------------------------------------------------
	public ItemTemplate getItemTemplate(String key) {
		for (ItemTemplate tmp : items) {
			if (tmp.getId().equals(key))
				return tmp;
		}
		return null;
	}
	
	//--------------------------------------------------------------------
	public UbiquityCalculationUtil getUtilities() {
		return utility;
	}
	
	//--------------------------------------------------------------------
	public void setUtilities(UbiquityCalculationUtil utils) {
		utility = utils;
	}

}

