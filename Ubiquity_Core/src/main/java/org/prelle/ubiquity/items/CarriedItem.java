package org.prelle.ubiquity.items;

import org.prelle.ubiquity.persist.ItemTemplateConverter;

import de.rpgframework.genericrpg.SelectedValue;

public class CarriedItem implements Comparable<CarriedItem>, SelectedValue<ItemTemplate> {
	
	@org.prelle.simplepersist.Attribute(name="idref",required=true)
	@org.prelle.simplepersist.AttribConvert(ItemTemplateConverter.class)
	private ItemTemplate ref;
	@org.prelle.simplepersist.Attribute(name="custom",required=false)
    private String customName;
	@org.prelle.simplepersist.Attribute(name="count")
	private int count = 1;
	@org.prelle.simplepersist.Attribute(name="loc")
	private ItemLocationType location;

	//--------------------------------------------------------------------
	public CarriedItem() {
	}

	//--------------------------------------------------------------------
	public CarriedItem(ItemTemplate item) {
		ref = item;
	}

	//--------------------------------------------------------------------
	public boolean equals(Object o) {
		if (o instanceof CarriedItem) {
			CarriedItem other = (CarriedItem)o;
			if (ref!=other.getItem()) return false;
			return true;
		}
		return false;
	}

	//--------------------------------------------------------------------
	public String toString() {
		return "CarriedItem "+ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the item
	 */
	public ItemTemplate getItem() {
		return ref;
	}

	//--------------------------------------------------------------------
	/**
	 * @param item the item to set
	 */
	public void setItem(ItemTemplate item) {
		this.ref = item;
	}

    //-------------------------------------------------------------------
    public String getName() {
    	if (customName!=null)
    		return customName;
        return ref.getName();
    }

	//-------------------------------------------------------------------
	public String getShortName() {
		if (customName!=null)
			return customName;
		return ref.getShortName();
	}

    //-------------------------------------------------------------------
    public void setName(String name) {
    	customName = name;
    }

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(CarriedItem other) {
		return ref.compareTo(other.getItem());
	}

	//--------------------------------------------------------------------
	public boolean isType(ItemType type) {
		return ref.isType(type);
	}

	//-------------------------------------------------------------------
	/**
	 * @return the count
	 */
	public int getCount() {
		return count;
	}

	//-------------------------------------------------------------------
	/**
	 * @param count the count to set
	 */
	public void setCount(int count) {
		this.count = count;
	}

	//-------------------------------------------------------------------
	/**
	 * @return the location
	 */
	public ItemLocationType getLocation() {
		return location;
	}

	//-------------------------------------------------------------------
	/**
	 * @param location the location to set
	 */
	public void setLocation(ItemLocationType location) {
		this.location = location;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectedValue#getModifyable()
	 */
	@Override
	public ItemTemplate getModifyable() {
		return ref;
	}
}
