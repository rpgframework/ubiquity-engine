/**
 * 
 */
package org.prelle.ubiquity.items;

import java.text.Collator;
import java.util.ArrayList;
import java.util.List;
import java.util.MissingResourceException;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.ElementListUnion;
import org.prelle.ubiquity.BasePluginData;

import de.rpgframework.genericrpg.SelectableItem;

/**
 * @author Stefan
 *
 */
public class ItemTemplate extends BasePluginData implements Comparable<ItemTemplate>, SelectableItem {
	
	private static Logger logger = LogManager.getLogger("ubiquity");

	@org.prelle.simplepersist.Attribute
	private String    id;
	@org.prelle.simplepersist.Attribute
	private ItemType type;	
	@org.prelle.simplepersist.Attribute(required=false)
	private int    price;
	@org.prelle.simplepersist.Attribute(required=true)
	private int    weight;

	@ElementListUnion({
		@ElementList(entry="weapon",type=Weapon.class),
		@ElementList(entry="rangeweapon",type=RangeWeapon.class),
		@ElementList(entry="armor",type=Armor.class)
	})
	private List<ItemTypeData> typeData;
	
	//--------------------------------------------------------------------
	public ItemTemplate() {
		typeData = new ArrayList<ItemTypeData>();
	}
	
	//--------------------------------------------------------------------
	public ItemTemplate(String id) {
		this();
		this.id = id;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getPageI18NKey()
	 */
	@Override
	public String getPageI18NKey() {
		return "item."+id+".page";
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.BasePluginData#getHelpI18NKey()
	 */
	@Override
	public String getHelpI18NKey() {
		return "item."+id+".desc";
	}

	//--------------------------------------------------------------------
	public String toString() {
		return String.format("%s (wgt=%s cost=%s type=%s)", getName(), price, weight, typeData);
	}

	//--------------------------------------------------------------------
	public String getName() {
		if (i18n==null)
			return id;
		
		try {
			return i18n.getString("item."+id);
		} catch (MissingResourceException mre) {
			logger.error(mre.toString());
			return "item."+id;
		}
	}


	public String getShortName() {
		if (i18n==null)
			return id;

		try {
			return i18n.getString("item."+id+".short");
		} catch (MissingResourceException mre) {
			return getName();
		}
	}

	//--------------------------------------------------------------------
	public String getId() {
		return id;
	}


	//--------------------------------------------------------------------
	/**
	 * @return the type
	 */
	public ItemType getType() {
		return type;
	}


	//--------------------------------------------------------------------
	/**
	 * @return the price
	 */
	public int getPrice() {
		return price;
	}


	//--------------------------------------------------------------------
	/**
	 * @return the weight
	 */
	public int getWeight() {
		return weight;
	}

	//--------------------------------------------------------------------
	public List<ItemTypeData> getTypeData() {
		return typeData;
	}

	//--------------------------------------------------------------------
	public <T> T getType(Class<T> cls) {
		for (ItemTypeData tmp : typeData)
			if (tmp.getClass()==cls)
				return (T) tmp;
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Comparable#compareTo(java.lang.Object)
	 */
	@Override
	public int compareTo(ItemTemplate other) {
		return Collator.getInstance().compare(getName(), other.getName());
	}

	//--------------------------------------------------------------------
	public void add(ItemTypeData data) {
		typeData.add(data);
	}

	//--------------------------------------------------------------------
	public boolean isType(ItemType type) {
		if (type==null)
			return true;
		if (this.type==type)
			return true;
		if ( typeData.isEmpty() && type == ItemType.OTHER) {
			return true;
		}
		for (ItemTypeData data : typeData) {
			if (data.getType() == type) {
				return true;
			}
		}

		return false;
	}

	//--------------------------------------------------------------------
	public void setType(ItemType type) {
		this.type = type;
	}

	//-------------------------------------------------------------------
	/**
	 * @param price the price to set
	 */
	public void setPrice(int price) {
		this.price = price;
	}

	//-------------------------------------------------------------------
	/**
	 * @param weight the weight to set
	 */
	public void setWeight(int weight) {
		this.weight = weight;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectableItem#getTypeId()
	 */
	@Override
	public String getTypeId() {
		return "item";
	}


}
