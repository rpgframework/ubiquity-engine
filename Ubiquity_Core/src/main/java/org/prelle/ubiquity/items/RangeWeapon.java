/**
 * 
 */
package org.prelle.ubiquity.items;

import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.persist.CapacityConverter;
import org.prelle.ubiquity.persist.FireRateConverter;

/**
 * @author prelle
 *
 */
public class RangeWeapon extends Weapon {
	
	public enum FireRate {
		EVERY_FOURTH_TURN,
		EVERY_OTHER_TURN,
		EVERY_TURN,
		SEMIAUTOMATIC,
		AUTOMATIC,
		;
		public String getShortName() {
			return UbiquityCore.getI18nResources().getString("rate."+name().toLowerCase());
		}
	}
	
	/*
	 * In tenth of meter
	 */
	@org.prelle.simplepersist.Attribute
	private int range;
	@org.prelle.simplepersist.Attribute
	@org.prelle.simplepersist.AttribConvert(FireRateConverter.class)
	private FireRate rate;
	@org.prelle.simplepersist.Attribute(name="cap",required=false)
	@org.prelle.simplepersist.AttribConvert(CapacityConverter.class)
	private Capacity capacity;
	
	//--------------------------------------------------------------------
	public RangeWeapon() {
		type = ItemType.WEAPON_RANGE;
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof RangeWeapon) {
            RangeWeapon other = (RangeWeapon) o;
            try {
                if (rate  != other.getRate()) return false;
                if (range != other.getRange()) return false;
                if (capacity!= other.getCapacity()) return false;
                return super.equals(other);
            } catch (Exception e) {
                logger.error("Error comparing items",e);
            }
        }
		return false;
	}

	//--------------------------------------------------------------------
	public String toString() {
		return String.format("%s rng=%s rat=%s cap=%s", super.type, range, rate, capacity);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the range
	 */
	public int getRange() {
		return range;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the rate
	 */
	public FireRate getRate() {
		return rate;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the capacity
	 */
	public Capacity getCapacity() {
		return capacity;
	}

	//-------------------------------------------------------------------
	/**
	 * @param range the range to set
	 */
	public void setRange(int range) {
		this.range = range;
	}

	//-------------------------------------------------------------------
	/**
	 * @param rate the rate to set
	 */
	public void setRate(FireRate rate) {
		this.rate = rate;
	}

	//-------------------------------------------------------------------
	/**
	 * @param capacity the capacity to set
	 */
	public void setCapacity(Capacity capacity) {
		this.capacity = capacity;
	}

}
