package org.prelle.ubiquity.items;

import org.prelle.ubiquity.UbiquityCore;

public class Capacity {
	
	public enum AmmunitionStorage {
		BELT,
		CLIP,
		INTERNAL,
		MAGAZINE,
		REVOLVER
		;
		public String getName() {
			return UbiquityCore.getI18nResources().getString("ammustorage."+name().toLowerCase());
		}
		public String getShortName() {
			return UbiquityCore.getI18nResources().getString("ammustorage."+name().toLowerCase()+".short");
		}
	}
	
	private int amount;
	private AmmunitionStorage storage;
	
	//--------------------------------------------------------------------
	public Capacity() {		
	}

	//--------------------------------------------------------------------
	public String toString() {
		if (storage==null)
			return String.valueOf(amount);
		return amount+"("+storage.getShortName()+")";
	}

	//--------------------------------------------------------------------
	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the storage
	 */
	public AmmunitionStorage getStorage() {
		return storage;
	}

	//--------------------------------------------------------------------
	/**
	 * @param amount the amount to set
	 */
	public void setAmount(int amount) {
		this.amount = amount;
	}

	//--------------------------------------------------------------------
	/**
	 * @param storage the storage to set
	 */
	public void setStorage(AmmunitionStorage storage) {
		this.storage = storage;
	}
	
}