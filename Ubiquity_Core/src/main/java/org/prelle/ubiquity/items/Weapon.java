/**
 * 
 */
package org.prelle.ubiquity.items;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.simplepersist.Element;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.SkillSpecialization;
import org.prelle.ubiquity.UbiquityCore;
import org.prelle.ubiquity.persist.SkillConverter;
import org.prelle.ubiquity.persist.SkillSpecializationConverter;
import org.prelle.ubiquity.persist.WeaponDamageConverter;
import org.prelle.ubiquity.persist.WeaponSpeedConverter;
import org.prelle.ubiquity.requirements.Requirement;
import org.prelle.ubiquity.requirements.RequirementList;

/**
 * @author prelle
 *
 */
public class Weapon extends ItemTypeData {
	
	public enum Speed {
		SLOW,
		AVERAGE,
		FAST;

		public String getShortName() {
			return UbiquityCore.getI18nResources().getString("speed." + name().toLowerCase());
		}
	}

	protected final static Logger logger = LogManager.getLogger("ubiquity.items");
	
	@org.prelle.simplepersist.Attribute(name="skill")
	@org.prelle.simplepersist.AttribConvert(SkillConverter.class)
	private Skill skill;
	@org.prelle.simplepersist.Attribute(required=false)
	@org.prelle.simplepersist.AttribConvert(SkillSpecializationConverter.class)
	private SkillSpecialization special;
	@org.prelle.simplepersist.Attribute
	@org.prelle.simplepersist.AttribConvert(WeaponSpeedConverter.class)
	private Speed speed;
	@org.prelle.simplepersist.Attribute
	@org.prelle.simplepersist.AttribConvert(WeaponDamageConverter.class)
	private Damage damage;
	@Element(name="requires")
	private RequirementList requires;
	
	//--------------------------------------------------------------------
	public Weapon() {
		super(ItemType.WEAPON_CLOSE);
		requires = new RequirementList();
	}
	
	//--------------------------------------------------------------------
	/**
	 * @see java.lang.Object#equals(java.lang.Object)
	 */
	public boolean equals(Object o) {
		if (o instanceof Weapon) {
            Weapon other = (Weapon) o;
            try {
                if (skill != other.getSkill()) return false;
                if (speed != other.getSpeed()) return false;
                if (!damage.equals(other.getDamage())) return false;
                if (!requires.equals(other.getRequirements())) return false;
                return true;
            } catch (Exception e) {
                logger.error("Error comparing items",e);
            }
        }
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public Skill getSkill() {
		return skill;
	}

	//--------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setSkill(Skill skill) {
		this.skill = skill;
	}

	//--------------------------------------------------------------------
	/**
	 * @return the speed
	 */
	public Speed getSpeed() {
		return speed;
	}

	//--------------------------------------------------------------------
	public Damage getDamage() {
		return damage;
	}

	//--------------------------------------------------------------------
	public void addRequirement(Requirement req) {
		requires.add(req);
	}

	//--------------------------------------------------------------------
	public RequirementList getRequirements() {
		return requires;
	}

	//--------------------------------------------------------------------
	public String toString() {
		return String.format("dmg=%s spd=%s req=%s", damage, speed, requires);
	}

	//--------------------------------------------------------------------
	/**
	 * @return the special
	 */
	public SkillSpecialization getSpecial() {
		return special;
	}

	//--------------------------------------------------------------------
	/**
	 * @param special the special to set
	 */
	public void setSpecial(SkillSpecialization special) {
		this.special = special;
	}

	//-------------------------------------------------------------------
	/**
	 * @param speed the speed to set
	 */
	public void setSpeed(Speed speed) {
		this.speed = speed;
	}

	//-------------------------------------------------------------------
	/**
	 * @param damage the damage to set
	 */
	public void setDamage(Damage damage) {
		this.damage = damage;
	}

}
