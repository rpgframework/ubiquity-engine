package org.prelle.ubiquity.requirements;

import org.prelle.simplepersist.AttribConvert;
import org.prelle.simplepersist.Attribute;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.persist.TalentConverter;

/**
 * @author prelle
 *
 */
public class TalentRequirement extends Requirement {

	@Attribute(name="idref")
	@AttribConvert(TalentConverter.class)
    private Talent talent;
	@Attribute(required=false)
    private int val;
    
    //-----------------------------------------------------------------------
    public TalentRequirement() {
    	val = 1;
    }
    
    //-----------------------------------------------------------------------
    public TalentRequirement(Talent skill, int val) {
        this.talent = skill;
        this.val  = val;
    }
    
    //-----------------------------------------------------------------------
    public String toString() {
        if (talent==null)
        	return "SKILL_NOT_SET";
        return talent.getName()+" = "+val;
    }
    
    //-----------------------------------------------------------------------
    public int getValue() {
        return val;
    }
    
    //-----------------------------------------------------------------------
    public void setValue(int val) {
        this.val = val;
    }
    
    //-----------------------------------------------------------------------
    public Object clone() {
        return new TalentRequirement(talent, val);
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean equals(Object o) {
        if (o instanceof TalentRequirement) {
            TalentRequirement other = (TalentRequirement)o;
            if (other.getTalent()!=talent) return false;
            return (other.getValue()==val);
        } else
            return false;
    }
    
    //-----------------------------------------------------------------------
    /**
     */
    public boolean matches(Object o) {
        if (o instanceof TalentRequirement) {
            TalentRequirement other = (TalentRequirement)o;
            if (other.getTalent()!=talent) return false;
            return true;
        } else
            return false;
    }

	//-------------------------------------------------------------------
	/**
	 * @return the skill
	 */
	public Talent getTalent() {
		return talent;
	}

	//-------------------------------------------------------------------
	/**
	 * @param skill the skill to set
	 */
	public void setTalent(Talent skill) {
		this.talent = skill;
	}
    
}// AttributeModification
