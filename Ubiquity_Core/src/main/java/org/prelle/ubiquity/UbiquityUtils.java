/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.items.Armor;
import org.prelle.ubiquity.items.CarriedItem;
import org.prelle.ubiquity.items.ItemLocationType;
import org.prelle.ubiquity.items.ItemTemplate;
import org.prelle.ubiquity.items.ItemType;
import org.prelle.ubiquity.items.ItemTypeData;
import org.prelle.ubiquity.items.Weapon;
import org.prelle.ubiquity.modifications.AttributeModification;
import org.prelle.ubiquity.modifications.ModificationImpl;
import org.prelle.ubiquity.modifications.ResourceModification;
import org.prelle.ubiquity.modifications.SkillModification;
import org.prelle.ubiquity.requirements.AttributeRequirement;
import org.prelle.ubiquity.requirements.Requirement;

import de.rpgframework.RPGFrameworkLoader;
import de.rpgframework.core.RoleplayingSystem;
import de.rpgframework.genericrpg.Datable;
import de.rpgframework.genericrpg.HistoryElement;
import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.products.Adventure;
import de.rpgframework.products.ProductService;

/**
 * @author Stefan
 *
 */
public class UbiquityUtils implements UbiquityCalculationUtil {
	
	private final static Logger logger = LogManager.getLogger("ubiquity");
	
//	private final static ResourceBundle CORE = UbiquityCore.getI18nResources();

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.UbiquityCalculationUtil#calculateSecondaryAttributes(org.prelle.ubiquity.UbiquityCharacter)
	 */
	@Override
	public List<AttributeValue> calculateSecondaryAttributes(UbiquityCharacter model) {
		List<AttributeValue> ret = new ArrayList<AttributeValue>();
		
		for (Attribute key : Attribute.secondaryValues()) {
			AttributeValue a1 = null;
			AttributeValue a2 = null;
			AttributeValue a3 = null;
			AttributeValue de = model.getAttribute(key);
			int oldPoints = de.getPoints();
			int newPoints = de.getPoints();
			switch (key) {
			case MOVE:
				a1 = model.getAttribute(Attribute.STRENGTH);
				a2 = model.getAttribute(Attribute.DEXTERITY); 
				// 
				// TODO: Sportlichkeit
				SkillValue athletics = model.getSkill("athletics");				
				newPoints = a1.getValue()+Math.max(a2.getValue(), (athletics!=null)?athletics.getModifiedValue():0);
				break;
			case PERCEPTION:
				a1 = model.getAttribute(Attribute.INTELLIGENCE);
				a2 = model.getAttribute(Attribute.WILLPOWER); 
				// 
//				logger.debug(key+": Add "+a1+" + "+a2);
				newPoints = a1.getValue()+a2.getValue();
				break;
			case INITIATIVE:
				a1 = model.getAttribute(Attribute.INTELLIGENCE);
				a2 = model.getAttribute(Attribute.DEXTERITY); 
				// 
				newPoints = a1.getValue()+a2.getValue();
				break;
			case DEFENSE:
				a1 = model.getAttribute(Attribute.BODY);
				a2 = model.getAttribute(Attribute.DEXTERITY); 
				a3 = model.getAttribute(Attribute.SIZE);
				// 
				newPoints = a1.getValue()+a2.getValue() - a3.getValue();
				break;
			case STUN:
				a1 = model.getAttribute(Attribute.BODY);
				// 
				newPoints = a1.getValue();
				break;
			case SIZE:
				newPoints = 0;
				break;
			case HEALTH:
				a1 = model.getAttribute(Attribute.BODY);
				a2 = model.getAttribute(Attribute.WILLPOWER);
				a3 = model.getAttribute(Attribute.SIZE);
				// 
				newPoints = a1.getValue()+a2.getValue() + a3.getValue();
				break;
			default:
				if (key.isPrimary())
					continue;
				throw new IllegalArgumentException("Can't handle "+key);
			}
//			logger.debug("Change "+key+" from "+oldPoints+" to "+newPoints);
			de.setPoints(newPoints);
			if (oldPoints!=newPoints)
				ret.add(de);
		}
		return ret;
	}


	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.UbiquityCalculationUtil#convertToHistoryElementList(org.prelle.ubiquity.UbiquityCharacter)
	 */
	@Override
	public List<HistoryElement> convertToHistoryElementList(UbiquityCharacter charac, RoleplayingSystem rules) {
		// Inititial reward
//		HistoryElement initial = new HistoryElement();
//		initial.setName(CORE.getString("label.reward.creation"));
//		initial.addGained();
		
		/*
		 * Build a merged list of rewards and modifications and sort it by time
		 */
		List<Datable> rewardsAndMods = new ArrayList<Datable>();
		rewardsAndMods.addAll(charac.getRewards());
		rewardsAndMods.addAll(charac.getHistory());
		Collections.sort(rewardsAndMods, new Comparator<Datable>() {
			public int compare(Datable o1, Datable o2) {
				Long time1 = 0L;
				Long time2 = 0L;
				if (o1.getDate()!=null)	time1 = o1.getDate().getTime();
				if (o2.getDate()!=null)	time2 = o2.getDate().getTime();
				
				int cmp = time1.compareTo(time2);
				if (cmp==0) {
					if (o1 instanceof RewardImpl && o2 instanceof ModificationImpl) return -1;
					if (o1 instanceof ModificationImpl && o2 instanceof RewardImpl) return  1;
				}
				return cmp;
			}
		});
		
		/*
		 * Now build a list of HistoryElements. Start a new H
		 */
		List<HistoryElement> ret = new ArrayList<HistoryElement>();
		HistoryElementImpl current = null;
		
		ProductService prodServ = RPGFrameworkLoader.getInstance().getProductService();
		for (Datable item : rewardsAndMods) {
			logger.info("* found "+item);
			if (item instanceof RewardImpl) {
				RewardImpl reward = (RewardImpl)item;
				Adventure adv = null;
				if (reward.getId()!=null) {
					adv = prodServ.getAdventure(rules, reward.getId());
					if (adv==null) {
						logger.warn("Rewards of character '"+charac.getName()+"' reference an unknown adventure: "+reward.getId());
					}
				}
				// If is same adventure as current, keep same history element				
				if (current==null || !(adv!=null && adv==current.getAdventure()) ) {
					current = new HistoryElementImpl();
					current.setName(reward.getTitle());
					if (adv!=null) {
						current.setName(adv.getTitle());
						current.setAdventure(adv);					
					}
					ret.add(current);
				}
				current.addGained(reward);
			} else if (item instanceof ModificationImpl) {
				if (current==null) {
					logger.error("Failed preparing history: Exp spent on modification without previous reward");
				} else {
					current.addSpent((ModificationImpl) item);
				}
			}
		}
		
		return ret;
	}
	//-------------------------------------------------------------------
	/**
	 * Iterate through enhancements. Get their modifications. Apply those that
	 * refer to the item itself. Return those that needs to be applied to the
	 * character
	 */
	@Override
	public void applyToCharacter(UbiquityCharacter model, List<Modification> modifications) {
		logger.debug("Apply "+modifications.size()+" modifications to character");
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification data = (AttributeModification)mod;
				model.getAttribute(data.getAttribute()).addModification(data);
				logger.debug("Added "+data.getAttribute()+" +"+data.getValue());
				calculateSecondaryAttributes(model);
			} else if (mod instanceof SkillModification) {
				SkillModification data = (SkillModification)mod;
				model.getSkill(data.getSkill()).addModification(data);
				logger.debug("Added "+data.getSkill()+" "+data.getValue());
			} else
				logger.warn("Don't know how to apply to character: "+mod.getClass());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Iterate through enhancements. Get their modifications. Apply those that
	 * refer to the item itself. Return those that needs to be applied to the
	 * character
	 */
	@Override
	public void unapplyToCharacter(UbiquityCharacter model, List<Modification> modifications) {
		logger.debug("unapply "+modifications.size()+" modifications from character");
		for (Modification mod : modifications) {
			if (mod instanceof AttributeModification) {
				AttributeModification data = (AttributeModification)mod;
				model.getAttribute(data.getAttribute()).removeModification(data);
				logger.debug("Removed "+data.getAttribute()+" +"+data.getValue());
			} else if (mod instanceof SkillModification) {
				SkillModification data = (SkillModification)mod;
				model.getSkill(data.getSkill()).removeModification(mod);
				logger.debug("Removed "+data.getSkill()+" "+data.getValue());
			} else
				logger.warn("Don't know how to apply to character: "+mod.getClass());
		}
	}

	//-------------------------------------------------------------------
	/**
	 * Apply those modifications not saved in the character - e.g. the
	 * resistance boni depending on level
	 */
	public void applyAfterLoadModifications(UbiquityCharacter data) {
		logger.info("calculate modifications by talents, etc.");
		
		/*
		 * Talents
		 */
		for (TalentValue talentRef : data.getTalents()) {
			logger.info("TalentValue "+talentRef);
			Talent talent = talentRef.getTalent();
			for (Modification mod : talent.getModifications()) {
				mod.setSource(talent);
				if (mod instanceof AttributeModification) {
					AttributeModification aMod = (AttributeModification)mod;
					AttributeValue aVal = data.getAttribute(aMod.getAttribute());
					aMod.setValue(talentRef.getLevel()*aMod.getValue());
					aMod.setSource(talentRef);
					logger.info("Add "+aMod+"  (already exist = "+aVal.getModifications()+")");
					aVal.addModification(aMod);
				} else if (mod instanceof SkillModification) {
					SkillModification sMod = (SkillModification)mod;
					if (sMod.getSkill()==Skill.CHOSEN) {
						Skill skill = talentRef.getSkillOption();
						SkillValue sVal = data.getSkill(skill);
						SkillModification sMod2 = new SkillModification(skill, sMod.getValue()*talentRef.getLevel());
						sMod2.setSource(talentRef);
						if (sVal==null) {
							logger.error("In character "+data.getName()+" the talent "+talent.getName()+" has a skillmod for a skill not present in character");
						} else {
							System.err.println("Add "+sMod.getValue()+"/"+talentRef.getLevel()+"/"+sMod2.getValue()+" to "+sVal.getModifiedValue());
							sVal.addModification(sMod2);
						}
					} else {
						SkillValue sVal = data.getSkill(sMod.getSkill());
						if (sVal==null) {
							logger.error("In character "+data.getName()+" the talent "+talent.getName()+" has a skillmod for a skill not present in character");
						} else {
							sVal.addModification(sMod);
						}
					}
				} else {
					logger.warn("Unsupported modification "+mod+" in power "+talent);
				}
			}
		}
		
		/*
		 * Calculate derived attributes
		 */
		calculateSecondaryAttributes(data);
	}

	//--------------------------------------------------------------------
	public static SkillValue getSafeSkillValue(UbiquityCharacter model, Skill key) {
		SkillValue ret = model.getSkill(key);
		if (ret!=null)
			return ret;
				
		ret = new SkillValue(key, 0);
		// Is the char Universalist?		
		TalentValue jackOfAll = model.getTalent("jackofall");
		if (jackOfAll!=null) {
			if (key.isPartOfGroup()) {
				ret.addModification( new SkillModification(key, -3 + jackOfAll.getLevel()) );
			} else {
				ret.addModification( new SkillModification(key, -1 + jackOfAll.getLevel()) );
			}
		} else {
			if (key.isPartOfGroup()) {
				ret.addModification( new SkillModification(key, -9) );
			} else {
				ret.addModification( new SkillModification(key, -2));
			}
		}
		
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * Return the final dice pool a character has using a weapon
	 * @param model The character
	 * @param item  The weapon (or other item)
	 * @return Number of dices in pool. -1 if no weapon
	 */
	public static int getWeaponModifier(UbiquityCharacter model, CarriedItem item) {
		if (!(item.isType(ItemType.WEAPON_CLOSE) || item.isType(ItemType.WEAPON_RANGE))) {
			return 0;
		}
		
		logger.info("-------"+item+"-------------");
		ItemTemplate real = item.getItem();
		for (ItemTypeData data : real.getTypeData()) {
			if (data instanceof Weapon) {
				Weapon weapon = (Weapon)data;
				// 1. Consider weapon damage
				int pool = 0;
				// 2. Add weapon skill
				Skill skill = weapon.getSkill();
				SkillValue sval = model.getSkill(skill);
				pool += model.getSkillDicePool(skill);
				logger.info("Skill dicepool is "+pool);
				// 3. Add eventually existing modification
				SkillSpecialization spec = weapon.getSpecial();
				if (spec!=null && sval.getSpecializations().contains(spec)) {
					pool += 1;
					logger.info("Found spec "+spec+" .. pool now "+pool);
				} else
					logger.info("Either spec is null ("+spec+") or character has no specs "+sval.getSpecializations());
				// 4. Subtract modifiers by missing strength
				for (Requirement req : weapon.getRequirements()) {
					logger.info("*****"+req+" for "+item);
					if (req instanceof AttributeRequirement) {
						AttributeRequirement areq = (AttributeRequirement)req;
						AttributeValue aval = model.getAttribute(areq.getAttr());
						logger.info("  attr="+aval+"  areq="+areq);
						if (aval.getValue()<areq.getVal()) {
							pool -= (areq.getVal()-aval.getValue())*2;
							logger.info("Reduce pool for lack of "+areq.getAttr()+"  ..pool now "+pool);
						} else
							logger.info(areq.getAttr()+" is sufficient ..pool still "+pool);
					} else
						logger.warn("Unchecked requirement "+req);
				}
				return pool;
			}
		}
		return -1;
	}

	//--------------------------------------------------------------------
	public static int getTotalDefenseModifier(UbiquityCharacter model) {
		int ret = 0;
		for (CarriedItem item : model.getItems()) {
			if (item.isType(ItemType.ARMOR)) {
				Armor armor = item.getItem().getType(Armor.class);
				ret += armor.getDefense();
			}
		}
		return ret;
	}

	//--------------------------------------------------------------------
	public static int getTotalArmorHandicap(UbiquityCharacter model) {
		int ret = 0;
		for (CarriedItem item : model.getItems()) {
			if (item.isType(ItemType.ARMOR)) {
				Armor armor = item.getItem().getType(Armor.class);
				ret += armor.getHandicap();
			}
		}
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @return Changed attributes
	 */
	private static List<Attribute> equip(UbiquityCharacter model, CarriedItem item) {
		if (!model.getItems().contains(item))
			throw new IllegalStateException("Not in inventory");
		if (item.getLocation()==ItemLocationType.BODY)
			throw new IllegalStateException("Already equipped");
		
		item.setLocation(ItemLocationType.BODY);
		
		// Apply modifications
		List<Attribute> ret = new ArrayList<>();
		for (ItemTypeData data : item.getItem().getTypeData()) {
			if (data instanceof Armor) {
				Armor armor = (Armor)data;
				if (armor.getDefense()!=0) {
					Attribute key = Attribute.DEFENSE;
					AttributeModification mod = new AttributeModification(key, armor.getDefense());
					mod.setSource(item);
					model.getAttribute(key).addModification(mod);
					ret.add(key);
				}
				if (armor.getHandicap()!=0) {
					Attribute key = Attribute.DEXTERITY;
					AttributeModification mod = new AttributeModification(key, armor.getDefense());
					mod.setSource(item);
					model.getAttribute(key).addModification(mod);
					ret.add(key);
				}
			}
		}
		
		return ret;
	}

	//--------------------------------------------------------------------
	/**
	 * @return Changed attributes
	 */
	private static List<Attribute> unequip(UbiquityCharacter model, CarriedItem item) {
		item.setLocation(ItemLocationType.SOMEWHEREELSE);
		
		// Unapply attribute modifications
		List<Attribute> ret = new ArrayList<>();
		for (Attribute key : Attribute.primaryValues()) {
			AttributeValue aVal = model.getAttribute(key);
			for (AttributeModification mod : new ArrayList<>(aVal.getModifications())) {
				if (mod.getSource()==item) {
					logger.info("Remove "+mod);
					aVal.removeModification(mod);
					ret.add(key);
				}
			}
		}
		
		return ret;
	}


	//-------------------------------------------------------------------
	public static void reward(UbiquityCharacter charac, RewardImpl reward) {
		logger.info("Add reward "+reward+" to "+charac);
		charac.addReward(reward);

		charac.setExperienceFree(charac.getExperienceFree() + reward.getExperiencePoints());
		for (Modification mod : reward.getModifications()) {
			if (mod instanceof ResourceModification) {
				Resource res = ((ResourceModification)mod).getResource();
				int      val = ((ResourceModification)mod).getValue();
				String title = ((ResourceModification)mod).getResourceName();

//				if (res.isBaseResource()) {
//					for (ResourceValue ref : charac.getResources()) {
//						if (ref.getResource()==res) {
//							ref.setValue(ref.getValue() + val);
//							logger.info("Reward existing resource to "+ref);
//							ref.setComment(title);
//							break;
//						}
//					}
//				} else {
					ResourceValue ref = new ResourceValue(res, val);
					ref.setComment(title);
					logger.info("Reward new resource "+ref);
					charac.addResource(ref);
//				}
				// Add to history
				charac.addToHistory(mod);
			} else {
				logger.error("Unsupported modification: "+mod.getClass());
			}
		}

	}

}
