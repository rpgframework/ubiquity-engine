/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="resources")
@ElementList(entry="resource",type=Resource.class,inline=true)
public class ResourceList extends ArrayList<Resource> {

}
