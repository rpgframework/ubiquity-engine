/**
 * 
 */
package org.prelle.ubiquity;

import java.util.ArrayList;

import org.prelle.simplepersist.ElementList;
import org.prelle.simplepersist.Root;

/**
 * @author prelle
 *
 */
@SuppressWarnings("serial")
@Root(name="motivations")
@ElementList(entry="motivation",type=Motivation.class,inline=true)
public class MotivationList extends ArrayList<Motivation> {

}
