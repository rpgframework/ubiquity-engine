/**
 *
 */
package org.prelle.ubiquity.chargen;

import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.AttributeValue;
import org.prelle.ubiquity.GenerationTemplate;
import org.prelle.ubiquity.UbiquityCalculationUtil;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.AttributeController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.modifications.AttributeModification;

/**
 * @author prelle
 *
 */
public class AttributeGenerator implements AttributeController {

	private static Logger logger = LogManager.getLogger("ubiquity.chargen");

	private UbiquityRuleset rules;
	private UbiquityCalculationUtil calcUtil;
	private GenerationTemplate template;
	private UbiquityCharacter model;
	private int pointsLeft;

	//-------------------------------------------------------------------
	/**
	 * @param template
	 */
	public AttributeGenerator(GenerationTemplate template, UbiquityRuleset rules, UbiquityCharacter model) {
		this.template = template;
		this.model    = model;
		this.calcUtil = rules.getUtilities();
		this.rules    = rules;
		pointsLeft    = template.getAttributePoints();

		model.initPrimaryAttributes();
		for (Attribute attr : Attribute.primaryValues()) {
			// Value should have a minimum of 1
			AttributeValue val = model.getAttribute(attr);
			val.setPoints(1);
			pointsLeft -= 1;
		}

		model.setRuleset(rules);
		for (AttributeValue changed : calcUtil.calculateSecondaryAttributes(model)) {
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, changed.getAttribute(), changed));

		}
	}

	//-------------------------------------------------------------------
	public void changeTemplate(GenerationTemplate template) {
		if (this.template==template)
			return;

		logger.debug("Before: "+pointsLeft+"/"+this.template.getAttributePoints());
		pointsLeft += (template.getAttributePoints() - this.template.getAttributePoints());
		this.template = template;
		logger.debug("After: "+pointsLeft+"/"+this.template.getAttributePoints());
		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_ATTRIBUTES, null, pointsLeft));
	}

	//-------------------------------------------------------------------
	public void addModification(AttributeModification mod) {
		logger.debug("addModification");
		Attribute key = mod.getAttribute();
		model.getAttribute(key).addModification(mod);
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, key, model.getAttribute(key)));

		List<AttributeValue> changes =rules.getUtilities().calculateSecondaryAttributes(model);
		for (AttributeValue changed : changes) {
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, changed.getAttribute(), changed));
		}
	}

	//-------------------------------------------------------------------
	public void removeModification(AttributeModification mod) {
		logger.debug("remove Modification");

		Attribute key = mod.getAttribute();
		model.getAttribute(key).removeModification(mod);
		GenerationEventDispatcher.fireEvent(
				new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, key, model.getAttribute(key)));

		List<AttributeValue> changes = rules.getUtilities().calculateSecondaryAttributes(model);
		for (AttributeValue changed : changes) {
			GenerationEventDispatcher.fireEvent(
					new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, changed.getAttribute(), changed));
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.AttributeController#increase(org.prelle.ubiquity.Attribute)
	 */
	@Override
	public boolean increase(Attribute attrib) {
		if (!attrib.isPrimary()) {
			logger.debug(attrib+" is not primary");
			return true;
		}

		AttributeValue val = model.getAttribute(attrib);
		return increase(val);
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#increase(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean increase(AttributeValue val) {
		if (!canBeIncreased(val)) {
			logger.warn("Trying to increase attribute "+val.getAttribute()+" that cannot be increased");
			return false;
		}

		logger.debug("increase "+val.getModifyable()+" from "+val);
		if (val.getPoints()>=6)
			return false;
		if (pointsLeft==0)
			return false;

		// Modify attribute. This also fires an event
		val.setPoints(val.getPoints()+1);
		pointsLeft--;


		GenerationEvent event = new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, val.getAttribute(), val);
		GenerationEventDispatcher.fireEvent(event);

		// Update derived
		for (AttributeValue changed : rules.getUtilities().calculateSecondaryAttributes(model)) {
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, changed.getAttribute(), changed));
		}

		return true;
	}

//	//-------------------------------------------------------------------
//	private void checkIncreaseButtons() {
//		if (attribCallback==null) return;
//
//		for (Attribute attr : Attribute.primaryValues()) {
//			PerAttribute perAttrib = values.get(attr);
//			attribCallback.setIncreaseButton(attr, (pointsForAttributes>0) && (perAttrib.getDistributed()<3));
//		}
//	}

	//-------------------------------------------------------------------
	public boolean decrease(Attribute attrib) {
		if (!attrib.isPrimary())
			return false;

		AttributeValue val = model.getAttribute(attrib);
		return decrease(val);
	}

	//-------------------------------------------------------------------
	public boolean decrease(AttributeValue val) {
		if (!canBeDecreased(val)) {
			return false;
		}

		// Modify attribute. This also fires an event
		val.setPoints(val.getPoints()-1);
		pointsLeft++;

		GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.POINTS_LEFT_ATTRIBUTES, null, pointsLeft));
		GenerationEvent event = new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, val.getAttribute(), val);
		GenerationEventDispatcher.fireEvent(event);

		// Update derived
		for (AttributeValue changed : rules.getUtilities().calculateSecondaryAttributes(model)) {
			GenerationEventDispatcher.fireEvent(new GenerationEvent(GenerationEventType.ATTRIBUTE_CHANGED, changed.getAttribute(), changed));
		}

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.AttributeController#canBeDecreased(org.prelle.ubiquity.Attribute)
	 */
	@Override
	public boolean canBeDecreased(Attribute key) {
		return key.isPrimary() && model.getAttribute(key).getPoints()>1;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeDecreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean canBeDecreased(AttributeValue val) {
		return val.getModifyable().isPrimary() && val.getPoints()>1;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.AttributeController#canBeIncreased(org.prelle.ubiquity.Attribute)
	 */
	@Override
	public boolean canBeIncreased(Attribute key) {
		return key.isPrimary() && model.getAttribute(key).getPoints()<template.getAttributeMax() && pointsLeft>0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.NumericalValueController#canBeIncreased(de.rpgframework.genericrpg.NumericalValue)
	 */
	@Override
	public boolean canBeIncreased(AttributeValue val) {
		return val.getAttribute().isPrimary() && val.getPoints()<template.getAttributeMax() && pointsLeft>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.AttributeController#getPointsLeft()
	 */
	@Override
	public int getPointsLeft() {
		return pointsLeft;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.AttributeController#getIncreaseCost(org.prelle.ubiquity.Attribute)
	 */
	@Override
	public int getIncreaseCost(Attribute key) {
		return 1;
	}

}
