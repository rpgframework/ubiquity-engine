package org.prelle.ubiquity.charctrl;

import org.prelle.ubiquity.Attribute;
import org.prelle.ubiquity.AttributeValue;

import de.rpgframework.genericrpg.NumericalValueController;

public interface AttributeController extends NumericalValueController<Attribute, AttributeValue>{

	//-------------------------------------------------------------------
	/**
	 * Only for generation: Returns the available amount of points
	 * to distribute
	 */
	public int getPointsLeft();

	//-------------------------------------------------------------------
	public boolean canBeDecreased(Attribute key);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(Attribute key);

	//-------------------------------------------------------------------
	public int getIncreaseCost(Attribute key);

	//-------------------------------------------------------------------
	public boolean increase(Attribute key);

	//-------------------------------------------------------------------
	public boolean decrease(Attribute key);

}