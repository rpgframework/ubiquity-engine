package org.prelle.ubiquity.charctrl;

import java.util.List;

import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.SkillSpecialization;
import org.prelle.ubiquity.SkillSpecializationValue;
import org.prelle.ubiquity.SkillValue;
import org.prelle.ubiquity.UbiquityRuleset;

public interface SkillController extends Controller {

	//-------------------------------------------------------------------
	public UbiquityRuleset getRuleset();

	//-------------------------------------------------------------------
	/**
	 * Create a mixed list of skills owned by the character and those
	 * available.
	 */
	public List<Skill> getAvailableSkills();
	
	//-------------------------------------------------------------------
	/**
	 * Only for generation: Returns the available amount of points
	 * to distribute
	 */
	public double getPointsLeft();

	//-------------------------------------------------------------------
	public boolean canBeDecreased(Skill key);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(Skill key);

	//-------------------------------------------------------------------
	public int getIncreaseCost(SkillValue key);

	//-------------------------------------------------------------------
	public boolean increase(Skill key);

	//-------------------------------------------------------------------
	public boolean decrease(Skill key);

	//-------------------------------------------------------------------
	public List<SkillSpecialization> getAvailableSpecializations(Skill key);

	//-------------------------------------------------------------------
	public boolean canSelectSpecialization(SkillSpecialization key);

	//-------------------------------------------------------------------
	public boolean canDeselectSpecialization(SkillSpecializationValue key);

	//-------------------------------------------------------------------
	public SkillSpecializationValue selectSpecialization(SkillSpecialization key);

	//-------------------------------------------------------------------
	public boolean deselectSpecialization(SkillSpecializationValue key);

	//-------------------------------------------------------------------
	public boolean canIncreaseSpecialization(SkillSpecializationValue key);

	//-------------------------------------------------------------------
	public boolean canDecreaseSpecialization(SkillSpecializationValue key);

	//-------------------------------------------------------------------
	public boolean increaseSpecialization(SkillSpecializationValue key);

	//-------------------------------------------------------------------
	public boolean decreaseSpecialization(SkillSpecializationValue key);

}