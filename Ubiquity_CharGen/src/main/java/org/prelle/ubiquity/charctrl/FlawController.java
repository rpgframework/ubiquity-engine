/**
 * 
 */
package org.prelle.ubiquity.charctrl;

import java.util.List;

import org.prelle.ubiquity.Flaw;
import org.prelle.ubiquity.FlawValue;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.TalentValue;

import de.rpgframework.genericrpg.SelectionController;

/**
 * @author Stefan
 *
 */
public interface FlawController extends SelectionController<Flaw, FlawValue> {
//	
//	//-------------------------------------------------------------------
//	public List<Flaw> getAvailableFlaws();
//
//	//-------------------------------------------------------------------
//	public boolean canBeSelected(Flaw talent);
//
//	//-------------------------------------------------------------------
//	public boolean canBeDeSelected(FlawValue key);
//
//	//-------------------------------------------------------------------
//	public boolean select(Flaw talent);
//
//	//-------------------------------------------------------------------
//	public boolean deselect(FlawValue key);

}
