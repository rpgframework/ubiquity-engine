/**
 * 
 */
package org.prelle.ubiquity.charctrl;

import java.util.List;

import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.TalentValue;

import de.rpgframework.genericrpg.SelectionController;

/**
 * @author Stefan
 *
 */
public interface TalentController extends SelectionController<Talent, TalentValue> {

	//-------------------------------------------------------------------
	public double getPointsLeft();

	//-------------------------------------------------------------------
	/**
	 * Get all the skill options - remove those already selected
	 */
	public List<Skill> getOptions(Talent talent);

	//-------------------------------------------------------------------
	public TalentValue select(Talent talent, Skill skill);

	//-------------------------------------------------------------------
	public boolean canBeDecreased(TalentValue key);

	//-------------------------------------------------------------------
	public boolean canBeIncreased(TalentValue key);

	//-------------------------------------------------------------------
	public boolean increase(TalentValue key);

	//-------------------------------------------------------------------
	public boolean decrease(TalentValue key);

	//-------------------------------------------------------------------
	public boolean hasSkillMaster(Skill skill);

}
