/**
 * 
 */
package org.prelle.ubiquity.charctrl;

import org.prelle.ubiquity.items.CarriedItem;
import org.prelle.ubiquity.items.ItemTemplate;

import de.rpgframework.genericrpg.SelectionController;

/**
 * @author prelle
 *
 */
public interface ItemController extends SelectionController<ItemTemplate, CarriedItem> {

}
