/**
 * 
 */
package org.prelle.ubiquity.levelling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Skill;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.TalentValue;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.TalentController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventListener;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.modifications.AttributeModification;
import org.prelle.ubiquity.modifications.ModificationImpl;
import org.prelle.ubiquity.modifications.SkillModification;
import org.prelle.ubiquity.modifications.TalentModification;
import org.prelle.ubiquity.requirements.Requirement;

import de.rpgframework.genericrpg.modification.Modification;
import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author Stefan
 *
 */
public class TalentLeveller implements TalentController, GenerationEventListener {
	
	private static Logger logger = LogManager.getLogger("ubiquity.level.resource");

	private List<ModificationImpl> undoList;
	private Map<TalentValue, Stack<TalentModification>> talentUndoStack;

	private UbiquityCharacter model;
	private UbiquityRuleset rules;
	private CharacterLeveller charGen;
	
	private ArrayList<Talent> availableTal;
	private boolean showUnavailableTalents;

	//--------------------------------------------------------------------
	/**
	 */
	public TalentLeveller(CharacterLeveller ctrl, UbiquityCharacter model, List<ModificationImpl> undoList) {
		this.charGen = ctrl;
		this.rules = ctrl.getRuleset();
		this.model = model;
		this.undoList = undoList;
		
		availableTal = new ArrayList<>();

		talentUndoStack = new HashMap<TalentValue, Stack<TalentModification>>();
		for (TalentValue key : model.getTalents())
			talentUndoStack.put(key, new Stack<TalentModification>());
		
		updateAvailable();
		GenerationEventDispatcher.addListener(this);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#getPointsLeft()
	 */
	@Override
	public double getPointsLeft() {
		return model.getExperienceFree();
	}

	//--------------------------------------------------------------------
	private void updateAvailable() {
		logger.debug("updateAvailable");
		availableTal.clear();
		availableTal.addAll(rules.getTalents());
		
		// Remove those already selected
		for (TalentValue tmp : model.getTalents()) {
			availableTal.remove(tmp.getTalent());
		}
		
		// Remove those that cannot be selected
		if (!showUnavailableTalents) {
			for (Talent tmp : new ArrayList<Talent>(availableTal)) {
			if (!canBeSelected(tmp))
				availableTal.remove(tmp);
			}
		}

		// Inform GUI
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.TALENT_AVAILABLE_CHANGED, 
				availableTal));
		logger.debug("updateAvailable returns "+availableTal);
	}

	//--------------------------------------------------------------------
	@Override
	public List<Talent> getAvailable() {
		return availableTal;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#canBeSelected(org.prelle.ubiquity.Talent)
	 */
	@Override
	public boolean canBeSelected(Talent talent) {
		// Does the character have enough points?
		if (model.getExperienceFree()<15)
			return false;
		
		// Is the talent limited to character creation?
		if (talent.isGenerationOnly())
			return false;
		
		// Check if already selected
		for (TalentValue val : model.getTalents()) {
			if (val.getTalent()==talent) {
				if (getOptions(talent).isEmpty())
					return false;
				else
					// More options possible
					break;
			}
		}
		
		// Are requirements met
		for (Requirement req : talent.getRequirements()) {
			if (!model.meetsRequirement(req))
				return false;
		}

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#canBeDeSelected(org.prelle.ubiquity.TalentValue)
	 */
	@Override
	public boolean canBeDeselected(TalentValue key) {
		if (!model.getTalents().contains(key))
			return false;

		// Search for a modification to remove
		Stack<TalentModification> stack = talentUndoStack.get(key);
		if (stack!=null) {
			for (TalentModification mod : stack) {
				if (mod.getTalent()==key.getTalent())
					return true;
			}
		}
		
		return false;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#getOptions(org.prelle.ubiquity.Talent)
	 */
	@Override
	public List<Skill> getOptions(Talent talent) {
		ArrayList<Skill> options = new ArrayList<>(talent.getSkillOptions());

		for (TalentValue check : model.getTalents()) {
			if (check.getTalent()!=talent)
				continue;
			if (check.getSkillOption()!=null) {
				// Talent with this option already chosen
				logger.debug("Talent "+talent.getId()+", option "+check.getSkillOption()+" already chosen");
				options.remove(check.getSkillOption());
			}
		}
		logger.debug("Remaining skill options for "+talent+": "+options);
		return options;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#select(org.prelle.ubiquity.Talent)
	 */
	@Override
	public TalentValue select(Talent talent) {
		logger.info("Select "+talent);
		
		if (!canBeSelected(talent))
			return null;
		
		if (!talent.getSkillOptions().isEmpty()) {
			logger.error("Cannot select "+talent+" without an skill option");
			return null;
		}
		
		TalentValue val = new TalentValue(talent, 0);
		if (talent.getLevel()>0)
			val.setLevel(1);
		model.addTalent(val);

		// Apply and memorize the talents modifications
		applyTalentModifications(val);
		
		model.setExperienceFree(model.getExperienceFree() -15);
		model.setExperienceInvested(model.getExperienceInvested() +15);
		
		// Mark in history 
		TalentModification mod = new TalentModification(talent, 1);
		mod.setExpCost(15);
		undoList.add(mod);
		if (!talentUndoStack.containsKey(val))
			talentUndoStack.put(val, new Stack<TalentModification>());
		talentUndoStack.get(val).push(mod);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.TALENT_ADDED, 
				val));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.EXPERIENCE_CHANGED,
				new int[]{
						model.getExperienceFree(), 
						model.getExperienceInvested()
				}));
		
		updateAvailable();
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#select(org.prelle.ubiquity.Talent, org.prelle.ubiquity.Skill)
	 */
	@Override
	public TalentValue select(Talent talent, Skill skill) {
		logger.info("Select "+talent);
		
		if (!canBeSelected(talent))
			return null;
		
		if (talent.getSkillOptions().isEmpty()) {
			logger.warn("Redirecting to correct select method for "+talent);
			return select(talent);
		}
		
		TalentValue val = new TalentValue(talent, skill, 1);
		// Apply modifications
		// Specialize modifications, if present
		for (Modification mod : talent.getModifications()) {
			if (mod instanceof SkillModification) {
				SkillModification sMod = (SkillModification)mod;
				if (sMod.getSkill()==Skill.CHOSEN) {
					SkillModification realSMod = new SkillModification(skill, sMod.getValue());
					val.addModification(realSMod);
					logger.debug("Add as "+realSMod);
					charGen.apply(realSMod);
					continue;
				}
			}
			charGen.apply(mod);
		}
		
		model.addTalent(val);
		logger.info("Selected "+val);
		
		model.setExperienceFree(model.getExperienceFree() -15);
		model.setExperienceInvested(model.getExperienceInvested() +15);
		
		// Mark in history 
		TalentModification mod = new TalentModification(talent, 1);
		mod.setExpCost(15);
		undoList.add(mod);
		if (!talentUndoStack.containsKey(val))
			talentUndoStack.put(val, new Stack<TalentModification>());
		talentUndoStack.get(val).push(mod);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.TALENT_ADDED, 
				val));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.EXPERIENCE_CHANGED,
				new int[]{
						model.getExperienceFree(), 
						model.getExperienceInvested()
				}));

		updateAvailable();
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#deselect(org.prelle.ubiquity.TalentValue)
	 */
	@Override
	public boolean deselect(TalentValue val) {
		logger.info("Deselect "+val);
		
		if (!canBeDeselected(val)) {
			logger.debug("  trying to deselect a Talent that cannot be deselected: "+val);
			return false;
		}

		// Search for a modification to remove
		TalentModification mod = null;
		Stack<TalentModification> stack = talentUndoStack.get(val);
		if (stack!=null) {
			for (TalentModification tmp : stack) {
				if (tmp.getTalent()==val.getTalent()) {
					mod = tmp;
				}
			}
		}
		if (mod==null) {
			logger.error("Cannot find modification to undo");
			return false;
		}
		// Remove from history
		undoList.remove(mod);
		stack.remove(mod);
				
		// Remove from character
		model.removeTalent(val);

		// Undo and forget the talents modifications
		undoTalentModifications(val);
		
		// Give exp back
		model.setExperienceFree(model.getExperienceFree()+ mod.getExpCost());
		model.setExperienceInvested(model.getExperienceInvested() -mod.getExpCost());

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.TALENT_REMOVED, 
				val));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.EXPERIENCE_CHANGED,
				new int[]{
						model.getExperienceFree(), 
						model.getExperienceInvested()
				}));

		updateAvailable();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#canBeDecreased(org.prelle.ubiquity.TalentValue)
	 */
	@Override
	public boolean canBeDecreased(TalentValue key) {
		Talent talent = key.getTalent();
		// Has talent multiple levels?
		if (talent.getLevel()<=1)
			return false;
		
		return key.getLevel()>=0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#canBeIncreased(org.prelle.ubiquity.TalentValue)
	 */
	@Override
	public boolean canBeIncreased(TalentValue key) {
		Talent talent = key.getTalent();
		// Has talent multiple levels?
		if (talent.getLevel()<=1)
			return false;
		
		if (key.getLevel()>=talent.getLevel())
			return false;

		return model.getExperienceFree()>=15;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#increase(org.prelle.ubiquity.TalentValue)
	 */
	@Override
	public boolean increase(TalentValue key) {
		logger.debug("increase "+key);
		if (!canBeIncreased(key))
			return false;
		
		key.setLevel(key.getLevel()+1);
		model.setExperienceFree(model.getExperienceFree() -15);
		model.setExperienceInvested(model.getExperienceInvested() +15);
		
		// Mark in history 
		TalentModification mod = new TalentModification(key.getTalent(), 1);
		mod.setExpCost(15);
		undoList.add(mod);
		if (!talentUndoStack.containsKey(key))
			talentUndoStack.put(key, new Stack<TalentModification>());
		talentUndoStack.get(key).push(mod);

		// Apply and memorize the talents modifications
		undoTalentModifications(key);
		applyTalentModifications(key);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.TALENT_CHANGED, 
				key));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.EXPERIENCE_CHANGED,
				new int[]{
						model.getExperienceFree(), 
						model.getExperienceInvested()
				}));
		
		updateAvailable();
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#decrease(org.prelle.ubiquity.TalentValue)
	 */
	@Override
	public boolean decrease(TalentValue key) {
		logger.debug("decrease "+key);
		if (!canBeDecreased(key))
			return false;

		if (key.getLevel()==1) {
			logger.debug("  decreasing to 0 is the same as deselect");
			return deselect(key);
		}

		// Search for a modification to remove
		TalentModification mod = null;
		Stack<TalentModification> stack = talentUndoStack.get(key);
		if (stack!=null) {
			for (TalentModification tmp : stack) {
				if (tmp.getTalent()==key.getTalent()) {
					mod = tmp;
				}
			}
		}
		if (mod==null) {
			logger.error("Cannot find modification to undo");
			return false;
		}
		// Remove from history
		undoList.remove(mod);
		stack.remove(mod);

		key.setLevel(key.getLevel()-1);

		undoTalentModifications(key);
		applyTalentModifications(key);
		
		// Give exp back
		model.setExperienceFree(model.getExperienceFree()+ mod.getExpCost());
		model.setExperienceInvested(model.getExperienceInvested() -mod.getExpCost());
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.TALENT_CHANGED, 
				key));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.EXPERIENCE_CHANGED,
				new int[]{
						model.getExperienceFree(), 
						model.getExperienceInvested()
				}));
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.TalentController#hasSkillMaster(org.prelle.ubiquity.Skill)
	 */
	@Override
	public boolean hasSkillMaster(Skill skill) {
//		for (TalentValue val : model.getTalents()) {
//			if (val.getTalent().getId().equals("skillmaster") && val.getSkillOption()==skill)
//				return true;
//		}
		return false;
	}

	//--------------------------------------------------------------------
	private void applyTalentModifications(TalentValue val) {
		logger.info("applyTalentModifications "+val);
		List<Modification> toApply = new ArrayList<>();

		for (Modification mod : val.getTalent().getModifications()) {
			if (mod instanceof AttributeModification) {
				AttributeModification origMod = (AttributeModification)mod;
				AttributeModification newMod  = new AttributeModification(
						origMod.getAttribute(),
						origMod.getValue()*val.getLevel()
						);
				newMod.setSource(val);
				newMod.setConditional(origMod.isConditional());
				toApply.add(newMod);
			} else if (mod instanceof SkillModification) {
				SkillModification origMod = (SkillModification)mod;
				SkillModification newMod  = new SkillModification(
						origMod.getSkill(),
						origMod.getValue()*val.getLevel()
						);
				newMod.setSource(val);
				toApply.add(newMod);
			} else {
				if (mod instanceof ValueModification)
					logger.error("Need to do special handling for "+mod.getClass());
				toApply.add(mod);
			}
		}

//		rules.getUtilities().applyToCharacter(model, toApply);
		for (Modification mod : toApply) {
			charGen.apply(mod);
			 val.addModification(mod);
		}
	}

	//--------------------------------------------------------------------
	private void undoTalentModifications(TalentValue val) {
		logger.debug("undoTalentModifications "+val);
		for (Modification mod : val.getModifications()) {
			charGen.undo(mod);
		}
//		rules.getUtilities().unapplyToCharacter(model, val.getModifications());
		val.clearModifications();
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.chargen.event.GenerationEventListener#handleGenerationEvent(org.prelle.ubiquity.chargen.event.GenerationEvent)
	 */
	@Override
	public void handleGenerationEvent(GenerationEvent event) {
		switch (event.getType()) {
		case EXPERIENCE_CHANGED:
			logger.debug("RCV "+event.getType());
			updateAvailable();
			break;
		default:
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getSelected()
	 */
	@Override
	public List<TalentValue> getSelected() {
		return model.getTalents();
	}

	@Override
	public double getSelectionCost(Talent data) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getDeselectionCost(TalentValue value) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ValueModification<Talent> createModification(TalentValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#needsOptionSelection(de.rpgframework.genericrpg.SelectableItem)
	 */
	@Override
	public boolean needsOptionSelection(Talent toSelect) {
		return toSelect.getSkillOptions()!=null && toSelect.getSkillOptions().size()>0;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#select(de.rpgframework.genericrpg.SelectableItem, java.lang.Object)
	 */
	@Override
	public TalentValue select(Talent data, Object option) {
		return select(data, (Skill)option);
	}

}
