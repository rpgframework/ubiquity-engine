/**
 * 
 */
package org.prelle.ubiquity.levelling;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Talent;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.AttributeController;
import org.prelle.ubiquity.charctrl.CharGenMode;
import org.prelle.ubiquity.charctrl.CharacterController;
import org.prelle.ubiquity.charctrl.FlawController;
import org.prelle.ubiquity.charctrl.ItemController;
import org.prelle.ubiquity.charctrl.ResourceController;
import org.prelle.ubiquity.charctrl.SkillController;
import org.prelle.ubiquity.charctrl.TalentController;
import org.prelle.ubiquity.chargen.ItemGenerator;
import org.prelle.ubiquity.modifications.AttributeModification;
import org.prelle.ubiquity.modifications.ModificationImpl;
import org.prelle.ubiquity.requirements.Requirement;

import de.rpgframework.genericrpg.modification.Modification;


/**
 * @author prelle
 *
 */
public class CharacterLeveller implements CharacterController {

	private static Logger logger = LogManager.getLogger("ubiquity.chargen");

	private UbiquityRuleset rules;
	private UbiquityCharacter model;
	
	private List<ModificationImpl> undoList;
	private AttributeLeveller attributes;
	private SkillLeveller skills;
	private TalentLeveller talents;
	private ResourceLeveller resources;
	private FlawLeveller flaws;
	private ItemController items;
	
	//-------------------------------------------------------------------
	public CharacterLeveller(UbiquityRuleset rules, UbiquityCharacter model) {
		this.rules = rules;
		this.model = model;
		
		undoList  = new ArrayList<ModificationImpl>();
		attributes = new AttributeLeveller(rules, model, undoList);
		skills    = new SkillLeveller(rules, model, undoList);
		talents   = new TalentLeveller(this, model, undoList);
		resources = new ResourceLeveller(rules, model, undoList);
		flaws     = new FlawLeveller(this, model, undoList);
		items     = new ItemGenerator(null, rules, model);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getMode()
	 */
	@Override
	public CharGenMode getMode() {
		return CharGenMode.LEVELING;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getModel()
	 */
	@Override
	public UbiquityCharacter getModel() {
		return model;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getRuleset()
	 */
	@Override
	public UbiquityRuleset getRuleset() {
		return rules;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getAttributeController()
	 */
	@Override
	public AttributeController getAttributeController() {
		return attributes;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getSkillController()
	 */
	@Override
	public SkillController getSkillController() {
		return skills;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getTalentController()
	 */
	@Override
	public TalentController getTalentController() {
		return talents;
	}

	@Override
	public ResourceController getResourceController() {
		return resources;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getFlawController()
	 */
	@Override
	public FlawController getFlawController() {
		return flaws;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#apply(de.rpgframework.genericrpg.modification.Modification)
	 */
	@Override
	public void apply(Modification mod) {
		logger.debug("apply "+mod);
		if (mod instanceof AttributeModification) {
			attributes.addModification((AttributeModification) mod);
//		} else if (mod instanceof SkillModification) {
//			skills.addModification((SkillModification) mod);
		} else {
			logger.warn("Don't know how to apply modification of type "+mod.getClass());
			System.exit(0);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#undo(de.rpgframework.genericrpg.modification.Modification)
	 */
	@Override
	public void undo(Modification mod) {
		logger.debug("undo "+mod);
		if (mod instanceof AttributeModification) {
			attributes.removeModification((AttributeModification) mod);
//		} else if (mod instanceof SkillModification) {
//			skills.addModification((SkillModification) mod);
		} else {
			logger.warn("Don't know how to apply modification of type "+mod.getClass());
			System.exit(0);
		}
	}

	//--------------------------------------------------------------------
	public void updateHistory() {
		logger.info("Update character history");
		Date now = new Date(System.currentTimeMillis());
		for (Modification changed : undoList) {
			changed.setDate(now);
			model.addToHistory(changed);
		}
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#isRequirementMet(org.prelle.ubiquity.Talent)
	 */
	@Override
	public boolean isRequirementMet(Talent item) {
		for (Requirement req : item.getRequirements()) {
			if (!model.meetsRequirement(req))
				return false;
		}
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.CharacterController#getItemController()
	 */
	@Override
	public ItemController getItemController() {
		return items;
	}

}
