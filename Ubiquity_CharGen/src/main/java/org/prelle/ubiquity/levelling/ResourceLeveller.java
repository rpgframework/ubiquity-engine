/**
 * 
 */
package org.prelle.ubiquity.levelling;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Stack;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Resource;
import org.prelle.ubiquity.Resource.Type;
import org.prelle.ubiquity.ResourceValue;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.ResourceController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.modifications.ModificationImpl;
import org.prelle.ubiquity.modifications.ResourceModification;

/**
 * @author Stefan
 *
 */
public class ResourceLeveller implements ResourceController {
	
	private static Logger logger = LogManager.getLogger("ubiquity.level.resource");

	private List<ModificationImpl> undoList;
	private Map<ResourceValue, Stack<ResourceModification>> resourceUndoStack;

	private UbiquityCharacter model;
	private UbiquityRuleset rules;

	//--------------------------------------------------------------------
	/**
	 */
	public ResourceLeveller(UbiquityRuleset rules, UbiquityCharacter model, List<ModificationImpl> undoList) {
		this.rules = rules;
		this.model = model;
		this.undoList = undoList;

		resourceUndoStack = new HashMap<ResourceValue, Stack<ResourceModification>>();
		for (ResourceValue key : model.getResources())
			resourceUndoStack.put(key, new Stack<ResourceModification>());
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#getPointsLeft()
	 */
	@Override
	public double getPointsLeft() {
		return 0;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#getAvailableResources()
	 */
	@Override
	public List<Resource> getAvailableResources() {
		List<Resource> ret = new ArrayList<Resource>();
		if (model.getExperienceFree()<7)
			return ret;
		
		for (Resource tmp : rules.getResources()) {
			if (tmp.getType()==Type.BASE) continue;
			ret.add(tmp);
		}
			
		return ret;
	}

	//--------------------------------------------------------------------
	private int getIncreaseCost(ResourceValue key) {
		switch (key.getPoints()) {
		case -1: return 7;
		case  0: return 8;
		default:
			return 15;
		}
	}

	//--------------------------------------------------------------------
	private ResourceModification findUndoModification(ResourceValue key) {
		ResourceModification mod = null;
		Stack<ResourceModification> stack = resourceUndoStack.get(key);
		if (stack!=null) {
			for (ResourceModification tmp : stack) {
				if (tmp.getResource()==key.getModifyable()) {
					mod = tmp;
				}
			}
		}

		return mod;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#canBeSelected(org.prelle.ubiquity.Resource)
	 */
	@Override
	public boolean canBeSelected(Resource talent) {
		if (talent.getType()==Type.BASE)
			return false;

		return model.getExperienceFree()>=7;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#canBeDeSelected(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public boolean canBeDeSelected(ResourceValue key) {
		ResourceModification mod = findUndoModification(key);
		if (mod==null)
			return false;
		
		if (key.getPoints()!=0)
			return false;
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#select(org.prelle.ubiquity.Resource)
	 */
	@Override
	public ResourceValue select(Resource resource) {
		logger.debug("select "+resource);
		if (!canBeSelected(resource))
			return null;
		
		/*
		 * Adding a new resource costs 15. We start resources on level 0 for 7 exp
		 * which can be raised to 1 for 8 exp
		 */
		int cost = 7;
		ResourceValue key = new ResourceValue(resource, 0);
		model.addResource(key);
		logger.info("Selected resource "+key.getModifyable()+" at "+key.getPoints());
		
		model.setExperienceFree(model.getExperienceFree() - cost);
		model.setExperienceInvested(model.getExperienceInvested() + cost);

		// Prepare undo
		ResourceModification mod = new ResourceModification(key.getModifyable(), 0);
		mod.setExpCost(cost);
		mod.setComment(key.getComment());
		undoList.add(mod);
		Stack<ResourceModification> stack = resourceUndoStack.get(key);
		if (stack==null) {
			stack = new Stack<>();
			resourceUndoStack.put(key, stack);
		}
		stack.push(mod);
		
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.RESOURCE_ADDED, 
				key));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.EXPERIENCE_CHANGED, 
				new int[]{model.getExperienceFree(), model.getExperienceInvested()}));
		
		
		return key;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#deselect(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public boolean deselect(ResourceValue key) {
		logger.debug("deselect "+key);
		if (!canBeDeSelected(key))
			return false;
		
		ResourceModification mod = findUndoModification(key);
		if (mod==null) {
			logger.error("Cannot find modification to undo");
			return false;
		}
		// Remove from history
		Stack<ResourceModification> stack = resourceUndoStack.get(key);
		undoList.remove(mod);
		stack.remove(mod);

		// Really deselect
		model.removeResource(key);
		logger.info("Deselect resource "+key.getModifyable());
		
		// Give exp back
		model.setExperienceFree(model.getExperienceFree()+ mod.getExpCost());
		model.setExperienceInvested(model.getExperienceInvested() -mod.getExpCost());
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.RESOURCE_REMOVED, 
				key));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.EXPERIENCE_CHANGED,
				new int[]{
						model.getExperienceFree(), 
						model.getExperienceInvested()
				}));
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#canBeDecreased(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public boolean canBeDecreased(ResourceValue key) {
		ResourceModification mod = findUndoModification(key);
		if (mod==null)
			return false;
		
		if (key.getModifyable().getType()==Type.BASE) {
			return key.getPoints()>-2;
		} else {
			if (key.getPoints()>0)
				return true;
			return canBeDeSelected(key);			
		}
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#canBeIncreased(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public boolean canBeIncreased(ResourceValue key) {
		if (key.getPoints()>=5)
			return false;

		// Raising from 0 to 1 costs 0.5, everything else 1
		return model.getExperienceFree() >= getIncreaseCost(key);
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#increase(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public boolean increase(ResourceValue key) {
		logger.debug("increase "+key);
		if (!canBeIncreased(key))
			return false;
		
		/*
		 * Decreasing a normal resource from 1 to 0
		 * frees half a point
		 */
		logger.debug("Increasing "+key+" costs "+getIncreaseCost(key));
		int cost = getIncreaseCost(key);		
		
		key.setPoints(key.getPoints()+1);
		logger.info("Increased resource "+key.getModifyable()+" to "+key.getPoints());
		
		model.setExperienceFree(model.getExperienceFree() - cost);
		model.setExperienceInvested(model.getExperienceInvested() + cost);

		// Prepare undo
		ResourceModification mod = new ResourceModification(key.getModifyable(), 1);
		mod.setExpCost(cost);
		mod.setComment(key.getComment());
		undoList.add(mod);
		Stack<ResourceModification> stack = resourceUndoStack.get(key);
		if (stack==null) {
			stack = new Stack<>();
			resourceUndoStack.put(key, stack);
		}
		stack.push(mod);
		
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.RESOURCE_CHANGED, 
				key));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.EXPERIENCE_CHANGED, 
				new int[]{model.getExperienceFree(), model.getExperienceInvested()}));
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#decrease(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public boolean decrease(ResourceValue key) {
		logger.debug("decrease "+key);
		if (!canBeDecreased(key))
			return false;


		// Search for a modification to remove
		ResourceModification mod = findUndoModification(key);
		if (mod==null) {
			logger.error("Cannot find modification to undo");
			return false;
		}

		// Decreasing a normal resource below 0 means deselecting
		if (key.getModifyable().getType()==Type.NORMAL && key.getPoints()==0)
			return deselect(key);

		// Remove from history
		Stack<ResourceModification> stack = resourceUndoStack.get(key);
		undoList.remove(mod);
		stack.remove(mod);
		
		// Really decrease
		key.setPoints(key.getPoints()-1);
		logger.info("Decreased resource "+key.getModifyable()+" to "+key.getPoints());
		
		// Give exp back
		model.setExperienceFree(model.getExperienceFree()+ mod.getExpCost());
		model.setExperienceInvested(model.getExperienceInvested() -mod.getExpCost());
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.RESOURCE_CHANGED, 
				key));
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.EXPERIENCE_CHANGED,
				new int[]{
						model.getExperienceFree(), 
						model.getExperienceInvested()
				}));
		return true;
	}

	//--------------------------------------------------------------------
	public boolean canBeTrashed(ResourceValue data) {
		return data.getModifyable().getType()==Type.NORMAL;
	}

	//-------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#findResourceReference(org.prelle.ubiquity.Resource, java.lang.String, java.lang.String)
	 */
	@Override
	public ResourceValue findResourceReference(Resource res, String descr,
			String idref) {
		for (ResourceValue tmp : model.getResources()) {
			if (res!=tmp.getModifyable()) 
				continue;
			if (tmp.getComment()!=null && descr!=null && tmp.getComment().equals(descr))
				return tmp;
			if (tmp.getComment()==null && descr==null)
				return tmp;
			if (tmp.getIdReference()!=null && idref!=null && tmp.getIdReference().equals(idref))
				return tmp;
			if (tmp.getIdReference()==null && idref==null)
				return tmp;
		}
		return null;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.ResourceController#trash(org.prelle.ubiquity.ResourceValue)
	 */
	@Override
	public void trash(ResourceValue key) {
		logger.debug("trash "+key);
		if (!canBeTrashed(key))
			return;
		
		ResourceModification mod = findUndoModification(key);
		if (mod!=null) {
			Stack<ResourceModification> stack = resourceUndoStack.get(key);
			undoList.remove(mod);
			stack.remove(mod);
		}
		
		model.removeResource(key);
		
		mod = new ResourceModification(key.getModifyable(), -key.getPoints());
		mod.setComment("Removed/Verworfen");
		undoList.add(mod);
		
		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.RESOURCE_REMOVED, 
				key));
		
	}

}
