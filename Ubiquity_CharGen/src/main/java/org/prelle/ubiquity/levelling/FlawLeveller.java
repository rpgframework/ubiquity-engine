/**
 * 
 */
package org.prelle.ubiquity.levelling;

import java.util.ArrayList;
import java.util.List;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.prelle.ubiquity.Flaw;
import org.prelle.ubiquity.FlawValue;
import org.prelle.ubiquity.UbiquityCharacter;
import org.prelle.ubiquity.UbiquityRuleset;
import org.prelle.ubiquity.charctrl.FlawController;
import org.prelle.ubiquity.chargen.event.GenerationEvent;
import org.prelle.ubiquity.chargen.event.GenerationEventDispatcher;
import org.prelle.ubiquity.chargen.event.GenerationEventType;
import org.prelle.ubiquity.modifications.ModificationImpl;

import de.rpgframework.genericrpg.modification.ValueModification;

/**
 * @author Stefan
 *
 */
public class FlawLeveller implements FlawController {
	
	private static Logger logger = LogManager.getLogger("ubiquity.level.flaws");

	private UbiquityCharacter model;
	private UbiquityRuleset rules;
	private CharacterLeveller charGen;
	
	private ArrayList<Flaw> availableTal;
	private boolean showUnavailableFlaws;

	//--------------------------------------------------------------------
	/**
	 */
	public FlawLeveller(CharacterLeveller ctrl, UbiquityCharacter model, List<ModificationImpl> undoList) {
		this.charGen = ctrl;
		this.rules = ctrl.getRuleset();
		this.model = model;
		
		availableTal = new ArrayList<>();

		updateAvailable();
	}

	//--------------------------------------------------------------------
	private void updateAvailable() {
		logger.debug("updateAvailable");
		availableTal.clear();
		availableTal.addAll(rules.getFlaws());
		
		// Remove those already selected
		for (FlawValue tmp : model.getFlaws()) {
			availableTal.remove(tmp.getFlaw());
		}
		
		// Remove those that cannot be selected
		if (!showUnavailableFlaws) {
			for (Flaw tmp : new ArrayList<Flaw>(availableTal)) {
			if (!canBeSelected(tmp))
				availableTal.remove(tmp);
			}
		}

		// Inform GUI
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.FLAW_AVAILABLE_CHANGED,
				availableTal));
		logger.debug("updateAvailable returns "+availableTal);
	}

	//--------------------------------------------------------------------
	@Override
	public List<Flaw> getAvailable() {
		return availableTal;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.FlawController#canBeSelected(org.prelle.ubiquity.Flaw)
	 */
	@Override
	public boolean canBeSelected(Flaw talent) {
		// Does the character have enough points?
		if (model.getExperienceFree()<15)
			return false;
		
		// Check if already selected
		for (FlawValue val : model.getFlaws()) {
			if (val.getFlaw()==talent) {
				return false;
			}
		}

		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.FlawController#canBeDeSelected(org.prelle.ubiquity.FlawValue)
	 */
	@Override
	public boolean canBeDeselected(FlawValue key) {
		if (!model.getFlaws().contains(key))
			return false;
		
		return true;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.FlawController#select(org.prelle.ubiquity.Flaw)
	 */
	@Override
	public FlawValue select(Flaw talent) {
		logger.info("Select "+talent);
		
		if (!canBeSelected(talent))
			return null;
		
		FlawValue val = new FlawValue(talent);
		model.addFlaw(val);
		
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.FLAW_ADDED, 
				val));
		
		updateAvailable();
		return val;
	}

	//--------------------------------------------------------------------
	/**
	 * @see org.prelle.ubiquity.charctrl.FlawController#deselect(org.prelle.ubiquity.FlawValue)
	 */
	@Override
	public boolean deselect(FlawValue val) {
		logger.info("Deselect "+val);
		
		if (!canBeDeselected(val)) {
			logger.debug("  trying to deselect a Flaw that cannot be deselected: "+val);
			return false;
		}

		// Remove from character
		model.removeFlaw(val);

		// Inform listener
		GenerationEventDispatcher.fireEvent(new GenerationEvent(
				GenerationEventType.FLAW_REMOVED, 
				val));

		updateAvailable();
		return true;
	}

	//-------------------------------------------------------------------
	/**
	 * @see de.rpgframework.genericrpg.SelectionController#getSelected()
	 */
	@Override
	public List<FlawValue> getSelected() {
		return model.getFlaws();
	}

	@Override
	public double getSelectionCost(Flaw data) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public double getDeselectionCost(FlawValue value) {
		// TODO Auto-generated method stub
		return 0;
	}

	@Override
	public ValueModification<Flaw> createModification(FlawValue value) {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public boolean needsOptionSelection(Flaw toSelect) {
		return false;
	}

	@Override
	public List<?> getOptions(Flaw toSelect) {
		return null;
	}

	@Override
	public FlawValue select(Flaw data, Object option) {
		return null;
	}

}
