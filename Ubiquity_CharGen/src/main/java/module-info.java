/**
 * @author Stefan Prelle
 *
 */
module ubiquity.chargen {
	exports org.prelle.ubiquity.chargen.event;
	exports org.prelle.ubiquity.levelling;
	exports org.prelle.ubiquity.charctrl;
	exports org.prelle.ubiquity.chargen;

	requires org.apache.logging.log4j;
	requires rpgframework.api;
	requires transitive ubiquity.core;
}